<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="shortcut icon" href="img/ecom.png" />  
    <title>Index</title>
      
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
    <!-- CSS personalizado --> 
    <link rel="stylesheet" href="main.css">  
    
    <!-- Los iconos tipo Solid de Fontawesome-->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.8/css/solid.css">
    <script src="https://use.fontawesome.com/releases/v5.0.7/js/all.js"></script>
      
    <!--datables CSS básico-->
    <link rel="stylesheet" type="text/css" href="assets/datatables/datatables.min.css"/>
    <!--datables estilo bootstrap 4 CSS-->  
    <link rel="stylesheet"  type="text/css" href="assets/datatables/DataTables-1.10.18/css/dataTables.bootstrap4.min.css">    
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet">  
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    
    <!-- Nuestro css-->
    <link rel="stylesheet" type="text/css" href="index.css" th:href="@{index.css}">

    <!-- scrip recapchat -->

    <script src="https://www.google.com/recaptcha/api.js?hl=en" async defer></script>
    
  </head>


  <script>

function validar(){
  document.getElementById("formuser").addEventListener("submit",function(evt)
  {
  
  var response = grecaptcha.getResponse();
  if(response.length == 0) 
  { 
    //reCaptcha not verified
    //alert("please verify you are humann!"); 
    document.getElementById('valcap').style.display = 'block';
    evt.preventDefault();
    return false;
  }
  //captcha verified
  //do the rest of your validations here
  
});
}

</script>
    
  <body>
  <div class="bg-image">
  <div class="modal-dialog text-center">
        <div class="col-sm-8 main-section">
            <div class="modal-content">
                <div class="col-12 user-img">
                    <img src="img/logo1.png" th:src="@{/img/logo1.png}"/>
                </div>
                <form action="ValidarUsuario.php" method="POST" id="formuser">
                    <h6><b><p style="color:#FFFFFF">Inquiries Portal</p></b></h6>
                    <div class="form-group" id="user-group" style="padding-bottom: .7rem;">
                        <input type="text" class="form-control" placeholder="Username" name="username" required />
                    </div>
                    <div class="form-group" id="contrasena-group">
                        <input type="password" class="form-control" placeholder="Password" name="password" required />
                    </div>
                    <br>
                    <!-- CLAVE CAPCHAT PROD
                    <div class="g-recaptcha" data-sitekey="6Lelv90iAAAAAFsB0-6uv66RysH-39Vtf6kTNdms">  </div>-->
                      <!-- CLAVE CAPCHAT PRUEBAS-->
                      <div class="g-recaptcha" data-sitekey="6Ld5tsoiAAAAAHfqAElQ7gcc5xb-svrni_Q8tKao">  </div>
                    <div class="alert alert-warning" role="alert" id="valcap" style="display:none" >
                    Check the I am not a robot box
                    </div>
                    <?php 
                    if(isset($errorLogin)){
                        ?><p style="color:white"><?php echo $errorLogin; ?></p>
                    <?php } ?>
                    <br>
                    <button type="submit" class="btn btn-dark" onclick="validar()"><i class="fas fa-sign-in-alt"></i>  Log in </button>
                </form>
            </div>
        </div>
    </div>
    </div>
  </body>
</html>
