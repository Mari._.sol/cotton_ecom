<?php

if (!isset($_SESSION))
session_start();
if (isset($_SESSION['proveed'])) :
   echo($_SESSION['proveed']);
?>

    <!doctype html>
    <html lang="es en">
         
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv='cache-control' content='no-cache'>
        <meta http-equiv='expires' content='0'>
        <meta http-equiv='pragma' content='no-cache'>
        <link rel="shortcut icon" href="img/ecom.png" />

        <title>Home</title>
        
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script>
       
        
        <script defer type="text/javascript" src="passwordupdate.js"> </script>
        <script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
        <!-- jQuery, Popper.js, Bootstrap JS -->
        <script src="assets/jquery/jquery-3.3.1.min.js"></script>
        <script src="assets/popper/popper.min.js"></script>
        <script src="assets/bootstrap/js/bootstrap.min.js"></script>
    <!-- scrip y librerias necesarias para finalizar sesion por inactividad -->

        <script type="text/javascript" src="timer.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script>


        <!-- CSS bootstrap -->
        <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">

        <!-- CSS personalizado -->
        <link rel="stylesheet" href="main.css">
        <!--Google fonts -->
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Inter:wght@300&family=Padauk&display=swap" rel="stylesheet">
       
       
        <script defer type="text/javascript" src="assets/bootstrap/js/bootstrap.min.js"> </script>
    </head>

    <body>
     

        <!-- Esto es el Nav bar, todo contenido en un container-fluid -->
        <nav class="navbar navbar-expand-lg bg-transparent navbar-custom">
            <div class="container-fluid" style="padding-right: 1.5rem; padding-left: 1.5rem;">
                <div class="dropdown  nav-item col-xl-1 col-lg-1 col-md-2 col-sm-2 col-xs-1">
                    <a class="btn dropdown-toggle " href="#" role="button" id="dropdownMenuLink" data-bs-toggle="dropdown" aria-expanded="false">
                        <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" fill="white" class="bi bi-list" viewBox="0 0 16 16">
                            <path fill-rule="evenodd" d="M2.5 12a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5z" />
                        </svg>
                    </a>

                    <ul class="dropdown-menu " aria-labelledby="dropdownMenuLink">
                        <li><a class="dropdown-item" href="trucks.php">Trucks</a></li>
                        <li><a class="dropdown-item" href="lots.php">Lots</a></li>
                        <li><a class="dropdown-item" href="do.php">Delivery Order</a></li>
                        <li><a class="dropdown-item" href="adjustments.php">Adjustments</a></li>
                        <li><a class="dropdown-item" href="inv.php">Invoice</a></li>
                        <li><a class="dropdown-item" href="export.php">Exports</a></li>
                        <li><a class="dropdown-item" href="clients.php">Clients</a></li>
                        <li><a class="dropdown-item" href="routes.php">Routes</a></li>
                        <li><a class="dropdown-item" href="requisition.php">Requisition</a></li>
                        <li><a class="dropdown-item" href="powerbi.php">Dashboards Logistica</a></li>
                    </ul>
                </div>

              
                <div id="pas" style="display:none;">
                     <input id="user" value="<?php echo $_SESSION['proveed']; ?>"/>
               </div> 
             
               <div id="pas" style="display:none;">
                     <input id="password" value="<?php echo $_SESSION['pass']; ?>"/>
               </div> 
                <a class="navbar-brand nav-item col-xl-1 col-lg-1 col-md-2 col-sm-2 col-xs-2" href="#">
                    <div class="logos"><img class="log" src="img/logo1.png"> AMSA</div>
                </a>
                <div class="col-xl-1 col-lg-1 col-md-1 col-sm-1 col-xs-1 tex">
                    <p>/ Home</p>
                </div>
                <div class="container-fluid  col-xl-7 col-lg-6 col-md-3 col-sm-2 col-xs-1">
                </div>
                <div class="dropdown nav-item col-xl-2 col-lg-3 col-md-3 col-sm-3 col-xs-1 d-flex justify-content-end" style="padding-right: 1.5rem;">
                    <button style="color:white;" class="btn dropdown-toggle btn-outline-success" data-bs-toggle="dropdown" aria-expanded="false" id="dropdownMenuLink2"><?php echo $_SESSION['proveed']; ?></button>
                    <ul class="dropdown-menu" aria-labelledby="dropdownMenuLink2">
                        <li><a class="dropdown-item" href="logout.php">Cerrar sesión</a></li>
                    </ul>
                </div>
            </div>
        </nav>
        <!-- Esto es el Nav bar, todo contenido en un container-fluid -->

        <!-- Aquí inicia todo código de tablas etc -->

        <!-- Formulario para actualizar contraseña -->
    <div class="modal fade bd-example-modal-lg" id="updatepass" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
            <div class="modal-dialog" id="waitDialog">
            <div class="modal-content">
                <div class="modal-header">
                <h3> <div class="p-3 mb-2 bg-gradient-primary "> Update Password</div></h3>
               
                </div>
                <div class="modal-body">
                    
                            
                    <form  id="passup">
                        <!--

                        <div class="form-group">
                        <div class="p-3 mb-2 bg-gradient-primary ">Enter Current Password</div>
                        <input type="password" class="form-control" id="pass" required placeholder="">
                        
                        
                        </div>
-->                     
                            <small class="p-3 mb-2 bg-gradient-primary " id="" class="form-text text-muted">Your new password must contain: </small>
                            <br>
                            <small class="p-3 mb-2 bg-gradient-primary " id="" class="form-text text-muted">-8 to 12 characters</small>
                            <br>
                            <small class="p-3 mb-2 bg-gradient-primary " id="" class="form-text text-muted">-At least one capital letter</small>
                            <br>
                            <small class="p-3 mb-2 bg-gradient-primary " id="" class="form-text text-muted">-At least one number</small>
                        
                        
                        <div class="form-group">
                            <div class="p-3 mb-2 bg-gradient-primary "> New Password  </div>
                            
                            <input type="password" class="form-control" id="newpass" required onkeyup="passwordCheck2()" maxlength="12"  pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,12}">
                            <div class="form-check">
                            <input class="form-check-input" type="checkbox"  onclick="Toggle()" value="" id="muestrapass">
                            <label class="form-check-label" for="flexCheckDefault">
                            Show Password
                            </label>
                            
                            </div>
                            <span class="badge rounded-pill bg-danger" id ="estatus1"></span>
                    
                        </div>
     
                     

                        <div class="form-group">
                        <div class="p-3 mb-2 bg-gradient-primary ">Repeat New Password </div>
                        <input type="password" class="form-control" id="newpass2" required onkeyup="passwordCheck()" placeholder=""   maxlength="12" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,12}">
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" onclick="Toggle2()" value="" id="muestrapass2">
                            <label class="form-check-label" for="flexCheckDefault">
                            Show Password
                            </label>
                            
                        
                        </div>
                        </div>
                        <span class="badge rounded-pill bg-danger" id ="estatus"></span>
                        <br>
                        <br>     
                        <button type="submit" disabled  id="update" class="btn btn-primary" >Update</button>
                          
                    </form>
                </div>
                <div class="modal-footer">
                
              
                </div>
            </div>
            </div>


        <div class="container-fluid">
       
            <div class="col-12">
                <p><br><br><br><br><br><br><br><br><br><br><br> </p>
            </div>
            <div class="emblema col-11">
                <h1>
                    "Portal de Logística - AMSA
                </h1>
                <br>
                <h1>
                    Algodón México"
                </h1>
            </div>

        </div>

        <!-- Aquí termina-->


    </body>

    </html>
<?php
else :
    include_once('index.php');
endif;
?>
