<?php

include_once ('conexion.php');
$objeto = new Conexion();
$conexion = $objeto->Conectar();

$fecha=date('Y-m-d');
// Para usar la phpSpreadsheet llamamos a autoload
require './vendor/autoload.php';
use PhpOffice\PhpSpreadsheet\{Spreadsheet, IOFactory};
use PhpOffice\PhpSpreadsheet\Style\{Border, Color, Fill, Alignment};
require './vendor/phpoffice/phpspreadsheet/src/PhpSpreadsheet/Spreadsheet.php';
require './vendor/phpoffice/phpspreadsheet/src/PhpSpreadsheet/IOFactory.php';
require './vendor/phpoffice/phpspreadsheet/src/PhpSpreadsheet/Style/Color.php';
require './vendor/phpoffice/phpspreadsheet/src/PhpSpreadsheet/Style/Borders.php';
require './vendor/phpoffice/phpspreadsheet/src/PhpSpreadsheet/Style/Fill.php';
//use PhpOffice\PhpSpreadsheet\Style\Alignment as alignment;
//Para scribir xlsx
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
$heading = false;

//array de origenes 
$origenes=['ARG','AUS','BRA','GRC','IND','MEX','SPA','TUR','USA-MS','USA-PM','USA-SE','USA-STX','USA-WTX','WAF'];

//NOMBRE DEL ARCHIVO
$fileName = "Basis-".date('Y-m-d').".xlsx";
//CONSULTAR MESES DE COVERTURA
$consulta="Select CovMon FROM amsadb1.Inquiries_Covmon WHERE Inquiries_Covmon.Basis !=0 ORDER BY Basis ASC";
$resultado = $conexion->prepare($consulta);
$resultado->execute();    
$meses=$resultado->fetchAll(PDO::FETCH_ASSOC);

$mes1=$meses[0]['CovMon'];
$mes2=$meses[1]['CovMon'];
$mes3=$meses[2]['CovMon'];
$mes4=$meses[3]['CovMon'];





//SE CREA Y SE EMPIEZA A LLENAR LA HOJA
$excel = new Spreadsheet();
$hojaActiva = $excel->getActiveSheet();
$hojaActiva->setTitle("Basis");

//negritas en encabezado

$hojaActiva->getStyle('A1:V1')->getFont()->setBold( true );

//relleno de celda encabezado
$hojaActiva->getStyle('A1:Q1')->getFill()
    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
    ->getStartColor()->setARGB('FFE1E1E1');



//$hojaActiva->getColumnDimension('A')->setWidth(12);
$hojaActiva->setCellValue('A1','Origin');
$hojaActiva->mergeCells('A1:A2');

//centrar dos celdas unidas verticalmente 
$hojaActiva->getStyle('A1:A2')->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
//Marcar los bordes 
$hojaActiva->getStyle('A1:A2')->getBorders()->getAllBorders()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK);;


$hojaActiva->setCellValue('B1',$mes1);
$hojaActiva->mergeCells('B1:E1');

//centrar celdas unidas horizontalmente 
$hojaActiva->getStyle('B1:E1')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

$hojaActiva->setCellValue('F1',$mes2);
$hojaActiva->mergeCells('F1:I1');
//centrar celdas unidas horizontalmente 
$hojaActiva->getStyle('F1:I1')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

$hojaActiva->setCellValue('J1',$mes3);
$hojaActiva->mergeCells('J1:M1');
//centrar celdas unidas horizontalmente 
$hojaActiva->getStyle('J1:M1')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

$hojaActiva->setCellValue('N1',$mes4);
$hojaActiva->mergeCells('N1:Q1');
//centrar celdas unidas horizontalmente 
$hojaActiva->getStyle('N1:Q1')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

//Marcar los bordes 
$hojaActiva->getStyle('B1:Q1')->getBorders()->getAllBorders()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK);;


//SUBTITULOS RS-SM
$hojaActiva->setCellValue('B2','RS-SM');
$hojaActiva->setCellValue('F2','RS-SM');
$hojaActiva->setCellValue('J2','RS-SM');
$hojaActiva->setCellValue('N2','RS-SM');

//SUBTITULOS RS-M
$hojaActiva->setCellValue('C2','RS-M');
$hojaActiva->setCellValue('G2','RS-M');
$hojaActiva->setCellValue('K2','RS-M');
$hojaActiva->setCellValue('O2','RS-M');

//SUBTITULOS RS-M
$hojaActiva->setCellValue('D2','RS-SLM');
$hojaActiva->setCellValue('H2','RS-SLM');
$hojaActiva->setCellValue('L2','RS-SLM');
$hojaActiva->setCellValue('P2','RS-SLM');

//SUBTITULOS RS-M
$hojaActiva->setCellValue('E2','OE');
$hojaActiva->setCellValue('I2','OE');
$hojaActiva->setCellValue('M2','OE');
$hojaActiva->setCellValue('Q2','OE');


//Marcar los bordes 
$hojaActiva->getStyle('B2:Q2')->getBorders()->getAllBorders()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUM);;
//centrar celdas unidas horizontalmente 
$hojaActiva->getStyle('B2:Q2')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);


$tam=count($origenes);
//CONSULTAR VALORES
$fila=3;
for($i=0; $i<$tam; $i++){
    $origen = $origenes[$i];
    $consulta="SELECT   IF((SELECT DISTINCT Org  FROM OrgBasis WHERE Org ='$origen')IS NOT NULL ,(SELECT DISTINCT Org  FROM OrgBasis WHERE Org ='$origen'),'$origen') as Origen,
    (SELECT IFNULL((SELECT Bas FROM OrgBasis WHERE Org ='$origen' AND CovMon='$mes1' AND Dat BETWEEN date_sub('$fecha',interval 28 day ) AND '$fecha' AND Grd='RS-SM' ORDER BY Dat DESC,IdBasis DESC  LIMIT 1), '')) as mes1_RS_SM,
    (SELECT IFNULL((SELECT Bas FROM OrgBasis WHERE Org ='$origen' AND CovMon='$mes1' AND Dat BETWEEN date_sub('$fecha',interval 28 day ) AND '$fecha' AND Grd='RS-M' ORDER BY Dat DESC,IdBasis DESC  LIMIT 1), '')) as mes1_RS_M,
    (SELECT IFNULL((SELECT Bas FROM OrgBasis WHERE Org ='$origen' AND CovMon='$mes1' AND Dat BETWEEN date_sub('$fecha',interval 28 day ) AND '$fecha' AND Grd='RS-SLM' ORDER BY Dat DESC,IdBasis DESC  LIMIT 1), '')) as mes1_RS_SLM,
    (SELECT IFNULL((SELECT Bas FROM OrgBasis WHERE Org ='$origen' AND CovMon='$mes1' AND Dat BETWEEN date_sub('$fecha',interval 28 day ) AND '$fecha' AND Grd='OE' ORDER BY Dat DESC,IdBasis DESC  LIMIT 1), '')) as mes1_OS,
    (SELECT IFNULL((SELECT Bas FROM OrgBasis WHERE Org ='$origen' AND CovMon='$mes2' AND Dat BETWEEN date_sub('$fecha',interval 28 day ) AND '$fecha' AND Grd='RS-SM' ORDER BY Dat DESC,IdBasis DESC  LIMIT 1), '')) as mes2_RS_SM,
    (SELECT IFNULL((SELECT Bas FROM OrgBasis WHERE Org ='$origen' AND CovMon='$mes2' AND Dat BETWEEN date_sub('$fecha',interval 28 day ) AND '$fecha' AND Grd='RS-M' ORDER BY Dat DESC,IdBasis DESC  LIMIT 1), '')) as mes2_RS_M,
    (SELECT IFNULL((SELECT Bas FROM OrgBasis WHERE Org ='$origen' AND CovMon='$mes2' AND Dat BETWEEN date_sub('$fecha',interval 28 day ) AND '$fecha' AND Grd='RS-SLM' ORDER BY Dat DESC,IdBasis DESC  LIMIT 1), '')) as mes2_RS_SLM,
    (SELECT IFNULL((SELECT Bas FROM OrgBasis WHERE Org ='$origen' AND CovMon='$mes2' AND Dat BETWEEN date_sub('$fecha',interval 28 day ) AND '$fecha' AND Grd='OE' ORDER BY Dat DESC,IdBasis DESC  LIMIT 1), '')) as mes2_OS,
    (SELECT IFNULL((SELECT Bas FROM OrgBasis WHERE Org ='$origen' AND CovMon='$mes3' AND Dat BETWEEN date_sub('$fecha',interval 28 day ) AND '$fecha' AND Grd='RS-SM' ORDER BY Dat DESC,IdBasis DESC  LIMIT 1), '')) as mes3_RS_SM,
    (SELECT IFNULL((SELECT Bas FROM OrgBasis WHERE Org ='$origen' AND CovMon='$mes3' AND Dat BETWEEN date_sub('$fecha',interval 28 day ) AND '$fecha' AND Grd='RS-M' ORDER BY Dat DESC,IdBasis DESC  LIMIT 1), '')) as mes3_RS_M,
    (SELECT IFNULL((SELECT Bas FROM OrgBasis WHERE Org ='$origen' AND CovMon='$mes3' AND Dat BETWEEN date_sub('$fecha',interval 28 day ) AND '$fecha' AND Grd='RS-SLM' ORDER BY Dat DESC,IdBasis DESC  LIMIT 1), '')) as mes3_RS_SLM,
    (SELECT IFNULL((SELECT Bas FROM OrgBasis WHERE Org ='$origen' AND CovMon='$mes3' AND Dat BETWEEN date_sub('$fecha',interval 28 day ) AND '$fecha' AND Grd='OE' ORDER BY Dat DESC,IdBasis DESC  LIMIT 1), '')) as mes3_OS,
    (SELECT IFNULL((SELECT Bas FROM OrgBasis WHERE Org ='$origen' AND CovMon='$mes4' AND Dat BETWEEN date_sub('$fecha',interval 28 day ) AND '$fecha' AND Grd='RS-SM' ORDER BY Dat DESC,IdBasis DESC  LIMIT 1), '')) as mes4_RS_SM,
    (SELECT IFNULL((SELECT Bas FROM OrgBasis WHERE Org ='$origen' AND CovMon='$mes4' AND Dat BETWEEN date_sub('$fecha',interval 28 day ) AND '$fecha' AND Grd='RS-M' ORDER BY Dat DESC,IdBasis DESC  LIMIT 1), '')) as mes4_RS_M,
    (SELECT IFNULL((SELECT Bas FROM OrgBasis WHERE Org ='$origen' AND CovMon='$mes4' AND Dat BETWEEN date_sub('$fecha',interval 28 day ) AND '$fecha' AND Grd='RS-SLM' ORDER BY Dat DESC,IdBasis DESC  LIMIT 1), '')) as mes4_RS_SLM,
    (SELECT IFNULL((SELECT Bas FROM OrgBasis WHERE Org ='$origen' AND CovMon='$mes4' AND Dat BETWEEN date_sub('$fecha',interval 28 day ) AND '$fecha' AND Grd='OE' ORDER BY Dat DESC,IdBasis DESC  LIMIT 1), '')) as mes4_OS";
    $resultado = $conexion->prepare($consulta);
    $resultado->execute();    
    $renglon=$resultado->fetchAll(PDO::FETCH_ASSOC);
    
    $hojaActiva->setCellValue('A' . $fila,$renglon[0]['Origen']);
    $hojaActiva->setCellValue('B' . $fila,$renglon[0]['mes1_RS_SM']);
    $hojaActiva->setCellValue('C' . $fila,$renglon[0]['mes1_RS_M']);
    $hojaActiva->setCellValue('D' . $fila,$renglon[0]['mes1_RS_SLM']);
    $hojaActiva->setCellValue('E' . $fila,$renglon[0]['mes1_OS']);

    $hojaActiva->setCellValue('F' . $fila,$renglon[0]['mes2_RS_SM']);
    $hojaActiva->setCellValue('G' . $fila,$renglon[0]['mes2_RS_M']);
    $hojaActiva->setCellValue('H' . $fila,$renglon[0]['mes2_RS_SLM']);
    $hojaActiva->setCellValue('I' . $fila,$renglon[0]['mes2_OS']);

    $hojaActiva->setCellValue('J' . $fila,$renglon[0]['mes3_RS_SM']);
    $hojaActiva->setCellValue('K' . $fila,$renglon[0]['mes3_RS_M']);
    $hojaActiva->setCellValue('L' . $fila,$renglon[0]['mes3_RS_SLM']);
    $hojaActiva->setCellValue('M' . $fila,$renglon[0]['mes3_OS']);


    $hojaActiva->setCellValue('N' . $fila,$renglon[0]['mes4_RS_SM']);
    $hojaActiva->setCellValue('O' . $fila,$renglon[0]['mes4_RS_M']);
    $hojaActiva->setCellValue('P' . $fila,$renglon[0]['mes4_RS_SLM']);
    $hojaActiva->setCellValue('Q' . $fila,$renglon[0]['mes4_OS']);


    //Marcar los bordes 
    $hojaActiva->getStyle('A'.$fila.':Q'.$fila)->getBorders()->getAllBorders()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_HAIR);;
    //centrar celdas unidas horizontalmente 
    $hojaActiva->getStyle('A'.$fila.':Q'.$fila)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);




    $fila++;



}






header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="'.$fileName.'"');
header('Cache-Control: max-age=0');
ob_end_clean();
$writer = IOFactory::createWriter($excel, 'Xlsx');
$writer->save('php://output');
exit;


?>