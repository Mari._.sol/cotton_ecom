<?php
 session_start();
 header("Cache-Control: no-cache, must-revalidate"); //para borrar cache
if (!isset($_SESSION['user_inquiries'])) :
    include_once('index.php');
else :
    include_once('bd/permisos.php');
   
    $region = $_SESSION['Region'];
    $usuario=$_SESSION['user_inquiries'];
    $priv = $_SESSION['Priv'];
    $vistaComp=$_SESSION['ViewCompetition'];
    $vistaInq= $_SESSION['ViewInquirie'];
    $vistabuyer= $_SESSION['ViewBuyer'];
    $CostToLand = $_SESSION['CostToLand'];
    $basis = $_SESSION['basis'];
    $MailBasis =$_SESSION['MailBasis']; 
    $permisos_basis= getEditBasis($usuario);
    $editbasis=$permisos_basis[0]['EditBasis'];

    if ($basis == 0){
        header("Location:index.php"); 
    }
    
    
    //Se inicializa la variable de sesion para filtros
    $_SESSION['Filters'] = array(
        "FromDate" => "",
        "ToDate" => "",
        "OriginReg" => "",
        "OriginCount" => "",
        "UserComp" => ""                             
    );
?>
<!doctype html>
<html lang="en-US">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="shortcut icon" href="img/ecom.png" />
        <title>Origin Basis - JM Cotton</title>

        <!-- CSS bootstrap -->
        <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
        <!--datables CSS básico-->
        <link rel="stylesheet" type="text/css" href="assets/datatables/datatables.min.css" />
        <!--datables estilo bootstrap 4 CSS-->
        <link rel="stylesheet" type="text/css" href="assets/datatables/DataTables-1.10.18/css/dataTables.bootstrap4.min.css">

        <!-- CSS personalizado -->
        <link rel="stylesheet" href="main.css">
        
        <!--Google fonts -->
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Inter:wght@300&family=Padauk&display=swap" rel="stylesheet">

        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

        <!--font awesome con CDN  -->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.1/font/bootstrap-icons.css">
    </head>
    <body>
        <!-- Scripts -->
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script>
   
        <!-- jQuery, Popper.js, Bootstrap JS -->
        <script src="assets/jquery/jquery-3.3.1.min.js"></script>
        <script src="assets/popper/popper.min.js"></script>
        <script src="assets/bootstrap/js/bootstrap.min.js"></script>

        <!-- librerias necesarias para finalizar sesion por inactividad -->
        <script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script>

        <!-- Scrip para finalizar sesion por inactividad -->
        <script type="text/javascript" src="timer.js"></script>

        <!-- datatables JS -->
        <script type="text/javascript" src="assets/datatables/datatables.min.js"></script>

        <!-- para usar botones en datatables JS -->
        <script src="datatables/Buttons-1.5.6/js/dataTables.buttons.min.js"></script>
        <script src="datatables/JSZip-2.5.0/jszip.min.js"></script>
        <script src="datatables/pdfmake-0.1.36/pdfmake.min.js"></script>
        <script src="datatables/pdfmake-0.1.36/vfs_fonts.js"></script>
        <script src="datatables/Buttons-1.5.6/js/buttons.html5.min.js"></script>
        
        <!-- Ficed columns -->
        <script src="https://cdn.datatables.net/fixedcolumns/4.0.0/js/dataTables.fixedColumns.min.js"></script>        
        <script type="text/javascript" src="BasisJM.js"></script>

        <!-- Terminan Scripts -->
            
        <!-- Esto es el Nav bar, todo contenido en un container-fluid -->
        <nav class="navbar navbar-expand-lg bg-transparent navbar-custom">
            <div class="container-fluid" style="padding-right: 1.5rem; padding-left: 1.5rem;">
                <div class="dropdown  nav-item col-xl-1 col-lg-1 col-md-2 col-sm-2 col-xs-1">
                    <a class="btn dropdown-toggle " href="#" role="button" id="dropdownMenuLink" data-bs-toggle="dropdown" aria-expanded="false">
                        <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" fill="white" class="bi bi-list" viewBox="0 0 16 16">
                            <path fill-rule="evenodd" d="M2.5 12a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5z" />
                        </svg>
                    </a>
                    
                    <ul class="dropdown-menu " aria-labelledby="dropdownMenuLink">
                        <?php if ($vistaInq == 1){?>      
                            <li><a class="dropdown-item" href="inquirie.php">Inquiries</a></li>
                        <?php } ?>  
                        <?php if ($vistaComp == 1){?> 
                            <li><a class="dropdown-item" href="competition.php">Competition Offers</a></li>   
                        <?php } ?>                           
                        <?php if ($vistabuyer == 1){?>           
                            <li><a class="dropdown-item" href="buyers.php">Buyers</a></li>    
                        <?php } ?>
                        <?php if ($CostToLand == 1){?>                            
                            <li><a class="dropdown-item" href="CostToLand.php">Cost to Land</a></li> 
                        <?php } ?>
                        <?php if ($basis  == 1){?>                            
                            <li><a class="dropdown-item" href="Basis.php">Origin Basis</a></li> 
                        <?php } ?> 
                        <li><a class="dropdown-item" style="background-color: #5a926d;" href="BasisJM.php">Origin Basis - JM Cotton</a></li>
                    </ul>
                   
                <div id="gin" style="display:none;">
                    <input id="priv" value="<?php echo $priv; ?>"/>
                    <input id="usuario" value="<?php echo $usuario; ?>"/>
                    <input id="editar" value="<?php echo $editbasis; ?>"/>
                    <input id="region" value="<?php echo $region; ?>"/>
                    <input id="nombre" value="<?php echo $Nombre; ?>"/>
                </div> 

                </div>
                <a class="navbar-brand nav-item col-xl-2 col-lg-2 col-md-2 col-sm-3 col-xs-3" href="main.php">
                    <div class="logos"><img class="log" src="img/logo1.png"> ECOM/Origin Basis - JM Cotton</div>
                </a>
                <div class="col-xl-1 col-lg-1 col-md-1 col-sm-1 col-xs-1 tex">
                </div>
                <div class="container-fluid  col-xl-7 col-lg-6 col-md-3 col-sm-2 col-xs-1">
                    <!-- Botones -->
                    <?php if ($editbasis == 1){?>
                    <button id="btnNuevo" type="button" class="btn btn-light" data-toggle="modal tooltip" data-placement="bottom" title="New"><i class="bi bi-plus-square"></i></button>
                    <?php } ?> 
                </div>
                <div class="dropdown nav-item col-xl-1 col-lg-2 col-md-2 col-sm-2 col-xs-1 d-flex justify-content-end" style="padding-right: 1.5rem;">
                    <button style="color:white;" class="btn dropdown-toggle btn-outline-success" data-bs-toggle="dropdown" aria-expanded="false" id="dropdownMenuLink2"  value="<?php echo $_SESSION['user_inquiries']; ?>"><?php echo $_SESSION['user_inquiries']; ?></button>
                    <ul class="dropdown-menu" aria-labelledby="dropdownMenuLink2">
                        <li><a class="dropdown-item" href="logout.php">Close sesion</a></li>
                    </ul>
                </div>
            </div>
        </nav>
        <div class="card card-body " style="opacity:100%;" >   
            <div class="table-responsive" style="opacity:100%;">            
                <table id="table_competition"  class="table bg-white table-striped row-border order-column table-hover " style="opacity:100%;">                     
                    <thead style="background-color: #65ac7c;" style="opacity:100%;">                   
                        <tr>        
                            <th >ID</th>
                            <th >Date</th>       
                            <th >Origin Region </th>                         
                            <th >Country Origin</th>
                            <th >SubOrigin</th>
                            <th >Quantity (tons)</th>
                            <th >Crop (from)</th>
                            <th >Crop (to)</th>
                            <th >Comments</th>
                            <th >Col Max</th>  
                            <th >Len Min</th>
                            <th >Mic Min</th>
                            <th >Mic Max</th>
                            <th >Str Min</th>
                            <th >Quality comment</th>
                            <th >Basis(Pts.)</th>                                
                            <th >Cover Month</th>
                            <th >User</th>
                            <th class="no-exportar"></th>                              
                        </tr>
                    </thead>                    
                      
                    <tbody> 
                    </tbody>

                    <tfoot>
                        <tr>        
                            <th >ID</th>
                            <th >Date</th>       
                            <th >Origin Region </th>                         
                            <th >Country Origin</th>
                            <th >SubOrigin</th>
                            <th >Quantity (tons)</th>
                            <th >Crop (from)</th>
                            <th >Crop (to)</th>
                            <th >Comments</th>
                            <th >Col Max</th>  
                            <th >Len Min</th>
                            <th >Mic Min</th>
                            <th >Mic Max</th>
                            <th >Str Min</th>
                            <th >Quality comment</th>
                            <th >Basis(Pts.)</th>                                
                            <th >Cover Month</th>
                            <th >User</th>
                            <th class="no-exportar"></th>                              
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>

        <!--Modal-->
         <div class="modal hide fade in" data-bs-backdrop="static"  data-bs-keyboard="false" class="modal" id="modalCompetition" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-xl">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title"></h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                        </button>
                    </div>
                    <form id="form_competition">
                        <div class="modal-body" id="InData">
                            <div class="row">
                                <div class="col-lg-3">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Date<font size=2>*</font></label>
                                        <input type="date" class="form-control form-control-sm" id="Date" onkeydown="return false" lang="en" required>
                                    </div>
                                </div>

                                <div class="col-lg-3">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Origin Region<font size=2>*</font></label >
                                        <select class="form-control form-control-sm" id="zone2" required>
                                        </select>

                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Origin Country</label >
                                        <select class="form-control form-control-sm" id="Origin" >
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">SubOrigin</label>
                                        <select class="form-control form-control-sm" id="Origin2" disabled>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                            <div class="col-lg-2">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Quantity (tons)</label>
                                        <input type="text" class="form-control form-control-sm" id="Quantity" >                                        
                                    </div>
                                </div>
                                <div class="col-lg-2">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Crop (from)<font size=2>*</font></label>
                                        <select class="form-control form-control-sm" id="Crop_from" required>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-2">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Crop (to)<font size=2>*</font></label>
                                        <select class="form-control form-control-sm"  id="Crop_to" disabled required>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Comments</label>
                                        <input type="text" class="form-control form-control-sm"  id="Comments">
                                    </div>
                                </div>

                                <div id="alerta_cantidad" style="display: none" class="col align-self-center"  >
                                    <div class="alert alert-warning" role="alert">
                                        <center>Quantity cannot be a negative number or value 0!</center>
                                    </div>  
                                </div>
                            </div>
                            <p></p>
                            <div class="strike">
                           <span><b>Quality</b></span>
                            </div>
                            <div class="row">
                                
                                <div class="col-lg-1">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Color</label>                                      
                                        <select class="form-control form-control-sm"  id="Col_Max" >
                                        <option value="0" selected >Choose...</option>
                                        
                                        </select>
                                    </div>
                                </div>
                                
                                <div class="col-lg-1">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Len Min</label>
                                        <input type="text" class="form-control form-control-sm" style="text-transform:uppercase;" id="Len_Min">
                                    </div>
                                </div>
                                <div class="col-lg-1">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Mic Cat</label>                                        
                                        <select class="form-control form-control-sm"  id="Mic_Cat" >
                                        <option value="" selected >Choose...</option>
                                            <option value="G7">G7</option>
                                            <option value="G6">G6</option>
                                            <option value="G5">G5</option>
                                            <option value="G4">G4</option>
                                            <option value="G3">G3</option>
                                            <option value="G2">G2</option>
                                            <option value="G1">G1</option>
                                            <option value="G0">G0</option>                                            
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-1">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Mic Min</label>
                                        <input class="form-control form-control-sm" style="text-transform:uppercase;" id="Mic_Min">
                                    </div>
                                </div>

                                <div class="col-lg-1">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Mic Max</label>
                                        <input  class="form-control form-control-sm" style="text-transform:uppercase;" id="Mic_Max">
                                    </div>
                                </div>

                                <div class="col-lg-1">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Str Min</label>
                                        <input  class="form-control form-control-sm" style="text-transform:uppercase;" id="Str_Min">
                                    </div>
                                </div>

                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Quality comment</label>
                                        <input type="text" class="form-control form-control-sm"  id="Quality_Comment">
                                    </div>
                                </div>
                                <div id="alerta_calidad" style="display: none" class="col align-self-center"  >
                                    <div class="alert alert-warning" role="alert">
                                        Mic min value is greater than Mic max value!
                                    </div>  
                                </div>
                            </div>    
                            <p></p>
                            <div class="strike">
                                <span><b>Buyer Bid</b></span>
                            </div>
                            <div class="row">                                
                                <div class="col-lg-2">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Basis (Pts.)</label>
                                        <input type="text" class="form-control form-control-sm" style="text-transform:uppercase;" id="Basis">
                                    </div>
                                </div>

                                <div class="col-lg-2">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Cover Month</label>                                        
                                        <select class="form-control form-control-sm"  id="Cover_Month">
                                        </select>
                                    </div>
                                </div>
                                <font size=2>*Required Fields</font>
                                <p></p>
                            </div>
                            <div id="divCreate">
                                <div class="modal-footer">                                 
                                    <button type="button" class="btn btn-light" id="btnCancel" data-bs-dismiss="modal">Cancel</button>
                                    <button type="submit" id="btnGuardar" class="btn btn-dark">Save</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <!-- MODAL PARA FILTROS -->
        <div class="modal hide fade" id="filtrarmodal" data-bs-backdrop="static" tabindex="-1" role="dialog" data-bs-keyboard="false" aria-hidden="true">
            <div class="modal-dialog modal-xl">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-tittle">Filter table by</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <form id="filtros" class="filtros">
                            <div class="strike">
                                <span>By Date</span>
                            </div>
                            <br>
                            <div class="row">
                                <div class="input-group mb-3">
                                    <label class="input-group-text" for="timeSelect">Select Range</label>
                                    <select class="form-select me-2" id="timeSelect" name="timeSelect">
                                        <option value="" selected >Choose...</option>
                                        <option value="7">Last week</option>
                                        <option value="14">Last 2 weeks</option>
                                        <option value="30">Last month</option>
                                        <option value="60">Last 2 months</option>
                                        <option value="120">Last 4 months</option>
                                        <option value="180">Last 6 months</option>
                                        <option value="365">Last 1 year </option>
                                    </select>                         
                                
                                    <label class="input-group-text" for="fromdate">From date:</label>
                                    <input type="date" class="form-control me-4" name="fromdate" id="fromdate">
                                    <label class="input-group-text" for="todate">To date:</label>
                                    <input type="date" class="form-control me-2" name="todate" id="todate">
                                </div>
                            </div>
                            <div class="strike">
                                <span>By Country</span>
                            </div>
                            <br>
                            <div class="row">                              
                                <div class="input-group mb-3">                                     
                                    <label for="" class="input-group-text">Origin Region</label >
                                    <select class="form-select me-2" id="RegionOrigen" required>
                                    </select>   

                                    <label for="" class="input-group-text">Origin Country</label >
                                    <select class="form-select me-2" id="OriginFiltros" >
                                    </select>                                   
                                </div>
                            </div>
                            <div class="strike">
                                <span>By Shipper</span>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-lg-4">                            
                                    <div class="input-group mb-3"> 
                                        <label for="" class="input-group-text">User</label >
                                        <select class="form-select me-2" id="userlist" >
                                        </select>  
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <div style="height: 2.5em;" id="aviso"></div>
                        <button id="cer" type="button" class="btn btn-light" value="3" data-bs-dismiss="modal" >Close</button>
                        <button id="borrarFiltro" type="button" value="0" class="btn btn-danger Filtro" >Clear filters</button>
                        <button id="buscafiltro" type="button" value="1" class="btn btn-primary Filtro" >Apply filters</button>
                    </div>
                </div>
            </div>
        </div>
        
    </body>
</html>
<?php
endif;
?>
