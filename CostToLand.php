<?php
 session_start();
 header("Cache-Control: no-cache, must-revalidate"); //para borrar cache
if (!isset($_SESSION['user_inquiries'])) :
    include_once('index.php');
else :
    
   
    $region = $_SESSION['Region'];
    $usuario=$_SESSION['user_inquiries'];
    $priv = $_SESSION['Priv'];
    $vistaComp=$_SESSION['ViewCompetition'];
    $vistaInq= $_SESSION['ViewInquirie'];
    $vistabuyer= $_SESSION['ViewBuyer'];
    $EditCompetition=$_SESSION['EditCompetition'];
    $Nombre=$_SESSION['Name'];
    $ImportCsvComp=$_SESSION['ImportCsvComp'];
    $EditCostToLand=$_SESSION['EditCostToLand'];
    $vistabasis = $_SESSION['basis'];
    $MailCostToLan= $_SESSION['MailCostToLan'];
    
  /*  if ($vistaComp == 0){
        header("Location:index.php"); 
    }*/
?>
    <!doctype html>
    <html lang="en-US">

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="shortcut icon" href="img/ecom.png" />
        <title>Cost To Land</title>

        <!-- CSS bootstrap -->
        <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
        <!--datables CSS básico-->
        <link rel="stylesheet" type="text/css" href="assets/datatables/datatables.min.css" />
        <!--datables estilo bootstrap 4 CSS-->
        <link rel="stylesheet" type="text/css" href="assets/datatables/DataTables-1.10.18/css/dataTables.bootstrap4.min.css">

        <!-- CSS personalizado -->
        <link rel="stylesheet" href="main.css">
        
        
        
        <!--Google fonts -->
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Inter:wght@300&family=Padauk&display=swap" rel="stylesheet">

        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      

        <!--font awesome con CDN  -->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.1/font/bootstrap-icons.css">
    </head>

    <body>

        <!-- Scripts -->

        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script>
   

        <!-- jQuery, Popper.js, Bootstrap JS -->
        <script src="assets/jquery/jquery-3.3.1.min.js"></script>
        <script src="assets/popper/popper.min.js"></script>
        <script src="assets/bootstrap/js/bootstrap.min.js"></script>

         <!-- librerias necesarias para finalizar sesion por inactividad -->
         <script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script>
        <!-- Scrip para finalizar sesion por inactividad -->
        <script type="text/javascript" src="timer.js"></script>



        <!-- datatables JS -->
        <script type="text/javascript" src="assets/datatables/datatables.min.js"></script>
        

        <!-- para usar botones en datatables JS -->
        <script src="datatables/Buttons-1.5.6/js/dataTables.buttons.min.js"></script>
        <script src="datatables/JSZip-2.5.0/jszip.min.js"></script>
        <script src="datatables/pdfmake-0.1.36/pdfmake.min.js"></script>
        <script src="datatables/pdfmake-0.1.36/vfs_fonts.js"></script>
        <script src="datatables/Buttons-1.5.6/js/buttons.html5.min.js"></script>

         <!-- libreria para exportar tabla a excell -->
         <script src="https://unpkg.com/xlsx/dist/xlsx.full.min.js"></script>
        
        <!-- Ficed columns -->
        <script src="https://cdn.datatables.net/fixedcolumns/4.0.0/js/dataTables.fixedColumns.min.js"></script>

        
     <script type="text/javascript" src="CostToLand.js"></script> 
      
        
      

        <!-- Terminan Scripts -->
            
        <!-- Esto es el Nav bar, todo contenido en un container-fluid -->
        <nav class="navbar navbar-expand-lg bg-transparent navbar-custom">
            <div class="container-fluid" style="padding-right: 1.5rem; padding-left: 1.5rem;">
                <div class="dropdown  nav-item col-xl-1 col-lg-1 col-md-2 col-sm-2 col-xs-1">
                    <a class="btn dropdown-toggle " href="#" role="button" id="dropdownMenuLink" data-bs-toggle="dropdown" aria-expanded="false">
                        <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" fill="white" class="bi bi-list" viewBox="0 0 16 16">
                            <path fill-rule="evenodd" d="M2.5 12a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5z" />
                        </svg>
                    </a>
                    
                    <ul class="dropdown-menu " aria-labelledby="dropdownMenuLink">
                        <?php if ($vistaInq == 1){?>      
                            <li><a class="dropdown-item" href="inquirie.php">Inquiries</a></li>
                        <?php } ?>  
                        <?php if ($vistaComp == 1){?>   
                            <li><a class="dropdown-item" href="competition.php">Competition Offers</a></li>                                
                        <?php } ?>  
                        <?php if ($vistabuyer == 1){?>           
                            <li><a class="dropdown-item" href="buyers.php">Buyers</a></li>    
                        <?php } ?> 
                         
                        <li><a class="dropdown-item" style="background-color: #5a926d;" href="CostToLand.php">Cost to Land</a></li> 
                        <?php if ($vistabasis  == 1){?>                            
                        <li><a class="dropdown-item" href="Basis.php">Origin Basis</a></li> 
                        <?php } ?>                              

                        
                    </ul>


                <div id="gin" style="display:none;">
                     <input id="priv" value="<?php echo $priv; ?>"/>
                     <input id="usuario" value="<?php echo $usuario; ?>"/>
                     <input id="editar" value="<?php echo $EditCompetition; ?>"/>
                     <input id="region" value="<?php echo $region; ?>"/>
                     <input id="nombre" value="<?php echo $Nombre; ?>"/>
                     <input id="editCost" value="<?php echo $EditCostToLand; ?>"/>
                     
                </div> 


                </div>
                <a class="navbar-brand nav-item col-xl-1 col-lg-1 col-md-1 col-sm-2 col-xs-2" href="main.php">
                    <div class="logos"><img class="log" src="img/logo1.png">ECOM/Cost To Land</div>
                </a>
                <div class="col-xl-1 col-lg-1 col-md-1 col-sm-1 col-xs-1 tex">
                    <!--<p> Inquiries </p> -->
                </div>

                 <!--botónes  -->
                <div class="container-fluid  col-xl-7 col-lg-6 col-md-3 col-sm-2 col-xs-1">
                
                <?php if ($MailCostToLan == 1){?>      
                  <button id="PsendEmail" type="button" class="btn btn-primary" data-toggle="modal tooltip" data-placement="bottom" title="Send Mail"><i class="bi bi-envelope"></i>
                <?php } ?>   
                   
                </div>


                <div class="dropdown nav-item col-xl-2 col-lg-3 col-md-3 col-sm-3 col-xs-1 d-flex justify-content-end" style="padding-right: 1.5rem;">
                <button style="color:white;" class="btn dropdown-toggle btn-outline-success" data-bs-toggle="dropdown" aria-expanded="false" id="dropdownMenuLink2"  value="<?php echo $_SESSION['user_inquiries']; ?>"><?php echo $_SESSION['user_inquiries']; ?></button>
                    <ul class="dropdown-menu" aria-labelledby="dropdownMenuLink2">
                        <li><a class="dropdown-item" href="logout.php">Close sesion</a></li>
                    </ul>
                </div>
            </div>
        </nav>
                      
        <!-- CONTADOR DE REGITROS
        <div class="col-xs-3">
        <div class="input-group mb-1">
        <label class="input-group-text" >LiqID</label>
        <label class="input-group-text"  id="totaLiqID" >0</label>
        <label class="input-group-text" >Total Bales</label>
        <label class="input-group-text"  id="bales" >0</label>
   
        </div>
        </div>    
        -->

        <style type="text/css">
            .sinborde {
             background-color:0;
             border: 0;
            }

            .colortexto{
                font-weight:bold;
            }
            table {
            width: 100%;
            border-collapse: collapse;
            }

            table td {
                padding: 5px;
            }

            input[type="text"] {
                width: 80%;
                box-sizing: border-box;
            }
        </style>
 
    <div class="card card-body " style="opacity:100%; " >   
        <div class="table-responsive" style="opacity:100%;">
            <div class="col-xs-3">
            <div class="input-group mb-1">
            <label class="input-group-text" >Search by Date</label>
            <label class="input-group-text"  id="totaLiqID" >
            <input type="date" class="form-control form-control-sm" id="fecha" onkeydown="return false"  lang="en-US" ></label>
            <label class="input-group-text" ><button id="btnbuscar"class='btn btn-primary  btn-sm' title='Search' style="transform: scale(0.8);"><i class='material-icons'>search</i></button></label>            
            <label class="input-group-text" ><button id="exportar" type="button" class="btn btn-success" data-toggle="modal tooltip"   style="transform: scale(0.82);" data-placement="bottom" title="Export to Excel"><i class="bi bi-file-earmark-excel"></i></button></label>
            </div>
            </div>           
                   
            <table id="table_costtoland"  class="table bg-white table-striped row-border order-column table-hover table table-bordered tablas" style="opacity:100%; ">                     
                    <thead style="background-color: #65ac7c;" style="opacity:100%;">                   
                            <tr>        
                                <th >ORIGIN</th>
                                <th >BGD</th>       
                                <th >BRA</th>
                                <th >CHN</th>
                                <th >IDN</th>
                                <th >IND</th>
                                <th >ITA</th>
                                <th >MEX</th>
                                <th >PAK</th>
                                <th >PRT</th>
                                <th >TUR</th>
                                <th >TWN</th>  
                                <th >USA</th>
                                <th >VNM</th>
                                <th class="no-exportar"></th>                              
                            </tr>
                    </thead>                    
                      
                    <tbody class="sinborde"> 
                      
                    </tbody>
            </table>
        </div>

    </div>
    
    <!-- CORREO DE RECORDATORIO -->
    
            <div class="modal hide fade" id="modal-msj" data-bs-backdrop="static" tabindex="-1" role="dialog" data-bs-keyboard="false" aria-hidden="true">
            <div class="modal-dialog modal-xl">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-tittle" style="">Preview email</h4>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <form id="Msj" class="Msj">
                            <div class="input-group mt-3">
                                <label class="input-group-text" for="subject">Subject</label>
                                <input type="text" class="form-control subject" id="subject" value="Cost to Land - Update Reminder" readonly>
                            </div>
                            <br>
                            <div class="body-mail">
                                <h5>Mails:</h5>
                            </div>
                            <div class="input-group mt-3">
                                <textarea type="text" rows="4" placeholder="Separados por coma" class="form-control extraEmails" id="maildestino"></textarea>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        <button id="sendEmail" type="button" class="btn btn-primary sendEmail">Send Email</button>
                    </div>
                </div>
            </div>
        </div>

        




    </body>

    </html>
<?php
endif;
?>
