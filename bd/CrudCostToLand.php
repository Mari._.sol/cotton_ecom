<?php
include_once '../bd/conexion.php';
$objeto = new Conexion();
$conexion = $objeto->Conectar();
$opcion = (isset($_POST['opcion'])) ? $_POST['opcion'] : '';
$fecha = (isset($_POST['fecha'])) ? $_POST['fecha'] : '';
$usuario=(isset($_POST['usuario'])) ? $_POST['usuario'] : '';
$arreglo = (isset($_POST['arreglo'])) ? $_POST['arreglo'] : '';
$fechaActual = date('Y-m-d');
$origenes=['AUS','BRA','GRC','IND','MEX','PRY','SPA','TUR','USA-MS','USA-PM','USA-SE','USA-STX','USA-WTX','WAF'];
//$opcion=1;
$data = [];
switch($opcion){

    //obtener todos los datos del historicos 
    case 1:

        $tam=count($origenes);
        for($i=0; $i<$tam; $i++){

        $origen = $origenes[$i];

         $consulta = "SELECT   IF((SELECT DISTINCT Origin  FROM Costtoland WHERE Origin ='$origen')IS NOT NULL ,(SELECT DISTINCT Origin  FROM Costtoland WHERE Origin ='$origen'),'$origen') as Origen,
        (SELECT IFNULL((SELECT Basis FROM Costtoland WHERE Origin ='$origen' AND Destination='BGD' AND Date > date_sub('$fecha',interval 28 day )  ORDER BY Date DESC,IDCost DESC  LIMIT 1), '')) as GRC_BGD,
        (SELECT IFNULL((SELECT Basis FROM Costtoland WHERE Origin ='$origen' AND Destination='BRA' AND Date > date_sub('$fecha',interval 28 day )  ORDER BY Date DESC,IDCost DESC  LIMIT 1), '')) as GRC_BRA,       
        (SELECT IFNULL((SELECT Basis FROM Costtoland WHERE Origin ='$origen' AND Destination='CHN' AND Date > date_sub('$fecha',interval 28 day )  ORDER BY Date DESC,IDCost DESC  LIMIT 1), '')) as GRC_CHN,
        (SELECT IFNULL((SELECT Basis FROM Costtoland WHERE Origin ='$origen' AND Destination='IDN' AND Date > date_sub('$fecha',interval 28 day )  ORDER BY Date DESC,IDCost DESC  LIMIT 1), '')) as GRC_IDN,
        (SELECT IFNULL((SELECT Basis FROM Costtoland WHERE Origin ='$origen' AND Destination='IND' AND Date > date_sub('$fecha',interval 28 day )  ORDER BY Date DESC,IDCost DESC  LIMIT 1), '')) as GRC_IND,
        (SELECT IFNULL((SELECT Basis FROM Costtoland WHERE Origin ='$origen' AND Destination='ITA' AND Date > date_sub('$fecha',interval 28 day )  ORDER BY Date DESC,IDCost DESC  LIMIT 1), '')) as GRC_ITA,
        (SELECT IFNULL((SELECT Basis FROM Costtoland WHERE Origin ='$origen' AND Destination='MEX' AND Date > date_sub('$fecha',interval 28 day )  ORDER BY Date DESC,IDCost DESC  LIMIT 1), '')) as GRC_MEX,
        (SELECT IFNULL((SELECT Basis FROM Costtoland WHERE Origin ='$origen' AND Destination='PAK' AND Date > date_sub('$fecha',interval 28 day )  ORDER BY Date DESC,IDCost DESC  LIMIT 1), '')) as GRC_PAK,
        (SELECT IFNULL((SELECT Basis FROM Costtoland WHERE Origin ='$origen' AND Destination='PRT' AND Date > date_sub('$fecha',interval 28 day )  ORDER BY Date DESC,IDCost DESC  LIMIT 1), '')) as GRC_PRT,
        (SELECT IFNULL((SELECT Basis FROM Costtoland WHERE Origin ='$origen' AND Destination='TUR' AND Date > date_sub('$fecha',interval 28 day )  ORDER BY Date DESC,IDCost DESC  LIMIT 1), '')) as GRC_TUR,
        (SELECT IFNULL((SELECT Basis FROM Costtoland WHERE Origin ='$origen'AND Destination='TWN' AND Date > date_sub('$fecha',interval 28 day )  ORDER BY Date DESC,IDCost DESC  LIMIT 1), '')) as GRC_TWN,
        (SELECT IFNULL((SELECT Basis FROM Costtoland WHERE Origin ='$origen' AND Destination='USA' AND Date > date_sub('$fecha',interval 28 day )  ORDER BY Date DESC,IDCost DESC  LIMIT 1), '')) as GRC_USA,
        (SELECT IFNULL((SELECT Basis FROM Costtoland WHERE Origin ='$origen' AND Destination='VNM' AND Date > date_sub('$fecha',interval 28 day )  ORDER BY Date DESC,IDCost DESC  LIMIT 1), '')) as GRC_VNM ;";
       
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        array_push($data, $resultado->fetchAll(PDO::FETCH_NUM));

        }



        //print_r($data);

    break; 

    case 2:
       // print_r(gettype($arreglo));
        //($arreglo);

        


        $consulta  = 'INSERT INTO Costtoland (Date, Origin, Destination, Basis, User) VALUES (:fecha, :origen, :destino, :valor, :usuario)';
        $resultado = $conexion->prepare($consulta);
        foreach ($arreglo as $fila){
           
            $aux=json_decode($fila);
            //print_r($aux);
            foreach ($aux as $datos){
                if($datos->basis==""){
                    $val=NULL;
                }
                else{
                    $val=$datos->basis;
                }

                $consulta1 ="SELECT IDCost FROM Costtoland WHERE Date='$fecha' AND  Origin ='$datos->origin' AND Destination='$datos->destino' ORDER BY Date,IDCost DESC  LIMIT 1";
                $resultado1 = $conexion->prepare($consulta1);
                $resultado1->execute();     
                $idcosto=$resultado1->fetch();   
                //print_r($idcosto);
               
                if(empty($idcosto)){

                $resultado->bindValue(':fecha', $fecha);
                $resultado->bindValue(':origen', $datos->origin);
                $resultado->bindValue(':destino', $datos->destino);
                $resultado->bindValue(':valor', $val);
                $resultado->bindValue(':usuario', $usuario);
                
                $resultado->execute();
                }
                else{
                    $idcost=$idcosto['IDCost'];

                    $consulta2  = "UPDATE  Costtoland SET Origin=:origen, Destination=:destino, Basis=:valor WHERE  IDCost='$idcost' ;";
                    $resultado2 = $conexion->prepare($consulta2);
                    $resultado2->bindValue(':origen', $datos->origin);
                    $resultado2->bindValue(':destino', $datos->destino);
                    $resultado2->bindValue(':valor', $val);
                    $resultado2->execute();
                    
                }                       
            }
          
       
        }
        $data ="Changed Data";
    
    break;

    case 3:
        $tam=count($origenes);
        for($i=0; $i<$tam; $i++){

        $origen = $origenes[$i];

         $consulta = "SELECT   IF((SELECT DISTINCT Origin  FROM Costtoland WHERE Origin ='$origen')IS NOT NULL ,(SELECT DISTINCT Origin  FROM Costtoland WHERE Origin ='$origen'),'$origen') as Origen,
        (SELECT IFNULL((SELECT Basis FROM Costtoland WHERE Origin ='$origen' AND Destination='BGD' AND Date BETWEEN date_sub('$fecha',interval 28 day ) AND '$fecha' ORDER BY Date DESC,IDCost DESC  LIMIT 1), '')) as GRC_BGD,
        (SELECT IFNULL((SELECT Basis FROM Costtoland WHERE Origin ='$origen' AND Destination='BRA' AND Date BETWEEN date_sub('$fecha',interval 28 day ) AND '$fecha' ORDER BY Date DESC,IDCost DESC  LIMIT 1), '')) as GRC_BRA,       
        (SELECT IFNULL((SELECT Basis FROM Costtoland WHERE Origin ='$origen' AND Destination='CHN' AND Date BETWEEN date_sub('$fecha',interval 28 day ) AND '$fecha'  ORDER BY Date DESC,IDCost DESC  LIMIT 1), '')) as GRC_CHN,
        (SELECT IFNULL((SELECT Basis FROM Costtoland WHERE Origin ='$origen' AND Destination='IDN' AND Date BETWEEN date_sub('$fecha',interval 28 day ) AND '$fecha'  ORDER BY Date DESC,IDCost DESC  LIMIT 1), '')) as GRC_IDN,
        (SELECT IFNULL((SELECT Basis FROM Costtoland WHERE Origin ='$origen' AND Destination='IND' AND Date BETWEEN date_sub('$fecha',interval 28 day ) AND '$fecha'  ORDER BY Date DESC,IDCost DESC  LIMIT 1), '')) as GRC_IND,
        (SELECT IFNULL((SELECT Basis FROM Costtoland WHERE Origin ='$origen' AND Destination='ITA' AND Date BETWEEN date_sub('$fecha',interval 28 day ) AND '$fecha'  ORDER BY Date DESC,IDCost DESC  LIMIT 1), '')) as GRC_ITA,
        (SELECT IFNULL((SELECT Basis FROM Costtoland WHERE Origin ='$origen' AND Destination='MEX' AND Date BETWEEN date_sub('$fecha',interval 28 day ) AND '$fecha'  ORDER BY Date DESC,IDCost DESC  LIMIT 1), '')) as GRC_MEX,
        (SELECT IFNULL((SELECT Basis FROM Costtoland WHERE Origin ='$origen' AND Destination='PAK' AND Date BETWEEN date_sub('$fecha',interval 28 day ) AND '$fecha'  ORDER BY Date DESC,IDCost DESC  LIMIT 1), '')) as GRC_PAK,
        (SELECT IFNULL((SELECT Basis FROM Costtoland WHERE Origin ='$origen' AND Destination='PRT' AND Date BETWEEN date_sub('$fecha',interval 28 day ) AND '$fecha'  ORDER BY Date DESC,IDCost DESC  LIMIT 1), '')) as GRC_PRT,
        (SELECT IFNULL((SELECT Basis FROM Costtoland WHERE Origin ='$origen' AND Destination='TUR' AND Date BETWEEN date_sub('$fecha',interval 28 day ) AND '$fecha'  ORDER BY Date DESC,IDCost DESC  LIMIT 1), '')) as GRC_TUR,
        (SELECT IFNULL((SELECT Basis FROM Costtoland WHERE Origin ='$origen' AND Destination='TWN' AND Date BETWEEN date_sub('$fecha',interval 28 day ) AND '$fecha'  ORDER BY Date DESC,IDCost DESC  LIMIT 1), '')) as GRC_TWN,
        (SELECT IFNULL((SELECT Basis FROM Costtoland WHERE Origin ='$origen' AND Destination='USA' AND Date BETWEEN date_sub('$fecha',interval 28 day ) AND '$fecha'  ORDER BY Date DESC,IDCost DESC  LIMIT 1), '')) as GRC_USA,
        (SELECT IFNULL((SELECT Basis FROM Costtoland WHERE Origin ='$origen' AND Destination='VNM' AND Date BETWEEN date_sub('$fecha',interval 28 day ) AND '$fecha'  ORDER BY Date DESC,IDCost DESC  LIMIT 1), '')) as GRC_VNM;";
       
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        array_push($data, $resultado->fetchAll(PDO::FETCH_NUM));

        }

    
    break;
    
     case 4:
        $consulta = "SELECT Email FROM Users_Inquiries WHERE EditCostToLand = 1 AND Email != 'NULL';";
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $data=$resultado->fetchAll(PDO::FETCH_COLUMN, 0);
        $data = implode (',',$data); 
    break;



   
}
print json_encode($data, JSON_UNESCAPED_UNICODE);

?>
