<?php
  require '../vendor/autoload.php';
  use PHPMailer\PHPMailer\PHPMailer;
  use PHPMailer\PHPMailer\SMTP;

  include_once '../bd/conexion.php';
  $objeto = new Conexion();
  $conexion = $objeto->Conectar();

  require '../bd/vendor/phpmailer/phpmailer/src/PHPMailer.php';
  require '../bd/vendor/phpmailer/phpmailer/src/SMTP.php';

  $asunto= (isset($_POST['asunto'])) ? $_POST['asunto'] : '';
  $correos= (isset($_POST['correos'])) ? $_POST['correos'] : '';
  $User= (isset($_POST['User'])) ? $_POST['User'] : '';

  date_default_timezone_set('America/Los_Angeles');

  $query = 'SELECT Name,Email,PassApp FROM amsadb1.Users_Inquiries WHERE User = "'.$User.'"';
  $result =$conexion->prepare($query);
  $result->execute();
  $datosuser=$result->fetch(PDO::FETCH_ASSOC);

  $email=$datosuser['Email'];
  $pass =$datosuser['PassApp'];
  $usuario =$datosuser['Name'];

  $origenes = ['AUS', 'BRA', 'GRC', 'IND', 'MEX', 'PRY', 'SPA', 'TUR', 'USA-MS', 'USA-PM', 'USA-SE', 'USA-STX', 'USA-WTX', 'WAF'];
  $origenesData = array_fill_keys($origenes, 0);
  $placeholders = implode(', ', array_fill(0, count($origenes), '?'));
  $consulta = "SELECT Origin, COALESCE(MAX(Date), 0) as fecha FROM amsadb1.Costtoland WHERE Origin IN ($placeholders) GROUP BY Origin ORDER BY Origin";
  $resultado = $conexion->prepare($consulta);
  $resultado->execute($origenes);
  $tabla = $resultado->fetchAll(PDO::FETCH_ASSOC);

  foreach ($tabla as $row) {
    $origenesData[$row['Origin']] = $row['fecha'];
  }

  //hoy menos 15 dias
  $hoy = date('Y-m-d', strtotime('-15 days'));
  $formatoFecha = 'Y-m-d';

  $complementoTabla = "
  <table style='width: 230px;' border='1'>
  <tr>
    <th style='background-color:#CDCDCD'>Origin</th>
    <th style='background-color:#CDCDCD'>Last Update</th>
  </tr>";

  foreach ($origenesData as $origen => $maxDat) {
    // Comparar la fecha con $hoy
    if ($maxDat !== 0 && $maxDat < $hoy) {
      $estilo = 'style="color: red;"';
    } else {
      $estilo = '';
    }

    $complementoTabla .= "
    <tr " . $estilo . ">
      <td>" . $origen . "</td>
      <td>" . ($maxDat !== 0 ? date($formatoFecha, strtotime($maxDat)) : '') . "</td>
    </tr>";
  }
  $complementoTabla .= "</table>";

 // Intancia de PHPMailer
 $mail = new PHPMailer();
 // Es necesario para poder usar un servidor SMTP como gmail
 $mail->isSMTP();
 // Si estamos en desarrollo podemos utilizar esta propiedad para ver mensajes de error
 //SMTP::DEBUG_OFF    = off (for production use) 0
 $mail->SMTPDebug = 0; //SMTP::DEBUG_SERVER;
 //Set the hostname of the mail server
 $mail->Host = 'smtp.gmail.com';
 $mail->Port  = 465; // 465 o 587
 // Propiedad para establecer la seguridad de encripción de la comunicación
 $mail->SMTPSecure = PHPMailer::ENCRYPTION_SMTPS; // tls o ssl para gmail obligado
 // Para activar la autenticación smtp del servidor
 $mail->SMTPAuth = true;
 //$mail->SMTPAuthTLS      = false;
 // Credenciales de la cuenta
 $mail->Username = $email;
 $mail->Password = $pass;
 // Quien envía este mensaje
 $mail->setFrom($email, $usuario);
 // Si queremos una dirección de respuesta
 //$mail->addReplyTo('replyto@panchos.com', 'Pancho Doe');
 // Destinatario
 $correos = "marisol.hernandez@ecomtrading.com";//,josue.reyes@ecomtrading.com
 $mail->addAddress($correos);
/*  $arraycorreo = explode(",",$correos);
 $tam = sizeof($arraycorreo);
 for($i=0; $i<$tam; $i++){
  $mail->addAddress($arraycorreo[$i]);
 } */

  // Asunto del correo
  $mail->Subject = $asunto;
  // Contenido
  $mail->IsHTML(true);
  $mail->CharSet = 'UTF-8';

  $bodyy ='
  Hi all, <br><br>

  In order to have as much information as possible to analyze, could you please log in to the portal and update the <b>Cost to Land</b> data.<br><br>
  
  <b>Link:</b> www.ecom-tracking.com/_Cotton_ECOM/CostToLand.php<br><br>
    
  The following table shows the last capture from each of the origins: <br><br>'.$complementoTabla.'<br><br>

  <b>Note:</b> If you do not have your username or password, you can send me an email with a copy to: 
  cottonportal@ecomtrading.com<br><br><br>

  Your support is greatly appreciated.<br>
  Regards.';


  $mail->Body = $bodyy;
  
  if ($mail->send()) {
    echo 'Correo enviado correctamente';
  } else {
    echo 'Error al enviar el correo: ' . $mail->ErrorInfo;
  }
?>