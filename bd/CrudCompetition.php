
<?php
include_once '../bd/conexion.php';
$objeto = new Conexion();
$conexion = $objeto->Conectar();
$opcion = (isset($_POST['opcion'])) ? $_POST['opcion'] : '';
$origen = (isset($_POST['origen'])) ? $_POST['origen'] : '';
$Date= (isset($_POST['Date'])) ? $_POST['Date'] : '';
$Destination= (isset($_POST['Destination'])) ? $_POST['Destination'] : '';
$Origin= (isset($_POST['Origin'])) ? $_POST['Origin'] : '';
$Origin2=(isset($_POST['Origin2'])) ? $_POST['Origin2'] : '';
$Quantity=(isset($_POST['Quantity'])) ? $_POST['Quantity'] : '';
$CropFrom=(isset($_POST['CropFrom'])) ? $_POST['CropFrom'] : '';
$CropTo=(isset($_POST['CropTo'])) ? $_POST['CropTo'] : '';
$ColMax=(isset($_POST['ColMax'])) ? $_POST['ColMax'] : '';
$LenMin=(isset($_POST['LenMin'])) ? $_POST['LenMin'] : '';
$MicMin=(isset($_POST['MicMin'])) ? $_POST['MicMin'] : '';
$MicMax=(isset($_POST['MicMax'])) ? $_POST['MicMax'] : '';
$StrMin=(isset($_POST['StrMin'])) ? $_POST['StrMin'] : '';
$QualityComment=(isset($_POST['QualityComment'])) ? $_POST['QualityComment'] : '';
$StartShip=(isset($_POST['StartShip'])) ? $_POST['StartShip'] : '';
$EndShip=(isset($_POST['EndShip'])) ? $_POST['EndShip'] : '';
$ShipComment=(isset($_POST['ShipComment'])) ? $_POST['ShipComment'] : '';
$Basis=(isset($_POST['Basis'])) ? $_POST['Basis'] : '';
$Unit=(isset($_POST['Unit'])) ? $_POST['Unit'] : '';
$CoverMonth=(isset($_POST['CoverMonth'])) ? $_POST['CoverMonth'] : '';
$PaymentTerms=(isset($_POST['PaymentTerms'])) ? $_POST['PaymentTerms'] : '';
$OfferComments=(isset($_POST['OfferComments'])) ? $_POST['OfferComments'] : '';
$Comments=(isset($_POST['Comments'])) ? $_POST['Comments'] : '';
$Usuario=(isset($_POST['Usuario'])) ? $_POST['Usuario'] : '';
$IDReq=(isset($_POST['IDReq'])) ? $_POST['IDReq'] : '';
$Zone=(isset($_POST['pais'])) ? $_POST['pais'] : '';
$zoneorigen=(isset($_POST['zoneorigen'])) ? $_POST['zoneorigen'] : '';
$zonedest=(isset($_POST['zonedest'])) ? $_POST['zonedest'] : '';
$priv=(isset($_POST['Priv'])) ? $_POST['Priv'] : '';
$Shipper=(isset($_POST['Shipper'])) ? $_POST['Shipper'] : '';
$Price=(isset($_POST['Price'])) ? $_POST['Price'] : '';
$CountryDest=(isset($_POST['CountryDest'])) ? $_POST['CountryDest'] : '';

//// Variables para filtros
session_start();

$filterSes = $_SESSION['Filters'];

$FromDate=$filterSes['FromDate'];
$ToDate=$filterSes['ToDate'];
$OriginReg=$filterSes['OriginReg'];
$OriginCount=$filterSes['OriginCount'];
$DestReg=$filterSes['DestReg'];
$DestCount=$filterSes['DestCount'];
$ShipperFilter=$filterSes['ShipperFilter'];
$UserComp=$filterSes['UserComp'];      


switch($opcion){
    case 3:   
        $consulta = "SELECT DISTINCT CountryCode,CountryName FROM Inquiries_Countries WHERE IsDestination = '1' AND SubRegion = '$Zone' ORDER BY CountryName ASC";      
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();        
        $data=$resultado->fetchAll(PDO::FETCH_ASSOC);    
    break; 
    case 4:  
        $consulta = "SELECT CountryCode,CountryName FROM Inquiries_Countries WHERE IsOrigin = '1' AND SubRegion = '$Zone' ORDER BY CountryName ASC";      
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();        
        $data=$resultado->fetchAll(PDO::FETCH_ASSOC);    
    break;    
       
    case 5:
        $consulta = "SELECT IDZone,Zone  FROM Inquiries_Zones WHERE CountryCode ='$origen'  ORDER BY Zone ASC";      
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();        
        $data=$resultado->fetchAll(PDO::FETCH_ASSOC);       
    break;

    case 8:
        $consulta = "INSERT INTO Inquiries_Inquirie (Date,Destination,Origin,Origin2,Quantity,CropFrom,CropTo,ColMax,LenMin,MicMin,MicMax,StrMin,QualityComment,StartShip,EndShip,ShipComment,Basis,Unit,
        CoverMonth,PaymentTerms,OfferComments,Comments,User,RegionOrigin,RegionDest,Type,Shipper,Price) VALUES ('$Date','$Destination','$Origin','$Origin2','$Quantity','$CropFrom','$CropTo','$ColMax','$LenMin','$MicMin',
        '$MicMax','$StrMin','$QualityComment','$StartShip','$EndShip','$ShipComment','$Basis','$Unit','$CoverMonth','$PaymentTerms','$OfferComments','$Comments','$Usuario','$zoneorigen','$zonedest',2,'$Shipper','$Price')";      
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();        
        $data=$resultado->fetchAll(PDO::FETCH_ASSOC);       
    break;

    case 9:
        //CARGAR TABLA  
        $complemeto="";
        
        if($priv==1){
            $complemeto=" WHERE Inquiries_Inquirie.Type = '2' ORDER BY IDInquirie  DESC LIMIT 200;";
        }
        else{
            $complemeto=" WHERE Inquiries_Inquirie.User = '$Usuario' AND Inquiries_Inquirie.Type='2' ORDER BY IDInquirie  DESC ;";
        }       

    
        //VALIDAR LOS FILTROS 
        if($FromDate=="" && $ToDate=="" && $OriginReg =="" && $OriginCount =="" &&  $DestReg =="" &&  $DestCount =="" &&  $ShipperFilter =="" &&  $UserComp==""){
      
            $consulta = "SELECT IDInquirie,Date,Quantity,CropFrom,CropTo,ColMax,LenMin,MicMin,MicMax,StrMin,QualityComment,
            StartShip,EndShip,ShipComment,Basis,Unit,PaymentTerms,OfferComments,Comments,RegionOrigin,RegionDest,Inquiries_Countries.CountryName as Destination,
            Countries.CountryName as Origin,Origin2,Shipper,CoverMonth,U.Name as NameUser,Inquiries_Inquirie.Price
            FROM Inquiries_Inquirie 
            LEFT JOIN amsadb1.Inquiries_Countries
            ON Inquiries_Countries.CountryCode = Inquiries_Inquirie.Destination
            LEFT JOIN amsadb1.Inquiries_Countries as Countries 
            ON Countries.CountryCode = Inquiries_Inquirie.Origin
                
        
            LEFT JOIN Users_Inquiries as U
            ON U.User = Inquiries_Inquirie.User ".$complemeto;

            $resultado = $conexion->prepare($consulta);
            $resultado->execute();      
        
        } 

        else{

            $finconsulta=" ORDER BY IDInquirie  DESC";
            $filtro1="";
            if($OriginReg != ""){
                $filtro1 .= " AND RegionOrigin = '$OriginReg'";
            }
            if($OriginCount != ""){
                $filtro1 .= " AND Origin = '$OriginCount' ";
            }

            if($DestReg != ""){
                $filtro1 .= " AND RegionDest = '$DestReg' ";
            }

            if($DestCount != ""){
                $filtro1 .= " AND Destination = '$DestCount' ";
            }

            if($ShipperFilter != ""){
                $filtro1 .= " AND Shipper = '$ShipperFilter' ";
            }

            if($UserComp != ""){       

                $filtro1 .= " AND Inquiries_Inquirie.User = '$UserComp' ";
            }



            $consulta = "SELECT IDInquirie,Date,Quantity,CropFrom,CropTo,ColMax,LenMin,MicMin,MicMax,StrMin,QualityComment,
            StartShip,EndShip,ShipComment,Basis,Unit,PaymentTerms,OfferComments,Comments,RegionOrigin,RegionDest,Inquiries_Countries.CountryName as Destination,
            Countries.CountryName as Origin,Origin2,Shipper,CoverMonth,U.Name as NameUser,Inquiries_Inquirie.Price
            FROM Inquiries_Inquirie 
            LEFT JOIN amsadb1.Inquiries_Countries
            ON Inquiries_Countries.CountryCode = Inquiries_Inquirie.Destination
            LEFT JOIN amsadb1.Inquiries_Countries as Countries 
            ON Countries.CountryCode = Inquiries_Inquirie.Origin 
            LEFT JOIN Users_Inquiries as U
            ON U.User = Inquiries_Inquirie.User  WHERE Inquiries_Inquirie.Type = '2' AND (Date BETWEEN  '$FromDate' AND '$ToDate')".$filtro1.$finconsulta;
            
            //print_r($consulta);
            $resultado = $conexion->prepare($consulta);
            $resultado->execute();      
         
        }


        $data=$resultado->fetchAll(PDO::FETCH_ASSOC);   
        
    break;

    case 10:

        $consulta = " SELECT Inquiries_Countries.CountryCode as IDdestino,
        (SELECT origen.CountryCode FROM Inquiries_Countries as origen where origen.CountryName='$Origin') as IDOrigen        
        FROM Inquiries_Countries
        where Inquiries_Countries.CountryName='$Destination'";      
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();        
        $data=$resultado->fetchAll(PDO::FETCH_ASSOC);   
       
    break;

    case 11:
        $consulta = " UPDATE Inquiries_Inquirie  SET Date = '$Date', Destination = '$Destination', Origin='$Origin', Origin2='$Origin2',
        Quantity = '$Quantity',CropFrom='$CropFrom', CropTo = '$CropTo',
        ColMax = '$ColMax', LenMin='$LenMin', MicMin = '$MicMin', MicMax = $MicMax, StrMin = '$StrMin', QualityComment = '$QualityComment',
        StartShip = '$StartShip', EndShip = '$EndShip', ShipComment = '$ShipComment',Basis='$Basis', Unit = '$Unit', CoverMonth = '$CoverMonth',
        PaymentTerms = '$PaymentTerms', OfferComments = '$OfferComments', Comments = '$Comments',
        RegionOrigin ='$zoneorigen',RegionDest='$zonedest',Shipper='$Shipper', Price='$Price'
        where Inquiries_Inquirie.IDInquirie='$IDReq'";      
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();        
        $data=$resultado->fetchAll(PDO::FETCH_ASSOC);   
    break;

    case 12:
        $consulta = "SELECT  DISTINCT DISTINCT SubRegion,Region FROM Inquiries_Countries WHERE IsDestination = '1' ORDER BY Region ASC";      
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();        
        $data=$resultado->fetchAll(PDO::FETCH_ASSOC);    
    break;

    case 13:
        $consulta = "SELECT * FROM Inquiries_Covmon WHERE  Inquiries_Covmon.Available = '1' ";      
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();        
        $data=$resultado->fetchAll(PDO::FETCH_ASSOC);    
    break;

    case 14:
        $consulta = "SELECT  DISTINCT SubRegion,Region FROM Inquiries_Countries WHERE IsOrigin = '1' ORDER BY Region ASC";      
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();        
        $data=$resultado->fetchAll(PDO::FETCH_ASSOC);    
    break;
    
     case 15:
        $consulta = "SELECT  DISTINCT Users_Inquiries.Name,Users_Inquiries.User 
        FROM Inquiries_Inquirie,Users_Inquiries 
        WHERE Inquiries_Inquirie.User = Users_Inquiries.User AND Inquiries_Inquirie.Type = 2
        ORDER BY Name ASC";      

        $resultado = $conexion->prepare($consulta);
        $resultado->execute();        
        $data=$resultado->fetchAll(PDO::FETCH_ASSOC);    
    break;
    case 16:
        $consulta = "INSERT INTO Inquiries_Inquirie (Date, Destination, Origin, Origin2, Quantity, Shipper, CropFrom, CropTo,  ColMax, LenMin, MicMin, MicMax, StrMin, QualityComment, StartShip, EndShip, ShipComment, Basis, Unit, CoverMonth, PaymentTerms, OfferComments, Comments, RegionOrigin, RegionDest, Price, User, Type) 
                     VALUES ('$Date', '$Destination', '$Origin', '$Origin2', '$Quantity', '$Shipper', '$CropFrom', '$CropTo', '$ColMax', '$LenMin', '$MicMin', '$MicMax', '$StrMin', :QualityComment, '$StartShip', '$EndShip', :ShipComment, '$Basis', '$Unit', '$CoverMonth', '$PaymentTerms', :OfferComments, :Comments, '$zoneorigen', '$zonedest', '$Price','$Usuario', 2)";
        $resultado = $conexion->prepare($consulta);  
        $resultado->bindValue(':QualityComment', $QualityComment, PDO::PARAM_STR);
        $resultado->bindValue(':OfferComments', $OfferComments, PDO::PARAM_STR);
        $resultado->bindValue(':Comments', $Comments, PDO::PARAM_STR);
        $resultado->bindValue(':ShipComment', $ShipComment, PDO::PARAM_STR);
        $resultado->execute();        
        $data = $resultado->fetchAll(PDO::FETCH_ASSOC);   
    break;
    case 17:
        print_r($origen,$CountryDest);
        $consulta = "SELECT Max(IDInquirie) as IDInq, RegionOrigin, Origin, Origin2, RegionDest, Destination, Shipper, Quantity,
            CropFrom, CropTo, ColMax, LenMin, MicMin, MicMax, StrMin, QualityComment, StartShip, EndShip,
            ShipComment, Basis, CoverMonth, PaymentTerms, Price, Unit, OfferComments, Comments
            FROM inquiries_inquirie
            WHERE
                Date = (SELECT MAX(Date) FROM inquiries_inquirie WHERE Origin = '$origen' AND Destination = '$CountryDest')
                AND Origin ='$origen'
                AND Destination = '$CountryDest'";
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();        
        $data = $resultado->fetchAll(PDO::FETCH_ASSOC);   
        //print_r($data);
    break; 
}
print json_encode($data, JSON_UNESCAPED_UNICODE);

 $conexion=null;
//$conexion=null;
