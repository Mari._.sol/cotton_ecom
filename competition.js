var opcion,IDReq,editaCom;
$(document).ready(function() {

    //funcion para sumar

    
jQuery.fn.dataTable.Api.register( 'sum()', function ( ) {
    return this.flatten().reduce( function ( a, b ) {
      if ( typeof a === 'string' ) {
        a = a.replace(/[^\d.-]/g, '') * 1;
      }
      if ( typeof b === 'string' ) {
        b = b.replace(/[^\d.-]/g, '') * 1;
      }
      return a + b;
    }, 0);
});

$('#table_competition tfoot th').each( function () {
    var title = $(this).text();
    $(this).html( '<input type="text" placeholder="'+title+'" />' );
} );

Usuario = $("#usuario").val()
Priv = $("#priv").val();
editaCom=$("#editar").val();
console.log(editaCom);
if  (editaCom == 1){
editar_tabla="<div class='text-center'><div class='btn-group'><button class='btn btn-primary  btn-sm btneditar' title='Edit' ><i class='material-icons'>edit</i></button></div></div>";
nuevo_registro = "<div class='text-center'><div class='btn-group'><button class='btn btn-success btn-sm btnNuevoRegistro' title='Add New'><i class='material-icons'>add_circle</i></button></div></div>";
}

else{
  editar_tabla = '';
  nuevo_registro='';
}
table_competition = $('#table_competition').removeAttr('width').DataTable({


       /* drawCallback: function () {
            var api = this.api();
    
            var bales = api.column( 2, {"filter":"applied"}).data().sum();
            var total = api.rows({"filter":"applied"}).count();
           // var pesototal = api.column( 11, {"filter":"applied"}).data().sum();
            $('#bales').html(bales);
            $('#totaLiqID').html(total);
           // $("#totallotes").val(numlotes);
         //   $('#totalpeso').html(pesototal);
    
          },

          */
     
      
        responsive: "true",
        dom: 'Brtilp',       
        buttons:[ 
			{
			    extend:    'excelHtml5',
			    text:      '<i class="fas fa-file-excel"></i> ',
			    titleAttr: 'Export to Excel',
			    className: 'btn btn-success',
                download: 'open',
			    exportOptions: {
			        columns: ":not(.no-exportar)"
			    }
			},
      {
        text: '<i class="bi bi-funnel-fill"></i>',
        titleAttr: 'Filter by',
        orientation: 'landscape',
        className: 'btn btn-info btn-filtrar',
        attr:{
                id:'filtraboton',
                "data-toggle": 'modal tooltip',
                "disabled": false
        }
      },
        ],

        initComplete: function () {
            // Apply the search
            this.api().columns().every( function () {
                var that = this;
 
                $( 'input', this.footer() ).on( 'keyup change clear', function () {
                    if ( that.search() !== this.value ) {
                        that
                        .search( this.value, true, false ).draw();
                            
                    }
                } );
            } );
        },
           
             
        "order": [ 0, 'desc' ],
       "scrollX": true,
        "scrollY": "50vh",
        "autoWidth": true,
        "scrollCollapse": true,
        "lengthMenu": [100,200,300,500],
        fixedColumns:   {
            left: 0,
            right: 1
        },

        columnDefs: [
            { width: 100, targets: 1 },
            { width: 100, targets: 4 },
            { width: 100, targets: 5},
            { width: 100, targets: 6 },
            { width: 150, targets: 20},
            { width: 150, targets: 21},
            { width: 150, targets: 22},
            { width: 150, targets: 25}
        ],    
        "ajax":{            
          "url": "bd/CrudCompetition.php",
          "method": 'POST', //usamos el metodo POST
          "data":{opcion:9,Usuario:Usuario,Priv:Priv}, //enviamos opcion 4 para que haga un SELECT
          "dataSrc":""
      },
        "columns":[        
            {"data": "IDInquirie"},
            {"data": "Date"},    
            {"data": "RegionOrigin",width: 150}, 
            {"data": "Origin",width: 100}, 
            {"data": "Origin2",width: 150},
            {"data": "RegionDest"},
            {"data": "Destination"},
            {"data": "Shipper",width: 100},           
            {"data": "Quantity"},
            {"data": "CropFrom"},
            {"data": "CropTo"},           
            {"data": "ColMax"},
            {"data": "LenMin"},
            {"data": "MicMin"},
            {"data": "MicMax"},
            {"data": "StrMin"},
            {"data": "QualityComment"},
            {"data": "StartShip"},
            {"data": "EndShip"},
            {"data": "ShipComment"},
            {"data": "Basis"},          
            {"data": "CoverMonth"},
            {"data": "PaymentTerms"},
            {"data": "OfferComments"},    
            {"data": "Comments"},            
            {"data": "Price"},   
            {"data": "Unit"},
            {"data": "NameUser"},  
            {
              "defaultContent": '<div style="display: inline-block;">' + editar_tabla + '</div><div style="display: inline-block;">' + nuevo_registro + '</div>',
              "width": "100px"
            }
        ],
    });    
    
    var oTable = $('#table_competition').DataTable();


    //mostrar modal new 
    $("#btnNuevo").click(function(){
      $('#btnLastRecord').show();
      var boton = document.getElementById("btnLastRecord");
      boton.addEventListener("click", validarAutocompletar);
      document.getElementById('btnGuardar').style.display = 'block';
      document.getElementById('alerta_fecha').style.display = 'none';
      document.getElementById('alerta_calidad').style.display = 'none';
      document.getElementById('alerta_cantidad').style.display = 'none';
      opcion = 8;
        CoverMonth="";        
        Origin2="";
        var date=new Date();  
        var day=String(date.getDate()).padStart(2, '0');          
        var month=("0" + (date.getMonth() + 1)).slice(-2); 
        var year=date.getFullYear();  
        fechahoy=year+"-"+month+"-"+day
        //date = new Date();
       
        $("#form_competition").trigger("reset");
        $(".modal-header").css( "background-color", "#17562c");
        $(".modal-header").css( "color", "white" );
        $(".modal-title").text("New");
        $('#modalCompetition').modal('show');
        $("#Origin").empty();
        $("#Destination").empty();
        years_modal();
        years_modal_to();
        origin2();
        zones();
        zones_origen()  
        covmot();
        color(Origin2);
        $("select#Unit").val("USD Cts / lb");

        $("#Mic_Cat").change(function () {
          var colmax = $(this).val();
          miccat_select(colmax)
        }); 
        $("#zone1").change(function () {
          var pais = $(this).val();
          destination(pais);
        });
        $("#zone2").change(function () {
          var pais2 = $(this).val();
          origin(pais2);
          origin2();
        });         

        $("#Destination").change(function () {
        });

        $("#Mic_Min").keyup(function(evt) {
            $("select#Mic_Cat").val("");
            var MicMin = $(this).val();      
            var MicMax =$("#Mic_Max").val();
            //validar que micmax sea mayor que micmin
            if(MicMax !=""){
              validamic(MicMin,MicMax)
            }

            //validar que solo sean numeros enteros y decimales
            var self = $(this);
            self.val(self.val().replace(/[^0-9\.]/g, ''));
            if ((evt.which != 46 || self.val().indexOf('.') != -1) && (evt.which < 48 || evt.which > 57)) 
            {
              evt.preventDefault();
            }
            //validamos solo 1 digito despues del punto decimal
            if($(this).val().indexOf('.')!=-1){         
              if($(this).val().split(".")[1].length > 1){                
                  if( isNaN( parseFloat( this.value ) ) ) return;
                  this.value = parseFloat(this.value).toFixed(1);
              }  
            }    
        });



        
        $("#Mic_Max").keyup(function(evt) {
          $("select#Mic_Cat").val("");
          var MicMax = $(this).val();
          var MicMin =$("#Mic_Min").val();
          if(MicMin !=""){
            validamic(MicMin,MicMax)
          }

           //validar que solo sean numeros enteros y decimales
           var self = $(this);
           self.val(self.val().replace(/[^0-9\.]/g, ''));
           if ((evt.which != 46 || self.val().indexOf('.') != -1) && (evt.which < 48 || evt.which > 57)) 
           {
             evt.preventDefault();
           }
           //validamos solo 1 digito despues del punto decimal
           if($(this).val().indexOf('.')!=-1){         
             if($(this).val().split(".")[1].length > 1){                
                 if( isNaN( parseFloat( this.value ) ) ) return;
                 this.value = parseFloat(this.value).toFixed(1);
             }  
           }    
          
        });


        $("#Len_Min").keyup(function(evt) {

          //validar que solo sean numeros enteros y decimales
          var self = $(this);
          self.val(self.val().replace(/[^0-9\.]/g, ''));
          if ((evt.which != 46 || self.val().indexOf('.') != -1) && (evt.which < 48 || evt.which > 57)) 
          {
            evt.preventDefault();
          }
          //validamos solo 1 digito despues del punto decimal
          if($(this).val().indexOf('.')!=-1){         
            if($(this).val().split(".")[1].length > 1){                
                if( isNaN( parseFloat( this.value ) ) ) return;
                this.value = parseFloat(this.value).toFixed(1);
            }  
          }    

        });

        $("#Str_Min").keyup(function(evt) {

          //validar que solo sean numeros enteros y decimales
          var self = $(this);
          self.val(self.val().replace(/[^0-9\.]/g, ''));
          if ((evt.which != 46 || self.val().indexOf('.') != -1) && (evt.which < 48 || evt.which > 57)) 
          {
            evt.preventDefault();
          }
          //validamos solo 1 digito despues del punto decimal
          if($(this).val().indexOf('.')!=-1){         
            if($(this).val().split(".")[1].length > 1){                
                if( isNaN( parseFloat( this.value ) ) ) return;
                this.value = parseFloat(this.value).toFixed(1);
            }  
          }    

        });

        
      $("#Origin2").change(function () {
        var Origin2 = $(this).val();
       
        color(Origin2)
        
      }); 

      
      $("#Origin").change(function () {
        //var Origin2 = $(this).val();   
       // color()
        
      }); 

      $("#Date").val(fechahoy);


      $("#Start_Shipment").change(function () {
        var dat1 = $(this).val();
        var dat2 = $("#End_Shipment").val();
        if(dat2 !=""){
          valdate(dat1,dat2);

          
        }

        End_Shipment.min=new Date($(this).val()).toISOString().split("T")[0];

        //$("#End_Shipment").val($(this).val());

      }); 

      $("#End_Shipment").change(function () {
        var dat2 = $(this).val();
        var dat1 = $("#Start_Shipment").val();
        if(dat1 !=""){
          valdate(dat1,dat2);
        }

      }); 

     


      $("#Quantity").keyup(function(evt) {

        //validar que solo sean numeros enteros y decimales
        validaCantidad($(this).val());
        var self = $(this);
        self.val(self.val().replace(/[^0-9\.]/g, ''));
        if ((evt.which != 46 || self.val().indexOf('.') != -1) && (evt.which < 48 || evt.which > 57)) 
        {
          evt.preventDefault();
        }
        //validamos solo 2 digito despues del punto decimal
        if($(this).val().indexOf('.')!=-1){         
          if($(this).val().split(".")[1].length > 2){                
              if( isNaN( parseFloat( this.value ) ) ) return;
              this.value = parseFloat(this.value).toFixed(2);
          }  
        }  
        
      

      });

      $("#Basis").keyup(function(evt) {

        //validar que solo sean numeros enteros y decimales
        
        var self = $(this);
        self.val(self.val().replace(/[^0-9\.-]/g, ''));
        if ((evt.which != 46 || self.val().indexOf('.') != -1) && (evt.which < 48 || evt.which > 57 || evt.which == 45)) 
        {
          evt.preventDefault();
        }
        //validamos solo 2 digito despues del punto decimal
        if($(this).val().indexOf('.')!=-1){         
          if($(this).val().split(".")[1].length > 2){                
              if( isNaN( parseFloat( this.value ) ) ) return;
              this.value = parseFloat(this.value).toFixed(2);
          }  
        }  
      });

      
      $("#Price").keyup(function(evt) {

        //validar que solo sean numeros enteros y decimales
        
        var self = $(this);
        self.val(self.val().replace(/[^0-9\.]/g, ''));
        if ((evt.which != 46 || self.val().indexOf('.') != -1) && (evt.which < 48 || evt.which > 57)) 
        {
          evt.preventDefault();
        }
        //validamos solo 2 digito despues del punto decimal
        if($(this).val().indexOf('.')!=-1){         
          if($(this).val().split(".")[1].length > 2){                
              if( isNaN( parseFloat( this.value ) ) ) return;
              this.value = parseFloat(this.value).toFixed(2);
          }  
        }  
      });
    });

    

    
    $(document).on("click", ".btneditar", function(){
      $('#btnLastRecord').hide();
      document.getElementById('alerta_fecha').style.display = 'none';
      document.getElementById('alerta_calidad').style.display = 'none';
      document.getElementById('alerta_cantidad').style.display = 'none';
      fila = $(this).closest("tr");	     
      opcion = 11;   
      //console.log(opcion);
      IDReq = parseInt(fila.find('td:eq(0)').text());
      Datereq = fila.find('td:eq(1)').text(); 
      RegionOrigin = fila.find('td:eq(2)').text();    
      Origin = fila.find('td:eq(3)').text();
      Origin2 = fila.find('td:eq(4)').text();
      RegDestination =fila.find('td:eq(5)').text();
      Destination =fila.find('td:eq(6)').text();
      Shipper = fila.find('td:eq(7)').text();
      Quantity = fila.find('td:eq(8)').text();
      CropFrom = fila.find('td:eq(9)').text();
      CropTo = fila.find('td:eq(10)').text();
      ColMax = fila.find('td:eq(11)').text();
      LenMin = fila.find('td:eq(12)').text();
      MicMin = fila.find('td:eq(13)').text();
      MicMax = fila.find('td:eq(14)').text();
      StrMin = fila.find('td:eq(15)').text();
      QualityComment = fila.find('td:eq(16)').text();
      StartShip = fila.find('td:eq(17)').text();
      EndShip = fila.find('td:eq(18)').text();
      ShipComment = fila.find('td:eq(19)').text();
      Basis = fila.find('td:eq(20)').text();     
      CoverMonth = fila.find('td:eq(21)').text();
      PaymentTerms = fila.find('td:eq(22)').text();
      OfferComments = fila.find('td:eq(23)').text();
      Comments = fila.find('td:eq(24)').text();     
      Price = fila.find('td:eq(25)').text();
      Unit = fila.find('td:eq(26)').text();
      nombre = fila.find('td:eq(27)').text();
      $("#form_competition").trigger("reset");
      $(".modal-header").css( "background-color", "#17562c");
      $(".modal-header").css( "color", "white" );
      $(".modal-title").text("Edit ID " + IDReq);
      $('#modalCompetition').modal('show');    
      $("#Date").val(Datereq);     
      $("#Quantity").val(Quantity);    
      $("#Shipper").val(Shipper);  
      $("#Col_Max").val(ColMax);
      $("#Len_Min").val(LenMin);
      $("#Mic_Min").val(MicMin);
      $("#Mic_Max").val(MicMax);
      $("#Str_Min").val(StrMin);
      $("#Quality_Comment").val(QualityComment);
      $("#Start_Shipment").val(StartShip);
      $("#End_Shipment").val(EndShip);
      $("#Shipment_Comment").val(ShipComment);
      $("#Basis").val(Basis);
      $("#Payment_Terms").val(PaymentTerms);
      $("#Offer_Comments").val(OfferComments);
      $("#Comments").val(Comments);
      $("#Price").val(Price);
      $("#Destination").empty();
      $("#Origin").empty();

      //edit_countries(Destination,Origin); 
      years_modal_to();
     // destination();
      color(Origin2)
      origin2();        
      years_modalEdit(CropFrom);
      zones(RegDestination);
      zones_origen(RegionOrigin)
      Editcovmot(CoverMonth);
      origin(RegionOrigin,Origin);
      destination(RegDestination,Destination);
      nom = $("#nombre").val();
      regiones = $("#region").val();
      arrayReg = regiones.split(',');
      privOrigin = arrayReg.includes(Origin);
      privDest = arrayReg.includes(Destination);
      if((regiones==""|| privOrigin==true ||  privDest==true|| nom==nombre)&&editaCom==1){
       // $('#btnGuardar').prop('disabled', false);
        document.getElementById('btnGuardar').style.display = 'block';
      }
      else{
      //  $('#btnGuardar').prop('disabled', true);
        document.getElementById('btnGuardar').style.display = 'none';
      }
      
      $("#zone1").change(function () {
        var pais = $(this).val();
        destination(pais);
      }); 

      $("#zone2").change(function () {
        var pais2 = $(this).val();
        origin(pais2);
        origin2();
      }); 

      $("#Crop_to").append('<option class="form-control selected">' +CropTo+ '</option>');
      $('#Origin2').append('<option class="form-control selected">' +Origin2+ '</option>');


      $("#Col_Max").val(ColMax);
      $("#Unit").val(Unit);    
      $("#PaymentTerms").val(PaymentTerms);


      $("#Mic_Cat").change(function () {
        var colmax = $(this).val();
        miccat_select(colmax)
      }); 


      $("#Origin2").change(function () {
        var Origin2 = $(this).val();   
        color(Origin2)
        
      }); 

      $("#Origin").change(function () {
        //var Origin2 = $(this).val();   
        //color()
        
      }); 

      $("#Start_Shipment").change(function () {
        var dat1 = $(this).val();
        var dat2 = $("#End_Shipment").val();
        //console.log(dat1);
        
        if(dat2 !=""){
          valdate(dat1,dat2);
        }

       // $("#End_Shipment").val($(this).val());

      }); 

      $("#End_Shipment").change(function () {
        var dat2 = $(this).val();
        var dat1 = $("#Start_Shipment").val();
        if(dat1 !=""){
          valdate(dat1,dat2);
        }

      }); 


      $("#Basis").keyup(function(evt) {

        //validar que solo sean numeros enteros y decimales
        
        var self = $(this);
        self.val(self.val().replace(/[^0-9\.-]/g, ''));
        if ((evt.which != 46 || self.val().indexOf('.') != -1) && (evt.which < 48 || evt.which > 57 || evt.which == 45)) 
        {
          evt.preventDefault();
        }
        //validamos solo 2 digito despues del punto decimal
        if($(this).val().indexOf('.')!=-1){         
          if($(this).val().split(".")[1].length > 2){                
              if( isNaN( parseFloat( this.value ) ) ) return;
              this.value = parseFloat(this.value).toFixed(2);
          }  
        }  
      });

      
      $("#Price").keyup(function(evt) {

        //validar que solo sean numeros enteros y decimales
        
        var self = $(this);
        self.val(self.val().replace(/[^0-9\.]/g, ''));
        if ((evt.which != 46 || self.val().indexOf('.') != -1) && (evt.which < 48 || evt.which > 57)) 
        {
          evt.preventDefault();
        }
        //validamos solo 2 digito despues del punto decimal
        if($(this).val().indexOf('.')!=-1){         
          if($(this).val().split(".")[1].length > 2){                
              if( isNaN( parseFloat( this.value ) ) ) return;
              this.value = parseFloat(this.value).toFixed(2);
          }  
        }  
      });
    }); 


    $('#form_competition').submit(function(e){
      e.preventDefault();  
      Dateinq=$("#Date").val();
      Destination=$("#Destination").val();
      Origin=$("#Origin").val();
      Origin2=$("#Origin2").val();      
      Quantity=$("#Quantity").val();
      CropFrom=$("#Crop_from").val();
      CropTo=$("#Crop_to").val();
      ColMax=$("#Col_Max").val();
      LenMin=$("#Len_Min").val();
      MicMin=$("#Mic_Min").val();
      MicMax=$("#Mic_Max").val();
      StrMin=$("#Str_Min").val();
      QualityComment=$("#Quality_Comment").val();
      StartShip=$("#Start_Shipment").val();
      EndShip=$("#End_Shipment").val();
      ShipComment=$("#Shipment_Comment").val();
      Basis=$("#Basis").val();
      Unit=$("#Unit").val();
      CoverMonth=$("#Cover_Month").val();
      PaymentTerms=$("#Payment_Terms").val();
      OfferComments=$("#Offer_Comments").val();   
      Comments=$("#Comments").val();
      Usuario=$("#usuario").val();
      zoneorigen=$("#zone2").val();
      zonedest=$("#zone1").val();
      Shipper=$("#Shipper").val();  
      Price = $("#Price").val(); 

      if(opcion == 8){
            $.ajax({
              url: "bd/CrudCompetition.php",
              type: "POST",
              datatype:"json",
              data:  {opcion:opcion,
                      Date:Dateinq,
                      Destination:Destination,
                      Origin:Origin,
                      Origin2:Origin2,                                         
                      Quantity:Quantity,
                      Shipper:Shipper,
                      CropFrom:CropFrom,
                      CropTo:CropTo,
                      ColMax:ColMax,
                      LenMin:LenMin,
                      MicMin:MicMin,
                      MicMax:MicMax,
                      StrMin:StrMin,
                      QualityComment:QualityComment,
                      StartShip:StartShip,
                      EndShip:EndShip,
                      ShipComment:ShipComment,
                      Basis:Basis,
                      Unit:Unit,
                      CoverMonth:CoverMonth,
                      PaymentTerms:PaymentTerms,
                      OfferComments:OfferComments,  
                      Comments:Comments,
                      Usuario:Usuario,
                      zoneorigen:zoneorigen,
                      zonedest:zonedest,
                      Price:Price
                      
                    },    
              success: function(data){         
                alert("Successful registration");
                table_competition.ajax.reload(null,false);

              },
              error: function(data) {
                  alert('error');
              }
          });
      }
      else if(opcion == 11){
        $.ajax({
          url: "bd/CrudCompetition.php",
          type: "POST",
          datatype:"json",
          data:  {opcion:opcion,
                  IDReq:IDReq,
                  Date:Dateinq,
                  Destination:Destination,
                  Origin:Origin,
                  Origin2:Origin2,                             
                  Quantity:Quantity,
                  Shipper:Shipper,
                  CropFrom:CropFrom,
                  CropTo:CropTo,
                  ColMax:ColMax,
                  LenMin:LenMin,
                  MicMin:MicMin,
                  MicMax:MicMax,
                  StrMin:StrMin,
                  QualityComment:QualityComment,
                  StartShip:StartShip,
                  EndShip:EndShip,
                  ShipComment:ShipComment,
                  Basis:Basis,
                  Unit:Unit,
                  CoverMonth:CoverMonth,
                  PaymentTerms:PaymentTerms,
                  OfferComments:OfferComments,      
                  Comments:Comments,
                  Usuario:Usuario,
                  zoneorigen:zoneorigen,
                  zonedest:zonedest,
                  Price:Price
                },    
          success: function(data){         
            alert("Update Successes");
            table_competition.ajax.reload(null,false);

          },
          error: function(data) {
              alert('error');
          }
        });
      }
      else if(opcion == 16){
        var prueba = compararValores()
        //console.log(prueba)
        if (prueba==true) {
          e.stopPropagation();
          document.getElementById('btnContinuar').onclick = function(e) {
            AgregarNuevoRegistro();
            $('#confirmacionModal').modal('hide');
          };
          document.getElementById('btnCancelar').onclick = function() {
            $('#confirmacionModal').modal('hide');
          };
        }else{
          AgregarNuevoRegistro();
        }
      }
      $('#modalCompetition').modal('hide');
    }); 
  });


$(document).ready(function() {
 $("#btnModalLoad").click(function(){
      $("#formcomp").trigger("reset");      
      $(".modal-header").css( "background-color", "#17562c");
      $(".modal-header").css( "color", "white" );
      $(".modal-title").text("Load CSV Competition");
      $('#ModalCsvComp').modal('show');
      $('#btnGuardarFile').prop('disabled', true);


      //Hacer la validacion del archivo seleccionado 
      $("#csvcomp").change(function(){           
        valida_csv_competition();
      });


      

  }); 
  
  $('#btnGuardarFile').on('click', function(e) {
        e.preventDefault();    
        var file_data = $('#csvcomp').prop('files')[0];  
        var form_data = new FormData();             
        form_data.append('file', file_data);   
        var $el = $('#csvcomp');
        $el.wrap('<form>').closest('form').get(0).reset();
        $el.unwrap();  
        $('#btnGuardarFile').prop('disabled', true);     
        $.ajax({
            url: 'bd/loadcompetition.php', // <-- point to server-side PHP script 
            dataType: 'text',  // <-- what to expect back from the PHP script, if anything
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,                         
            type: 'post',
            success: function(data){
               // datos = JSON.parse(data);
               // cont = datos['cont'];
                //cont = data['cont'];

              if (!isNaN(data)) {
                alert("Successful file upload" + '\n' +"Records Uploaded: "+ data);
              
              }
                 
                table_competition.ajax.reload(null,false);     
            },
            error(){
                alert("Error to load file");
                var $el = $('#csvcomp');
                $el.wrap('<form>').closest('form').get(0).reset();
                $el.unwrap();  
                $('#btnGuardarFile').prop('disabled', true);    
            }

        });
    });
  
}); 



//***********************************FILTROS PARA TABLA DE COMPETITION********************************************* 
var fechainicio = "",fechafin="",rango=0;
$(document).ready(function() {
 
    $("#filtraboton").click(function(){    
        
        $(".modal-header").css( "background-color", "#17562c");
        $(".modal-header").css( "color", "white" );
        $('#filtrarmodal').modal('show');
        //$('#OriginFiltros').empty();
        //$("#Destinofiltro").empty(); 
        if ($('#RegionDest option').length === 0) {
        regiondestfiltro();
        }
        if ($('#RegionOrigen option').length === 0) {
        regionesorigen();
        }



        ///validar si hay un cambie en la fecha para capturar los valores
        if(fechainicio =="" && fechafin =="" && rango==0){
        fechadefault();
        }

        else{
          $("#timeSelect").val(rango);
          $("#fromdate").val(fechainicio);
          $("#todate").val(fechafin);
          
        }

      
        $("#RegionDest").change(function () {        
          destinofiltro($(this).val());
        });
        $("#RegionOrigen").change(function () {        
          paisoriginfiltro($(this).val());
        });
        $("#timeSelect").change(function () {        
          selectorfecha();
        });


        if ($('#userlist option').length === 0) {
        userflitro();
        }

        $("#timeSelect").change(function () {        
          rango=$(this).val();
          fechainicio = $("#fromdate").val();
          fechafin= $("#todate").val();
        });
        $("#fromdate").change(function () {        
          fechainicio= $(this).val();
          fechafin= $("#todate").val();
        });
        $("#todate").change(function () { 
          fechainicio = $("#fromdate").val();       
          fechafin= $(this).val();
        });
        
    });

    var botonglobal = 0;

    $("#buscafiltro, #borrarFiltro, #cer").click(function(){
      boton = $(this).val();
      if(boton == 1 || boton == 0)
          botonglobal = boton;
      if(boton != 3){
          FromDate = $("#fromdate").val();  
          ToDate = $("#todate").val();  
          OriginReg = $("#RegionOrigen").val();  
          OriginCount= $("#OriginFiltros").val();
          DestReg= $("#RegionDest").val();
          DestCount= $("#Destinofiltro").val();
          ShipperFilter= $("#Shipperfiltro").val();
          UserComp= $("#userlist").val();

          var applyFilter =  $.ajax({
            type: 'POST',
            url: 'bd/FiltrosComp.php',
            data: {
                boton:boton,
                FromDate: FromDate,
                ToDate:ToDate,
                OriginReg: OriginReg,
                OriginCount: OriginCount,
                DestReg: DestReg,
                DestCount: DestCount,
                ShipperFilter: ShipperFilter,
                UserComp: UserComp
            }
            
          })
          
          applyFilter.done(function(data){
            table_competition.ajax.reload(function(){
              if(boton == 1){
                $('#aviso').html('<div id="alert1" class="alert alert-success" style="width: 16rem; height: 3rem;" role="alert"><i class="bi bi-check-circle-fill"></i>Filters Applied</div>')
                setTimeout(function() {
                    $('#alert1').fadeOut(1000);
                    $('#filtrarmodal').modal('hide');
                },1000);
              }
              else{
                // resetear filtros 
                $("#filtros").trigger("reset");
                $('#OriginFiltros').empty();
                $("#Destinofiltro").empty(); 
                regiondestfiltro();
                regionesorigen();
                fechadefault();
                userflitro();
                rango=0;
                fechainicio = "";
                fechafin= "";
                $('#aviso').html('<div id="alert1" class="alert alert-success" style="width: 16rem; height: 3rem;" role="alert"> <i class="bi bi-trash-fill"></i>Clean Filters</div>')
                setTimeout(function() {
                    $('#alert1').fadeOut(1500);
                },1500);
              }
    
            });
            $('#aviso').html('<div class="spinner-border" style="width: 2rem; height: 2rem;" role="status"><span class="visually-hidden">Loading...</span> </div> <div class="spinner-grow" style="width: 2rem; height: 2rem;" role="status"><span class="visually-hidden">Loading...</span> </div>');  
          })
          applyFilter.error(function(data){
            $('#aviso').html("");
          })



      }

      else{
        $("#filtros").trigger("reset");
        rango=0;
        fechainicio = "";
        fechafin= "";
      }

    });
}); 

function years_modal(){
  $("#Crop_from").empty();
  date = new Date();
  year = date.getFullYear();
  diferencia = (year+7) - 2021
  
  $('#Crop_from').append($('<option>').val("").text("Choose..."));  
  for (var i = 0; i<= diferencia; i++){ 
      $('#Crop_from').append($('<option>').val(2021+i).text(2021+i));     
  }
}

function years_modalEdit(CropFrom){
  $("#Crop_from").empty();
  date = new Date();
  year = date.getFullYear();
  diferencia = (year+7) - 2021
  if(CropFrom!=""){
    $('#Crop_from').append($('<option>').val(CropFrom).text(CropFrom)); 
  }
  else{
    $('#Crop_from').append($('<option>').val("").text("Choose..."));  
  }
  for (var i = 0; i<= diferencia; i++){ 
    $('#Crop_from').append($('<option>').val(2021+i).text(2021+i));     
  }
}

function years_modal_CropTo(CropFrom){
  $("#Crop_to").empty();
  date = new Date();
  year = date.getFullYear();
  diferencia = (year+7) - 2021
  if(CropFrom!=""){
    $('#Crop_to').append($('<option>').val(CropFrom).text(CropFrom)); 
  }
  else{
    $('#Crop_to').append($('<option>').val("").text("Choose..."));  
  }
  for (var i = 0; i<= diferencia; i++){ 
      $('#Crop_to').append($('<option>').val(2021+i).text(2021+i));     
  }
}


function years_modal_to(paisorigen){   
  $('#Crop_to').prop('disabled', true);
  $("#Crop_to").empty();
  date = new Date();
  year = date.getFullYear();
  $("#Crop_from").change(function () { 
    year_from = parseInt($("#Crop_from").val());
    paisorigin = $("#Origin").val();
    //console.log(paisorigin);

    if(paisorigin =="AU" || paisorigin =="BR"){
      $("#Crop_to").empty();
      $('#Crop_to').prop('disabled', false);
      for (var i = year_from; i<= year+8; i++){ 
        $('#Crop_to').append($('<option>').val(i).text(i)); 
      }
      // $('#Crop_to').append($('<option>').val(year_from).text(year_from)); 
      $("select#Crop_to").val(year_from);
    } 
    else{     
      if ( year_from !=""){
        //console.log(year_from);
        year_from = year_from
        //console.log(year_from);
        $("#Crop_to").empty();
        $('#Crop_to').prop('disabled', false);
        //$('#Crop_to').append($('<option>').val("").text("Choose..."));
        for (var i = year_from; i<= year+8; i++){ 
          $('#Crop_to').append($('<option>').val(i).text(i)); 
        }

        $("select#Crop_to").val(year_from+1);
      }
      else{
        $("#Crop_to").empty();
        $('#Crop_to').prop('disabled', true);
      }
    }
  }); 
}


function destination(pais,paisselect){
    idDest="";
    ///$('#Origin').empty();
    $.ajax({
      url: "bd/CrudCompetition.php",
      type: "POST",
      datatype:"json",
      data:  {opcion:3,pais:pais},    
      success: function(data){        
           $("#Destination").empty(); 
          $('#Destination').append($('<option>').val("").text("Choose..."));
          opts = JSON.parse(data);
          for (var i = 0; i< opts.length; i++){ 
              $('#Destination').append($('<option>').val(opts[i].CountryCode).text(opts[i].CountryName)); 
              if (paisselect!="" && paisselect==opts[i].CountryName){
                idDest = opts[i].CountryCode
              }                
            }
            $("select#Destination").val(idDest);
          },          
          error: function(data) {
              alert('error');
          }
      });
  if(pais!=""){
    $("select#Destination").val(pais);
  }
}

function destinationNew(pais,paisselect){
  idDest="";
  ///$('#Origin').empty();
  $.ajax({
    url: "bd/CrudCompetition.php",
    type: "POST",
    datatype:"json",
    data:  {opcion:3,pais:pais},    
    success: function(data){        
        $("#Destination").empty(); 
        $('#Destination').append($('<option>').val("").text("Choose..."));
        opts = JSON.parse(data);
        for (var i = 0; i< opts.length; i++){ 
            $('#Destination').append($('<option>').val(opts[i].CountryCode).text(opts[i].CountryName)); 
            if (paisselect!="" && paisselect==opts[i].CountryName){
              idDest = opts[i].CountryCode
            }                
          }
          $("select#Destination").val(idDest);
        },          
        error: function(data) {
            alert('error');
        },async: false
      });
  /*   if(pais!=""){
    $("select#Destination").val(pais);
  } */
}


  function origin(pais,origindelect){    
    idOrigin="";
    $('#Origin').prop('disabled', false);
      $.ajax({
        url: "bd/CrudCompetition.php",
        type: "POST",
        datatype:"json",
        data:  {opcion:4,pais:pais },    
        success: function(data){    
            opts = JSON.parse(data);
              $('#Origin').empty();
              $('#Origin').append($('<option>').val("").text("Choose..."));
              for (var i = 0; i< opts.length; i++){ 
                  $('#Origin').append($('<option>').val(opts[i].CountryCode).text(opts[i].CountryName));  
                  if (origindelect!="" && origindelect==opts[i].CountryName){
                    idOrigin = opts[i].CountryCode
                  }               
              } 

              $("select#Origin").val(idOrigin);
        },
        error: function(data) {
            alert('error');
        },async: false
    });
  }

  function origin2(valor){
    idgin2="";
    $('#Origin2').prop('disabled', true);
    $("#Origin2").empty();
    $("#Origin").change(function (){ 
      origen = $("#Origin").val();
      if(origen !=""){  
        $("#Origin2").empty();     
          $.ajax({
            url: "bd/CrudInquiries.php",
            type: "POST",
            datatype:"json",
            data:  {opcion:5,origen:origen },    
            success: function(data){   
                opts = JSON.parse(data);
                if(opts.length > 0){
                  $("#Origin2").empty();   
                  $('#Origin2').prop('disabled', false);
                  $('#Origin2').append($('<option>').val("").text("Choose..."));
                  for (var i = 0; i< opts.length; i++){ 
                      $('#Origin2').append($('<option>').val(opts[i].Zone).text(opts[i].Zone));  
                     /* if(valor !="" && valor == opts[i].Zone){
                        idgin2 = opts[i].IDZone;
                      }           */
                  }
                }
                else{
                  $("#Origin2").empty();
                  $('#Origin2').prop('disabled', true);
                }   
                
                $("select#Origin2").val(valor);
            },
            error: function(data) {
                alert('error');
            }
          });
      }
      else{
        $('#Origin2').prop('disabled', true);
        $("#Origin2").empty();   
      }
    });
  }
  
function edit_countries(Destination,Origin){
  
    $.ajax({
      url: "bd/CrudCompetition.php",
      type: "POST",
      datatype:"json",
      data:  {opcion:10,Destination:Destination,Origin:Origin},    
      success: function(data){
        opts = JSON.parse(data);
        if(opts.length > 0){
        $("#Destination").prepend("<option value="+'"'+opts[0].IDdestino+'"'+"selected'>"+Destination+"</option>");
        $("#Origin").prepend("<option value="+'"'+opts[0].IDOrigen+'"'+" selected='selected'>"+Origin+"</option>");              
      }


      },
      error: function(data) {
          alert('error');
      }
    });

  }


  function miccat_select(colmax){     
      //console.log(colmax);
      switch (colmax) {
        case 'G7':
          $("#Mic_Min").val(5.3)
          $("#Mic_Max").val(10)        
          
        break;
        case 'G6':
          $("#Mic_Min").val(5)
          $("#Mic_Max").val(5.2)        
          
        break;
        case 'G5':
          $("#Mic_Min").val(3.5)
          $("#Mic_Max").val(4.9)        
          
        break;
        case 'G4':
          $("#Mic_Min").val(3.3)
          $("#Mic_Max").val(3.4)        
          
        break;
        case 'G3':
          $("#Mic_Min").val(3)
          $("#Mic_Max").val(3.2)        
          
        break;
        case 'G2':
          $("#Mic_Min").val(2.7)
          $("#Mic_Max").val(2.9)        
          
        break;
        case 'G1':
          $("#Mic_Min").val(2.5)
          $("#Mic_Max").val(2.6)        
          
        break;
        case 'G0':
          $("#Mic_Min").val(0)
          $("#Mic_Max").val(2.4)        
          
        break;
        }
      
    

  }


  function zones(valor){
    $("#zone1").empty();
    var group1 = $('<optgroup class="optgroups" label="Africa">');
    var group2 = $('<optgroup class="optgroups" label="America">');
    var group3 = $('<optgroup class="optgroups" label="Asia">');
    var group4 = $('<optgroup class="optgroups" label="Europe">');
    var group5 = $('<optgroup class="optgroups" label="Oceania">');

 

    $.ajax({
      url: "bd/CrudCompetition.php",
      type: "POST",
      datatype:"json",
      data:  {opcion:12},    
      success: function(data){
        opts = JSON.parse(data);
        $('#zone1').append($('<option>').val("").text("Choose..."));
      //  $('#zone2').append($('<option>').val("").text("Choose..."));
       // $('#Destination').append($('<option>').val("").text("Choose..."));    
        
        for (var i = 0; i< opts.length; i++){ 

            switch (opts[i].Region) {
              case 'Africa':
                $("#zone1").append(group1);
                group1.append($('<option></option>').val(opts[i].SubRegion).html(opts[i].SubRegion));
                break;
              case 'America':
                $("#zone1").append(group2);
                group2.append($('<option></option>').val(opts[i].SubRegion).html(opts[i].SubRegion));
                break;  
              case 'Asia':
                $("#zone1").append(group3);
                group3.append($('<option></option>').val(opts[i].SubRegion).html(opts[i].SubRegion));
                break;  
              case 'Europe':
                $("#zone1").append(group4);
                group4.append($('<option></option>').val(opts[i].SubRegion).html(opts[i].SubRegion));
                break;   
              case 'Oceania':
                $("#zone1").append(group5);
                group5.append($('<option></option>').val(opts[i].SubRegion).html(opts[i].SubRegion));
                break; 
            }   
          
        }     
        
        if(valor!=""){
          $("select#zone1").val(valor);
        }
     
      },
      error: function(data) {
          alert('error');
      }
    });




  }


  function zones_origen(valor){
    $("#zone2").empty();

    var group1 = $('<optgroup class="optgroups" label="Africa">');
    var group2 = $('<optgroup class="optgroups" label="America">');
    var group3 = $('<optgroup class="optgroups" label="Asia">');
    var group4 = $('<optgroup class="optgroups" label="Europe">');
    var group5 = $('<optgroup class="optgroups" label="Oceania">');

    $.ajax({
      url: "bd/CrudCompetition.php",
      type: "POST",
      datatype:"json",
      data:  {opcion:14},    
      success: function(data){
        opts = JSON.parse(data);       
        $('#zone2').append($('<option>').val("").text("Choose..."));
            for (var i = 0; i< opts.length; i++){
                switch (opts[i].Region) {
                  case 'Africa':
                    $("#zone2").append(group1);
                    group1.append($('<option></option>').val(opts[i].SubRegion).html(opts[i].SubRegion));
                    break;
                  case 'America':
                    $("#zone2").append(group2);
                    group2.append($('<option></option>').val(opts[i].SubRegion).html(opts[i].SubRegion));
                    break;  
                  case 'Asia':
                    $("#zone2").append(group3);
                    group3.append($('<option></option>').val(opts[i].SubRegion).html(opts[i].SubRegion));
                    break;  
                  case 'Europe':
                    $("#zone2").append(group4);
                    group4.append($('<option></option>').val(opts[i].SubRegion).html(opts[i].SubRegion));
                    break;   
                  case 'Oceania':
                    $("#zone2").append(group5);
                    group5.append($('<option></option>').val(opts[i].SubRegion).html(opts[i].SubRegion));
                    break; 
                }   
            }     
            
            if(valor!=""){
              $("select#zone2").val(valor);
            }
     
      },
      error: function(data) {
          alert('error');
      }
    });
  }


  function covmot(){
    val="";
    $.ajax({
      url: "bd/CrudCompetition.php",
      type: "POST",
      datatype:"json",
      data:  {opcion:13},    
      success: function(data){  
        $("#Cover_Month").empty();       
        $('#Cover_Month').append($('<option>').val("").text("Choose..."));
        opts = JSON.parse(data);
        for (var i = 0; i< opts.length; i++){
          $('#Cover_Month').append($('<option>').val(opts[i].CovMon).text(opts[i].CovMon)); 
          if (opts[i].DefaultSelect == 1){  
              val= opts[i].CovMon;
            }
          /*if (opts[i].CovMon == valor){
              val=opts[i].IDCoverMoths; 
          }*/
        }
        $("select#Cover_Month").val(val);       
      },
      error: function(data) {
        alert('error');
      }
    });
  }
  
  
  function Editcovmot(valor){
    //val=valor;
    $.ajax({
      url: "bd/CrudCompetition.php",
      type: "POST",
      datatype:"json",
      data:  {opcion:13},    
      success: function(data){  
        $("#Cover_Month").empty();       
        $('#Cover_Month').append($('<option>').val("").text("Choose..."));
        opts = JSON.parse(data);
        for (var i = 0; i< opts.length; i++){
          $('#Cover_Month').append($('<option>').val(opts[i].CovMon).text(opts[i].CovMon)); 
          /* if (opts[i].CovMon == valor){
              val=opts[i].IDCoverMoths; 
          }*/
        }
        $("select#Cover_Month").val(valor);   
      },
      error: function(data) {
          alert('error');
      }
    });
  }


function color(origen2){
 
  //$("#Col_Max").empty();  
  //$('#Col_Max').append($('<option>').val("0").text("Choose..."));
  if (origen2==4 || origen2=="PIMA" ){
    $("#Col_Max").empty();  
    $('#Col_Max').append($('<option>').val("0").text("Choose..."));
    $('#Col_Max').append($('<option>').val(1).text(1)); 
    $('#Col_Max').append($('<option>').val(2).text(2)); 
    $('#Col_Max').append($('<option>').val(3).text(3)); 
    $('#Col_Max').append($('<option>').val(4).text(4)); 
    $('#Col_Max').append($('<option>').val(5).text(5)); 
    $('#Col_Max').append($('<option>').val(6).text(6)); 
    //console.log("entro a grupo")
    //console.log(origen2)
   

}
else{
  $("#Col_Max").empty(); 
  $('#Col_Max').append($('<option>').val("0").text("Choose..."));
  var $group1 = $('<optgroup class="optgroups" label="White">');
  var $group2 = $('<optgroup class="optgroups" label="Light Spotted">');
  var $group3 = $('<optgroup class="optgroups" label="Spotted">');

  $("#Col_Max").append($group1);
  $group1.append($('<option></option>').val(11).html(11));
  $group1.append($('<option></option>').val(21).html(21));
  $group1.append($('<option></option>').val(31).html(31));
  $group1.append($('<option></option>').val(41).html(41));
  $group1.append($('<option></option>').val(51).html(51));
  $group1.append($('<option></option>').val(61).html(61));
  $group1.append($('<option></option>').val(71).html(71));
  $group1.append($('<option></option>').val(81).html(81));
  $("#Col_Max").append($group2);
  $group2.append($('<option></option>').val(12).html(12));
  $group2.append($('<option></option>').val(22).html(22));
  $group2.append($('<option></option>').val(32).html(32));
  $group2.append($('<option></option>').val(42).html(42));
  $group2.append($('<option></option>').val(52).html(52));
  $group2.append($('<option></option>').val(62).html(62));
  $group2.append($('<option></option>').val(82).html(82));  
$("#Col_Max").append($group3);
  $group3.append($('<option></option>').val(13).html(13));
  $group3.append($('<option></option>').val(23).html(23));
  $group3.append($('<option></option>').val(24).html(24));
  $group3.append($('<option></option>').val(33).html(33));
  $group3.append($('<option></option>').val(34).html(34));
  $group3.append($('<option></option>').val(35).html(35));
  $group3.append($('<option></option>').val(43).html(43));
  $group3.append($('<option></option>').val(44).html(44));
  $group3.append($('<option></option>').val(53).html(53));
  $group3.append($('<option></option>').val(54).html(54));
  $group3.append($('<option></option>').val(63).html(63));
  $group3.append($('<option></option>').val(83).html(83));
  $group3.append($('<option></option>').val(84).html(84));
  $group3.append($('<option></option>').val(85).html(85));
}


 
}

function valdate(dat1,dat2){
  var f1 = new Date(dat1);
  var f2 = new Date(dat2);
  if(f1>f2){
    document.getElementById('alerta_fecha').style.display = 'block';
  }
  else{
    document.getElementById('alerta_fecha').style.display = 'none';
  }
}


function validamic(micmin,micmax){
  if(parseFloat(micmin) > parseFloat(micmax)){
    document.getElementById('alerta_calidad').style.display = 'block';
  }
  else{
  document.getElementById('alerta_calidad').style.display = 'none';
  }

}

function validaCantidad(cantidad){

  if (cantidad <=0){
    document.getElementById('alerta_cantidad').style.display = 'block';    
  }
  else{
    document.getElementById('alerta_cantidad').style.display = 'none';
  }

}

function valida_csv_competition(){
    
  var filename = $("#csvcomp").val();
//  console.log(filename);
  //si es null muestra un mensaje de error
  if(filename == null){
      alert('You have not selected a file');
      }
  else{// si se eligio un archivo correcto obtiene la extension para validarla
      var extension = filename.replace(/^.*\./, '');               
      
      //$('#update').prop('disabled', false);
      if (extension == filename)
          extension = '';
      else{
          extension = extension.toLowerCase();
        
          //aqui puedes incluir todas las extensiones que quieres permitir
          if(extension == 'CSV' || extension == 'csv' || extension == 'Csv' ){
              $('#btnGuardarFile').prop('disabled', false);
          }
          else{
              alert("The selected file is not a CSV file");
              var $el = $('#csvcomp');
              $el.wrap('<form>').closest('form').get(0).reset();
              $el.unwrap();  
              $('#btnGuardarFile').prop('disabled', true);    
             // $('#load').prop('disabled', true);
              
          }
      }
  }


}


//funciones para filtros

function regionesdestino(){
  $("#zone1").empty();
  var group1 = $('<optgroup class="optgroups" label="Africa">');
  var group2 = $('<optgroup class="optgroups" label="America">');
  var group3 = $('<optgroup class="optgroups" label="Asia">');
  var group4 = $('<optgroup class="optgroups" label="Europe">');
  var group5 = $('<optgroup class="optgroups" label="Oceania">');



  $.ajax({
    url: "bd/CrudCompetition.php",
    type: "POST",
    datatype:"json",
    data:  {opcion:12},    
    success: function(data){
      opts = JSON.parse(data);
      $('#zone1').append($('<option>').val("").text("Choose..."));
    //  $('#zone2').append($('<option>').val("").text("Choose..."));
     // $('#Destination').append($('<option>').val("").text("Choose..."));    
      
      for (var i = 0; i< opts.length; i++){ 

          switch (opts[i].Region) {
            case 'Africa':
              $("#zone1").append(group1);
              group1.append($('<option></option>').val(opts[i].SubRegion).html(opts[i].SubRegion));
              break;
            case 'America':
              $("#zone1").append(group2);
              group2.append($('<option></option>').val(opts[i].SubRegion).html(opts[i].SubRegion));
              break;  
            case 'Asia':
              $("#zone1").append(group3);
              group3.append($('<option></option>').val(opts[i].SubRegion).html(opts[i].SubRegion));
              break;  
            case 'Europe':
              $("#zone1").append(group4);
              group4.append($('<option></option>').val(opts[i].SubRegion).html(opts[i].SubRegion));
              break;   
            case 'Oceania':
              $("#zone1").append(group5);
              group5.append($('<option></option>').val(opts[i].SubRegion).html(opts[i].SubRegion));
              break; 
          }   
        
      }     
      
 
   
    },
    error: function(data) {
        alert('error');
    }
  });




}


//funciones para el modal de filtro
function regiondestfiltro(){
  $("#RegionDest").empty();
  var group1 = $('<optgroup class="optgroups" label="Africa">');
  var group2 = $('<optgroup class="optgroups" label="America">');
  var group3 = $('<optgroup class="optgroups" label="Asia">');
  var group4 = $('<optgroup class="optgroups" label="Europe">');
  var group5 = $('<optgroup class="optgroups" label="Oceania">');
  $.ajax({
    url: "bd/CrudCompetition.php",
    type: "POST",
    datatype:"json",
    data:  {opcion:12},    
    success: function(data){
      opts = JSON.parse(data);
      $('#RegionDest').append($('<option>').val("").text("Choose..."));
    //  $('#zone2').append($('<option>').val("").text("Choose..."));
     // $('#Destination').append($('<option>').val("").text("Choose..."));    
      
      for (var i = 0; i< opts.length; i++){ 

          switch (opts[i].Region) {
            case 'Africa':
              $("#RegionDest").append(group1);
              group1.append($('<option></option>').val(opts[i].SubRegion).html(opts[i].SubRegion));
              break;
            case 'America':
              $("#RegionDest").append(group2);
              group2.append($('<option></option>').val(opts[i].SubRegion).html(opts[i].SubRegion));
              break;  
            case 'Asia':
              $("#RegionDest").append(group3);
              group3.append($('<option></option>').val(opts[i].SubRegion).html(opts[i].SubRegion));
              break;  
            case 'Europe':
              $("#RegionDest").append(group4);
              group4.append($('<option></option>').val(opts[i].SubRegion).html(opts[i].SubRegion));
              break;   
            case 'Oceania':
              $("#RegionDest").append(group5);
              group5.append($('<option></option>').val(opts[i].SubRegion).html(opts[i].SubRegion));
              break; 
          }   
        
      }     

   
    },
    error: function(data) {
        alert('error');
    }
  });
}


function destinofiltro(pais){
  idDest="";
  ///$('#Origin').empty();
  $.ajax({
    url: "bd/CrudCompetition.php",
    type: "POST",
    datatype:"json",
    data:  {opcion:3,pais:pais},    
    success: function(data){        
         $("#Destinofiltro").empty(); 
        $('#Destinofiltro').append($('<option>').val("").text("Choose..."));
        opts = JSON.parse(data);
        for (var i = 0; i< opts.length; i++){ 
            $('#Destinofiltro').append($('<option>').val(opts[i].CountryCode).text(opts[i].CountryName)); 
                        
          }
        },          
        error: function(data) {
            alert('error');
        }
    });

}

function regionesorigen(){
  $("#RegionOrigen").empty();

  var group1 = $('<optgroup class="optgroups" label="Africa">');
  var group2 = $('<optgroup class="optgroups" label="America">');
  var group3 = $('<optgroup class="optgroups" label="Asia">');
  var group4 = $('<optgroup class="optgroups" label="Europe">');
  var group5 = $('<optgroup class="optgroups" label="Oceania">');

  $.ajax({
    url: "bd/CrudCompetition.php",
    type: "POST",
    datatype:"json",
    data:  {opcion:14},    
    success: function(data){
      opts = JSON.parse(data);       
      $('#RegionOrigen').append($('<option>').val("").text("Choose..."));
          for (var i = 0; i< opts.length; i++){
              switch (opts[i].Region) {
                case 'Africa':
                  $("#RegionOrigen").append(group1);
                  group1.append($('<option></option>').val(opts[i].SubRegion).html(opts[i].SubRegion));
                  break;
                case 'America':
                  $("#RegionOrigen").append(group2);
                  group2.append($('<option></option>').val(opts[i].SubRegion).html(opts[i].SubRegion));
                  break;  
                case 'Asia':
                  $("#RegionOrigen").append(group3);
                  group3.append($('<option></option>').val(opts[i].SubRegion).html(opts[i].SubRegion));
                  break;  
                case 'Europe':
                  $("#RegionOrigen").append(group4);
                  group4.append($('<option></option>').val(opts[i].SubRegion).html(opts[i].SubRegion));
                  break;   
                case 'Oceania':
                  $("#RegionOrigen").append(group5);
                  group5.append($('<option></option>').val(opts[i].SubRegion).html(opts[i].SubRegion));
                  break; 
              }   
          }  
         
    },
    error: function(data) {
        alert('error');
    }
  });
}


function paisoriginfiltro(pais){    
    $.ajax({
      url: "bd/CrudCompetition.php",
      type: "POST",
      datatype:"json",
      data:  {opcion:4,pais:pais },    
      success: function(data){    
          opts = JSON.parse(data);
            $('#OriginFiltros').empty();
            $('#OriginFiltros').append($('<option>').val("").text("Choose..."));
            for (var i = 0; i< opts.length; i++){ 
                $('#OriginFiltros').append($('<option>').val(opts[i].CountryCode).text(opts[i].CountryName));  
                      
            } 
      },
      error: function(data) {
          alert('error');
      }
  });


}

function fechadefault(){
  var date=new Date();  
  date.setDate(date.getDate());
  var day=String(date.getDate()).padStart(2, '0');          
  var month=("0" + (date.getMonth() + 1)).slice(-2); 
  var year=date.getFullYear();  
  fechadef=year+"-"+month+"-"+day 

  var date2=new Date();  
  date2.setDate(date2.getDate() - 30);
  var day2=String(date2.getDate()).padStart(2, '0');          
  var month2=("0" + (date2.getMonth() + 1)).slice(-2); 
  var year2=date2.getFullYear();  
  fechadef2=year2+"-"+month2+"-"+day2 ;

  $('#fromdate').val( fechadef2);
  $('#todate').val(fechadef);
  $('#timeSelect').val(30);
}

function selectorfecha(){
    timeSelect = $('select[name=timeSelect] option').filter(':selected').val();
    if(timeSelect){
    timeSelect = parseInt(timeSelect);
    var today = new Date();

    //today = today.toLocaleTimeString('es-MX');
    let dateEnMilisegundos = 1000 * 60* 60* 24* timeSelect;
    let suma = today.getTime() - dateEnMilisegundos;
    let fechainicial = new Date(suma);
    //console.log(today);
    datetoday =  formatDate(today);
    datetosearch = formatDate(fechainicial);
    $('#fromdate').val(datetosearch);
    $('#todate').val(datetoday);
    }else{
      fechadefault();
    }

}

function formatDate(date) {
  var d = new Date(date),
      month = '' + (d.getMonth() + 1),
      day = '' + d.getDate(),
      year = d.getFullYear();

  if (month.length < 2) 
      month = '0' + month;
  if (day.length < 2) 
      day = '0' + day;

  return [year, month, day].join('-');
}

function userflitro(){
  $('#userlist').empty();
  $.ajax({
    url: "bd/CrudCompetition.php",
    type: "POST",
    datatype:"json",
    data:  {opcion:15},    
    success: function(data){    
        opts = JSON.parse(data);
          $('#userlist').empty();
          $('#userlist').append($('<option>').val("").text("Choose..."));
          for (var i = 0; i< opts.length; i++){ 
              $('#userlist').append($('<option>').val(opts[i].User).text(opts[i].Name));  
                    
          } 
    },
    error: function(data) {
      alert('error');
    }
  });
}


function AutoCompletar(origen, CountryDest) {
  $.ajax({
    url: "bd/CrudCompetition.php",
    type: "POST",
    dataType: "json",
    data: { opcion:17, origen: origen, CountryDest: CountryDest },
      success: function(data) {
        console.log(data)
        if (data[0].IDInq === null) {
          alert('There is no record with this Origin - Destination.');
        } else {
          $('#Origin2').val(data[0].Origin2);
          $('#Shipper').val(data[0].Shipper);
          $('#Quantity').val(data[0].Quantity);
          $('#Crop_from').val(data[0].CropFrom);
          years_modal_CropTo(data[0].CropTo);
          //$('#Crop_to').val(data[0].CropFrom);
          $('#Col_Max').val(data[0].ColMax);
          $('#Len_Min').val(data[0].LenMin);
          $('#Mic_Min').val(data[0].MicMin);
          $('#Mic_Max').val(data[0].MicMax);
          $('#Str_Min').val(data[0].StrMin);
          $('#Quality_Comment').val(data[0].QualityComment);
          $('#Start_Shipment').val(data[0].StartShip);
          $('#End_Shipment').val(data[0].EndShip);
          $('#Shipment_Comment').val(data[0].ShipComment);
          $('#Basis').val(data[0].Basis);
          $('#Cover_Month').val(data[0].CoverMonth);
          $('#Payment_Terms').val(data[0].PaymentTerms);
          $('#Price').val(data[0].Price);
          $('#Unit').val(data[0].Unit);
          $('#Offer_Comments').val(data[0].OfferComments);
          $('#Comments').val(data[0].Comments);
        }
      },
      error: function(data) {
          alert('error');
      }
  });
}

function validarAutocompletar() {
  var zone2= $("#zone2").val();
  var origen = $("#Origin").val();

  var zone1= $("#zone1").val();
  var destination = $("#Destination").val();
  if (zone2 !== "" && origen !== "" && zone1 !== "" && destination !== "") {
      AutoCompletar(origen, destination);
  } else {
    alert("Please select an origin and a destination.");
  }
}


$(document).on("click", ".btnNuevoRegistro", function(){
  $('#btnLastRecord').hide();
  document.getElementById('alerta_fecha').style.display = 'none';
  document.getElementById('alerta_calidad').style.display = 'none';
  document.getElementById('alerta_cantidad').style.display = 'none';
  fila = $(this).closest("tr");	     
  opcion = 16;   
  DatereqIni = fila.find('td:eq(1)').text(); 
  RegionOriginIni = fila.find('td:eq(2)').text();    
  Origin = fila.find('td:eq(3)').text();
  Origin2 = fila.find('td:eq(4)').text();
  RegDestinationIni =fila.find('td:eq(5)').text();
  Destination =fila.find('td:eq(6)').text();
  ShipperIni = fila.find('td:eq(7)').text();
  //console.log(ShipperIni)
  QuantityIni = fila.find('td:eq(8)').text();
  CropFromIni = fila.find('td:eq(9)').text();
  CropToIni = fila.find('td:eq(10)').text();
  ColMaxIni = fila.find('td:eq(11)').text();
  LenMinIni = fila.find('td:eq(12)').text();
  MicMinIni = fila.find('td:eq(13)').text();
  MicMaxIni = fila.find('td:eq(14)').text();
  StrMinIni = fila.find('td:eq(15)').text();
  QualityCommentIni = fila.find('td:eq(16)').text();
  StartShipIni = fila.find('td:eq(17)').text();
  EndShipIni = fila.find('td:eq(18)').text();
  ShipCommentIni = fila.find('td:eq(19)').text();
  BasisIni = fila.find('td:eq(20)').text();     
  CoverMonthIni = fila.find('td:eq(21)').text();
  PaymentTermsIni = fila.find('td:eq(22)').text();
  OfferCommentsIni = fila.find('td:eq(23)').text();
  CommentsIni = fila.find('td:eq(24)').text();     
  PriceIni = fila.find('td:eq(25)').text();
  UnitIni = fila.find('td:eq(26)').text();
  nombre = fila.find('td:eq(27)').text();
  $("#form_competition").trigger("reset");
  $(".modal-header").css( "background-color", "#17562c");
  $(".modal-header").css( "color", "white" );
  $(".modal-title").text("New ID ");
  $('#modalCompetition').modal('show');    
  var hoy = new Date().toISOString().slice(0, 10); 
  $("#Date").val(hoy);   
  $("#Quantity").val(QuantityIni);    
  $("#Shipper").val(ShipperIni);  
  $("#Col_Max").val(ColMaxIni);
  $("#Len_Min").val(LenMinIni);
  $("#Mic_Min").val(MicMinIni);
  $("#Mic_Max").val(MicMaxIni);
  $("#Str_Min").val(StrMinIni);
  $("#Quality_Comment").val(QualityCommentIni);
  $("#Start_Shipment").val(StartShipIni);
  $("#End_Shipment").val(EndShipIni);
  $("#Shipment_Comment").val(ShipCommentIni);
  $("#Basis").val(BasisIni);
  $("#Payment_Terms").val(PaymentTermsIni);
  $("#Offer_Comments").val(OfferCommentsIni);
  $("#Comments").val(CommentsIni);
  $("#Price").val(PriceIni);
  $("#Destination").empty();
  $("#Origin").empty();

  years_modal_to();
  color(Origin2)
  origin2();        
  years_modalEdit(CropFromIni);
  zones(RegDestinationIni);
  zones_origen(RegionOriginIni)
  Editcovmot(CoverMonthIni);
  origin(RegionOriginIni,Origin);
  destinationNew(RegDestinationIni,Destination);
  nom = $("#nombre").val();
  regiones = $("#region").val();
  arrayReg = regiones.split(',');
  privOrigin = arrayReg.includes(Origin);
  privDest = arrayReg.includes(Destination);
  if((regiones==""|| privOrigin==true ||  privDest==true|| nom==nombre)&&editaCom==1){
    document.getElementById('btnGuardar').style.display = 'block';
  }
  else{
    document.getElementById('btnGuardar').style.display = 'none';
  }
  
  $("#zone1").change(function () {
    var pais = $(this).val();
    destinationNew(pais);
  }); 

  $("#zone2").change(function () {
    var pais2 = $(this).val();
    origin(pais2);
    origin2();
  }); 

  $("#Crop_to").append('<option class="form-control selected">' +CropToIni+ '</option>');
  $('#Origin2').append('<option class="form-control selected">' +Origin2+ '</option>');
  $("#Col_Max").val(ColMaxIni);
  $("#Unit").val(UnitIni);    
  $("#PaymentTerms").val(PaymentTermsIni);


  $("#Mic_Cat").change(function () {
    var colmax = $(this).val();
    miccat_select(colmax)
  }); 


  $("#Origin2").change(function () {
    var Origin2 = $(this).val();   
    color(Origin2)
  }); 

  $("#Origin").change(function () {
    //var Origin2 = $(this).val();   
    //color()
  }); 

  $("#Start_Shipment").change(function () {
    var dat1 = $(this).val();
    var dat2 = $("#End_Shipment").val();
    if(dat2 !=""){
      valdate(dat1,dat2);
    }
  }); 

  $("#End_Shipment").change(function () {
    var dat2 = $(this).val();
    var dat1 = $("#Start_Shipment").val();
    if(dat1 !=""){
      valdate(dat1,dat2);
    }
  }); 


  $("#Basis").keyup(function(evt) {
    //validar que solo sean numeros enteros y decimales
    var self = $(this);
    self.val(self.val().replace(/[^0-9\.-]/g, ''));
    if ((evt.which != 46 || self.val().indexOf('.') != -1) && (evt.which < 48 || evt.which > 57 || evt.which == 45)) 
    {
      evt.preventDefault();
    }
    //validamos solo 2 digito despues del punto decimal
    if($(this).val().indexOf('.')!=-1){         
      if($(this).val().split(".")[1].length > 2){                
          if( isNaN( parseFloat( this.value ) ) ) return;
          this.value = parseFloat(this.value).toFixed(2);
      }  
    }  
  });
  

  $("#Price").keyup(function(evt) {
    //validar que solo sean numeros enteros y decimales
    var self = $(this);
    self.val(self.val().replace(/[^0-9\.]/g, ''));
    if ((evt.which != 46 || self.val().indexOf('.') != -1) && (evt.which < 48 || evt.which > 57)) 
    {
      evt.preventDefault();
    }
    //validamos solo 2 digito despues del punto decimal
    if($(this).val().indexOf('.')!=-1){         
      if($(this).val().split(".")[1].length > 2){                
          if( isNaN( parseFloat( this.value ) ) ) return;
          this.value = parseFloat(this.value).toFixed(2);
      }  
    }  
  });
}); 


function valoresIni() {
  var DestinationIni = $("#Destination").val();
  var OriginIni = $("#Origin").val();
  var Origin2Ini = $("#Origin2").val();
  return [
    DatereqIni, RegionOriginIni, OriginIni, Origin2Ini, RegDestinationIni, DestinationIni, ShipperIni, QuantityIni, CropFromIni, 
    CropToIni, ColMaxIni, LenMinIni, MicMinIni, MicMaxIni, StrMinIni, QualityCommentIni, StartShipIni, EndShipIni, ShipCommentIni, 
    BasisIni, CoverMonthIni, PaymentTermsIni, OfferCommentsIni, CommentsIni, PriceIni, UnitIni
  ];
}

function compararValores() {
  var valoresIniciales = valoresIni();
  //console.log(valoresIniciales);
  var valoresEnviados = [
    Dateinq, zoneorigen, Origin, Origin2, zonedest, Destination, Shipper, Quantity,  CropFrom,
    CropTo, ColMax, LenMin, MicMin, MicMax, StrMin, QualityComment, StartShip, EndShip, ShipComment,
    Basis, CoverMonth, PaymentTerms, OfferComments, Comments, Price, Unit
  ];
  //console.log("Valores enviados:", valoresEnviados);

  var cambioFecha = false;
  var cambioCampo = false;

  // fecha cambio?
  if (valoresIniciales[0] != valoresEnviados[0]) {
      //console.log("La fecha ha cambiado");
      cambioFecha = true;
  }
  
  // otros campos han cambiado
  for (var i = 1; i < valoresIniciales.length; i++) {
      if (valoresIniciales[i] != valoresEnviados[i]) {
        //console.log("El valor en la posición " + i + " ha cambiado");
        cambioCampo = true;
      }
  }

  if (cambioFecha && cambioCampo) {
    // cambio la fecha y los campos
    return false;
  }else if (!cambioFecha && cambioCampo) {
    // Solo cambiaron los campos
    return false;
  }else  if (cambioFecha && !cambioCampo) {
    // solo cambio la fecha
    $('#confirmacionModal .modal-body p').text("The date has been changed. Do you want to save?");
    $('#confirmacionModal').modal('show');
    $('#btnContinuar').show();
    return true;
  }
  else if (!cambioFecha && !cambioCampo) {
    // No cambio la fecha ni hubo cambios en los campos
    $('#confirmacionModal .modal-body p').text("This record already exists.");
    $('#confirmacionModal').modal('show');
    $('#btnContinuar').hide();
    return true;
  }
}


function AgregarNuevoRegistro(){
  opcion=16
  $.ajax({
    url: "bd/CrudCompetition.php",
    type: "POST",
    datatype:"json",
    data:{
      opcion:opcion,
      Date:Dateinq,
      Destination:Destination,
      Origin:Origin,
      Origin2:Origin2,                             
      Quantity:Quantity,
      Shipper:Shipper,
      CropFrom:CropFrom,
      CropTo:CropTo,
      ColMax:ColMax,
      LenMin:LenMin,
      MicMin:MicMin,
      MicMax:MicMax,
      StrMin:StrMin,
      QualityComment:QualityComment,
      StartShip:StartShip,
      EndShip:EndShip,
      ShipComment:ShipComment,
      Basis:Basis,
      Unit:Unit,
      CoverMonth:CoverMonth,
      PaymentTerms:PaymentTerms,
      OfferComments:OfferComments,      
      Comments:Comments,
      Usuario:Usuario,
      zoneorigen:zoneorigen,
      zonedest:zonedest,
      Price:Price
    },  
    success: function(data){        
      alert("Successful new registration");
      table_competition.ajax.reload(null,false);
    },
    error: function(data) {
      alert('error');
    }
  });
}