<?php
include_once '../bd/conexion.php';
$objeto = new Conexion();
$conexion = $objeto->Conectar();
$opcion = (isset($_POST['opcion'])) ? $_POST['opcion'] : '';
$mes1= (isset($_POST['mes1'])) ? $_POST['mes1'] : '';
$mes2= (isset($_POST['mes2'])) ? $_POST['mes2'] : '';
$mes3= (isset($_POST['mes3'])) ? $_POST['mes3'] : '';
$mes4= (isset($_POST['mes4'])) ? $_POST['mes4'] : '';
$fecha = (isset($_POST['fecha'])) ? $_POST['fecha'] : '';
$arreglo = (isset($_POST['arreglo'])) ? $_POST['arreglo'] : '';
$User= (isset($_POST['User'])) ? $_POST['User'] : '';

//array de origenes 
$origenes=['AUS','BRA','GRC','IND','MEX','PRY','SPA','TUR','USA-MS','USA-PM','USA-SE','USA-STX','USA-WTX','WAF'];
$cov_mon=[$mes1,$mes2,$mes3,$mes4];
$aux = [];
$data=[];
switch($opcion){

    case 1:
        $fecha=date('Y-m-d');
        $tam=count($origenes);
       
        for($i=0; $i<$tam; $i++){
            $origen = $origenes[$i];
              

                    $consulta = "SELECT   IF((SELECT DISTINCT Org  FROM OrgBasis WHERE Org ='$origen')IS NOT NULL ,(SELECT DISTINCT Org  FROM OrgBasis WHERE Org ='$origen'),'$origen') as Origen,
                    (SELECT IFNULL((SELECT Bas FROM OrgBasis WHERE Org ='$origen' AND CovMon='$mes1' AND Dat BETWEEN date_sub('$fecha',interval 28 day ) AND '$fecha' AND Grd='RS-SM' ORDER BY Dat DESC,IdBasis DESC  LIMIT 1), '')) as mes1_RS_SM,
                    (SELECT IFNULL((SELECT Bas FROM OrgBasis WHERE Org ='$origen' AND CovMon='$mes1' AND Dat BETWEEN date_sub('$fecha',interval 28 day ) AND '$fecha' AND Grd='RS-M' ORDER BY Dat DESC,IdBasis DESC  LIMIT 1), '')) as mes1_RS_M,
                    (SELECT IFNULL((SELECT Bas FROM OrgBasis WHERE Org ='$origen' AND CovMon='$mes1' AND Dat BETWEEN date_sub('$fecha',interval 28 day ) AND '$fecha' AND Grd='RS-SLM' ORDER BY Dat DESC,IdBasis DESC  LIMIT 1), '')) as mes1_RS_SLM,
                    (SELECT IFNULL((SELECT Bas FROM OrgBasis WHERE Org ='$origen' AND CovMon='$mes1' AND Dat BETWEEN date_sub('$fecha',interval 28 day ) AND '$fecha' AND Grd='OE' ORDER BY Dat DESC,IdBasis DESC  LIMIT 1), ' ')) as mes1_OS,
                    (SELECT IFNULL((SELECT Bas FROM OrgBasis WHERE Org ='$origen' AND CovMon='$mes2' AND Dat BETWEEN date_sub('$fecha',interval 28 day ) AND '$fecha' AND Grd='RS-SM' ORDER BY Dat DESC,IdBasis DESC  LIMIT 1), '')) as mes2_RS_SM,
                    (SELECT IFNULL((SELECT Bas FROM OrgBasis WHERE Org ='$origen' AND CovMon='$mes2' AND Dat BETWEEN date_sub('$fecha',interval 28 day ) AND '$fecha' AND Grd='RS-M' ORDER BY Dat DESC,IdBasis DESC  LIMIT 1), '')) as mes2_RS_M,
                    (SELECT IFNULL((SELECT Bas FROM OrgBasis WHERE Org ='$origen' AND CovMon='$mes2' AND Dat BETWEEN date_sub('$fecha',interval 28 day ) AND '$fecha' AND Grd='RS-SLM' ORDER BY Dat DESC,IdBasis DESC  LIMIT 1), '')) as mes2_RS_SLM,
                    (SELECT IFNULL((SELECT Bas FROM OrgBasis WHERE Org ='$origen' AND CovMon='$mes2' AND Dat BETWEEN date_sub('$fecha',interval 28 day ) AND '$fecha' AND Grd='OE' ORDER BY Dat DESC,IdBasis DESC  LIMIT 1), '')) as mes2_OS,
                    (SELECT IFNULL((SELECT Bas FROM OrgBasis WHERE Org ='$origen' AND CovMon='$mes3' AND Dat BETWEEN date_sub('$fecha',interval 28 day ) AND '$fecha' AND Grd='RS-SM' ORDER BY Dat DESC,IdBasis DESC  LIMIT 1), '')) as mes3_RS_SM,
                    (SELECT IFNULL((SELECT Bas FROM OrgBasis WHERE Org ='$origen' AND CovMon='$mes3' AND Dat BETWEEN date_sub('$fecha',interval 28 day ) AND '$fecha' AND Grd='RS-M' ORDER BY Dat DESC,IdBasis DESC  LIMIT 1), '')) as mes3_RS_M,
                    (SELECT IFNULL((SELECT Bas FROM OrgBasis WHERE Org ='$origen' AND CovMon='$mes3' AND Dat BETWEEN date_sub('$fecha',interval 28 day ) AND '$fecha' AND Grd='RS-SLM' ORDER BY Dat DESC,IdBasis DESC  LIMIT 1), '')) as mes3_RS_SLM,
                    (SELECT IFNULL((SELECT Bas FROM OrgBasis WHERE Org ='$origen' AND CovMon='$mes3' AND Dat BETWEEN date_sub('$fecha',interval 28 day ) AND '$fecha' AND Grd='OE' ORDER BY Dat DESC,IdBasis DESC  LIMIT 1), '')) as mes3_OS,
                    (SELECT IFNULL((SELECT Bas FROM OrgBasis WHERE Org ='$origen' AND CovMon='$mes4' AND Dat BETWEEN date_sub('$fecha',interval 28 day ) AND '$fecha' AND Grd='RS-SM' ORDER BY Dat DESC,IdBasis DESC  LIMIT 1), '')) as mes4_RS_SM,
                    (SELECT IFNULL((SELECT Bas FROM OrgBasis WHERE Org ='$origen' AND CovMon='$mes4' AND Dat BETWEEN date_sub('$fecha',interval 28 day ) AND '$fecha' AND Grd='RS-M' ORDER BY Dat DESC,IdBasis DESC  LIMIT 1), '')) as mes4_RS_M,
                    (SELECT IFNULL((SELECT Bas FROM OrgBasis WHERE Org ='$origen' AND CovMon='$mes4' AND Dat BETWEEN date_sub('$fecha',interval 28 day ) AND '$fecha' AND Grd='RS-SLM' ORDER BY Dat DESC,IdBasis DESC  LIMIT 1), '')) as mes4_RS_SLM,
                    (SELECT IFNULL((SELECT Bas FROM OrgBasis WHERE Org ='$origen' AND CovMon='$mes4' AND Dat BETWEEN date_sub('$fecha',interval 28 day ) AND '$fecha' AND Grd='OE' ORDER BY Dat DESC,IdBasis DESC  LIMIT 1), '')) as mes4_OS
                    
                    ";
                    $resultado = $conexion->prepare($consulta);
                    $resultado->execute();
                    array_push($data, $resultado->fetch(PDO::FETCH_ASSOC));
            

        }




    break;

    case 2:

        $consulta  = 'INSERT INTO amsadb1.OrgBasis (Dat, Org, CovMon, Grd, Bas, User) VALUES (:fecha, :origen, :mescov, :grado, :basis, :usuario );';
        $resultado = $conexion->prepare($consulta);

        foreach ($arreglo as $fila){
            $aux=json_decode($fila);
            
            foreach ($aux as $datos){
                $resultado->bindValue(':fecha', $fecha);
                $resultado->bindValue(':origen', $datos->origen);
                $resultado->bindValue(':mescov', $datos->mescov);
                $resultado->bindValue(':grado', $datos->grd);
                $resultado->bindValue(':basis', $datos->basis);
                $resultado->bindValue(':usuario', $User);
                $resultado->execute();
            }
        }

        $data ="Data Successfully Saved";

        //print_r($arreglo);


    break;

    case 3:

        $consulta = "SELECT CovMon from amsadb1.Inquiries_Covmon;";      
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $data=$resultado->fetchAll(PDO::FETCH_ASSOC);  

    break;

    case 4:
        $consulta2="";
        $consulta = "UPDATE amsadb1.Inquiries_Covmon SET Inquiries_Covmon.Basis= 0;";      
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        

        $consulta2 .= "UPDATE amsadb1.Inquiries_Covmon SET Inquiries_Covmon.Basis= 1 where CovMon = '$mes1' ;";    
        
        $consulta2 .= "UPDATE amsadb1.Inquiries_Covmon SET Inquiries_Covmon.Basis= 2 where CovMon = '$mes2';"; 

        $consulta2 .= "UPDATE amsadb1.Inquiries_Covmon SET Inquiries_Covmon.Basis= 3 where CovMon = '$mes3';"; 

        $consulta2 .= "UPDATE amsadb1.Inquiries_Covmon SET Inquiries_Covmon.Basis= 4 where CovMon = '$mes4';";

        $resultado = $conexion->prepare($consulta2);
        $resultado->execute();

        $data="Updated data";



    break;
    
    case 5:
        $consulta = "SELECT Email FROM Users_Inquiries WHERE EditBasis = 1 AND Email != 'NULL';";
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();
        $data=$resultado->fetchAll(PDO::FETCH_COLUMN, 0);
        $data = implode (',',$data); 
    break;
    
    
}

print json_encode($data, JSON_UNESCAPED_UNICODE);


?>