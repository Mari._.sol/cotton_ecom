<?php
 session_start();
if (!isset($_SESSION['user_inquiries'])) :
    include_once('index.php');
else :
    $region = $_SESSION['Region'];
    $usuario=$_SESSION['user_inquiries'];
    $priv = $_SESSION['Priv'];
    $vistaComp=$_SESSION['ViewCompetition'];
    $vistaInq= $_SESSION['ViewInquirie'];
    $vistabuyer= $_SESSION['ViewBuyer'];
    $CostToLand = $_SESSION['CostToLand'];
    $vistabasis = $_SESSION['basis'];
?>
<!doctype html>
<html lang="es en">
    <head>
        <meta charset="utf-8">
        <!-- <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv='cache-control' content='no-cache'>
        <meta http-equiv='expires' content='0'>
        <meta http-equiv='pragma' content='no-cache'>-->
        <link rel="shortcut icon" href="img/ecom.png" /> 
        <title>Home</title>

        <!-- CSS bootstrap -->
        <link rel="stylesheet" href="../cotton_ecom/assets/bootstrap/css/bootstrap.min.css">

        <!-- CSS personalizado -->
        <link rel="stylesheet" href="../cotton_ecom/main.css">
        <!--Google fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Inter:wght@300&family=Padauk&display=swap" rel="stylesheet"> 
        <script defer type="text/javascript" src="./assets/bootstrap/js/bootstrap.min.js"> </script>
    </head>

    <body >

        <!-- Esto es el Nav bar, todo contenido en un container-fluid -->
        <nav class="navbar navbar-expand-lg bg-transparent navbar-custom">
            <div class="container-fluid" style="padding-right: 1.5rem; padding-left: 1.5rem;">
                <div class="dropdown  nav-item col-xl-1 col-lg-1 col-md-2 col-sm-2 col-xs-1">
                    <a class="btn dropdown-toggle " href="main.php" role="button" id="dropdownMenuLink" data-bs-toggle="dropdown" aria-expanded="false">
                        <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" fill="white" class="bi bi-list" viewBox="0 0 16 16">
                            <path fill-rule="evenodd" d="M2.5 12a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5z" />
                        </svg>
                    </a>
                    <ul class="dropdown-menu " aria-labelledby="dropdownMenuLink">
                        <?php if ($vistaInq == 1){?>      
                            <li><a class="dropdown-item" href="inquirie.php">Inquiries</a></li>
                        <?php } ?>  
                        <?php if ($vistaComp == 1){?> 
                            <li><a class="dropdown-item" href="competition.php">Competition Offers</a></li>   
                        <?php } ?>                           
                        <?php if ($vistabuyer == 1){?>           
                            <li><a class="dropdown-item" href="buyers.php">Buyers</a></li>    
                        <?php } ?>
                        <?php if ($CostToLand == 1){?>                            
                            <li><a class="dropdown-item" href="CostToLand.php">Cost to Land</a></li> 
                        <?php } ?>
                        <?php if ($vistabasis  == 1){?>                            
                            <li><a class="dropdown-item" href="Basis.php">Origin Basis</a></li> 
                        <?php } ?> 
                        <?php if ($vistabasis  == 1){?>
                            <li><a class="dropdown-item" href="BasisJM.php">Origin Basis - JM Cotton</a></li>
                        <?php } ?>
                    </ul>
                </div>
                <a class="navbar-brand nav-item col-xl-1 col-lg-1 col-md-1 col-sm-2 col-xs-2" href="main.php">
                    <div class="logos"><img class="log" src="img/logo1.png">  ECOM/ Home</div>
                </a>
                <div class="container-fluid  col-xl-7 col-lg-6 col-md-3 col-sm-2 col-xs-1">
                </div>
                <div class="dropdown nav-item col-xl-2 col-lg-3 col-md-3 col-sm-3 col-xs-1 d-flex justify-content-end" style="padding-right: 1.5rem;">
                    <button style="color:white;" class="btn dropdown-toggle btn-outline-success" data-bs-toggle="dropdown" aria-expanded="false" id="dropdownMenuLink2"value="<?php echo $_SESSION['user_inquiries']; ?>"><?php echo $_SESSION['user_inquiries']; ?></button>
                    <ul class="dropdown-menu" aria-labelledby="dropdownMenuLink2">
                        <li><a class="dropdown-item" href="logout.php">Cerrar sesión</a></li>
                    </ul>
                </div>
            </div>
        </nav>
    </body>

</html>
<?php
endif;
?>