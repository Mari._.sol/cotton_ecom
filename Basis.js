$(document).ready(function() {
    mes1=$("#mes1").val();
    mes2=$("#mes2").val();
    mes3=$("#mes3").val();
    mes4=$("#mes4").val();

    var date=new Date();  
    var day=String(date.getDate()).padStart(2, '0');          
    var month=("0" + (date.getMonth() + 1)).slice(-2); 
    var year=date.getFullYear();  
    fechahoy=year+"-"+month+"-"+day
    User=$("#usuario").val();
    permisoedit=$("#editar").val();

    $.ajax({
        url: "bd/CrudBasis.php",
        type: "POST",
        datatype:"json",
        data:  {opcion:1,mes1:mes1,mes2:mes2,mes3:mes3,mes4:mes4},    
        success: function(data){    
            opts = JSON.parse(data);
            for (var i = 0; i< opts.length; i++){ 

                var fila = $("<tr>  class='sinborde'");
                fila.append("<td>"+opts[i]['Origen']+"</td>");
                fila.append("<td>"+opts[i]['mes1_RS_SM']+"</td>");
                fila.append("<td>"+opts[i]['mes1_RS_M']+"</td>");
                fila.append("<td>"+opts[i]['mes1_RS_SLM']+"</td>");
                fila.append("<td>"+opts[i]['mes1_OS']+"</td>");
                fila.append("<td>"+opts[i]['mes2_RS_SM']+"</td>");
                fila.append("<td>"+opts[i]['mes2_RS_M']+"</td>");
                fila.append("<td>"+opts[i]['mes2_RS_SLM']+"</td>");
                fila.append("<td>"+opts[i]['mes2_OS']+"</td>");
                fila.append("<td>"+opts[i]['mes3_RS_SM']+"</td>");
                fila.append("<td>"+opts[i]['mes3_RS_M']+"</td>");
                fila.append("<td>"+opts[i]['mes3_RS_SLM']+"</td>");
                fila.append("<td>"+opts[i]['mes3_OS']+"</td>");
                fila.append("<td>"+opts[i]['mes4_RS_SM']+"</td>");
                fila.append("<td>"+opts[i]['mes4_RS_M']+"</td>");
                fila.append("<td>"+opts[i]['mes4_RS_SLM']+"</td>");
                fila.append("<td>"+opts[i]['mes4_OS']+"</td>");
                if(permisoedit==1){
                    fila.append("<td> <button class='btn btn-primary  btn-sm editar' title='Edit' ><i class='material-icons'>edit</i></button></td>");
                }
                $('#table_basis').append(fila);
            }
        }
        });


    $("#table_basis").on("click", ".editar", function() {

        var fila = $(this).closest("tr");
        origen=fila.find('td:eq(0)').text();
        mes1_RS_SM=fila.find('td:eq(1)').text();
        mes1_RS_M=fila.find('td:eq(2)').text();
        mes1_RS_SLM=fila.find('td:eq(3)').text();
        mes1_OS=fila.find('td:eq(4)').text();

        mes2_RS_SM=fila.find('td:eq(5)').text();
        mes2_RS_M=fila.find('td:eq(6)').text();
        mes2_RS_SLM=fila.find('td:eq(7)').text();
        mes2_OS=fila.find('td:eq(8)').text();

        mes3_RS_SM=fila.find('td:eq(9)').text();
        mes3_RS_M=fila.find('td:eq(10)').text();
        mes3_RS_SLM=fila.find('td:eq(11)').text();
        mes3_OS=fila.find('td:eq(12)').text();

        mes4_RS_SM=fila.find('td:eq(13)').text();
        mes4_RS_M=fila.find('td:eq(14)').text();
        mes4_RS_SLM=fila.find('td:eq(15)').text();
        mes4_OS=fila.find('td:eq(16)').text();

        aux1=mes1_RS_SM;
        aux2=mes1_RS_M
        aux3=mes1_RS_SLM
        aux4=mes1_OS


        aux5=mes2_RS_SM;
        aux6=mes2_RS_M
        aux7=mes2_RS_SLM
        aux8=mes2_OS

        aux9=mes3_RS_SM;
        aux10=mes3_RS_M
        aux11=mes3_RS_SLM
        aux12=mes3_OS

        aux13=mes4_RS_SM;
        aux14=mes4_RS_M
        aux15=mes4_RS_SLM
        aux16=mes4_OS
        //ocultar alertas
        document.getElementById('alerta_mes1').style.display = 'none'; 
        document.getElementById('alerta_mes2').style.display = 'none'; 
        document.getElementById('alerta_mes3').style.display = 'none'; 
        document.getElementById('alerta_mes4').style.display = 'none'; 
        $("#form_basis").trigger("reset");
        $(".modal-header").css( "background-color", "#17562c");
        $(".modal-header").css( "color", "white" );
        $(".modal-title").text("Edit Origin "+origen);
        $('#modal_basis').modal('show');

   


        $("#origin").val(origen)

        $("#m1_RS_SM").val(mes1_RS_SM)
        $("#m1_RS_M").val(mes1_RS_M)
        $("#m1_RS_SLM").val(mes1_RS_SLM)
        $("#m1_OS").val(mes1_OS)

        $("#m2_RS_SM").val(mes2_RS_SM)
        $("#m2_RS_M").val(mes2_RS_M)
        $("#m2_RS_SLM").val(mes2_RS_SLM)
        $("#m2_OS").val(mes2_OS)


        $("#m3_RS_SM").val(mes3_RS_SM)
        $("#m3_RS_M").val(mes3_RS_M)
        $("#m3_RS_SLM").val(mes3_RS_SLM)
        $("#m3_OS").val(mes3_OS)


        $("#m4_RS_SM").val(mes4_RS_SM)
        $("#m4_RS_M").val(mes4_RS_M)
        $("#m4_RS_SLM").val(mes4_RS_SLM)
        $("#m4_OS").val(mes4_OS)
      

    });

    //boton guardar
    $('#form_basis').submit(function(e){
        e.preventDefault(); 
        m1_RS_SM= $("#m1_RS_SM").val();
        m1_RS_M= $("#m1_RS_M").val();
        m1_RS_SLM=$("#m1_RS_SLM").val();
        m1_OS=$("#m1_OS").val();

        validaM1= valida_valores(m1_RS_SM,m1_RS_M,m1_RS_SLM);

        

        m2_RS_SM= $("#m2_RS_SM").val();
        m2_RS_M= $("#m2_RS_M").val();
        m2_RS_SLM=$("#m2_RS_SLM").val();
        m2_OS=$("#m2_OS").val();

       validaM2= valida_valores(m2_RS_SM,m2_RS_M,m2_RS_SLM);


        m3_RS_SM= $("#m3_RS_SM").val();
        m3_RS_M= $("#m3_RS_M").val();
        m3_RS_SLM=$("#m3_RS_SLM").val();
        m3_OS=$("#m3_OS").val();

        validaM3= valida_valores(m3_RS_SM,m3_RS_M,m3_RS_SLM);

        m4_RS_SM= $("#m4_RS_SM").val();
        m4_RS_M= $("#m4_RS_M").val();
        m4_RS_SLM=$("#m4_RS_SLM").val();
        m4_OS=$("#m4_OS").val();
        //console.log(m4_RS_SM,m4_RS_M)
        //console.log(parseFloat(m4_RS_SM)>=parseFloat(m4_RS_M))

        validaM4= valida_valores( m4_RS_SM, m4_RS_M, m4_RS_SLM);



        datos=[];
        //DATOS MES DE COVERTURA 1 
        
        if(m1_RS_SM != '' /*&& m1_RS_SM!=aux1*/){
            var object1 = {              
                origen: origen,
                mescov: mes1,
                grd: "RS-SM",
                basis: m1_RS_SM
            };

            datos.push(object1);
        }

        if(m1_RS_M != '' /*&& m1_RS_M!=aux2*/){
            var object1 = {              
                origen: origen,
                mescov: mes1,
                grd: "RS-M",
                basis: m1_RS_M
            };

            datos.push(object1);
        }

        if(m1_RS_SLM != '' /*&& m1_RS_SLM!=aux3*/){
            var object1 = {              
                origen: origen,
                mescov: mes1,
                grd: "RS-SLM",
                basis: m1_RS_SLM
            };

            datos.push(object1);
        }

        if(m1_OS != '' && m1_OS != ' ' /*&& m1_OS!=aux4*/){
            var object1 = {              
                origen: origen,
                mescov: mes1,
                grd: "OE",
                basis: m1_OS
            };

            datos.push(object1);
        }


        //DATOS MES DE COVERTURA 2 

        if(m2_RS_SM != '' /*&& m2_RS_SM!=aux5*/){
            var object1 = {              
                origen: origen,
                mescov: mes2,
                grd: "RS-SM",
                basis: m2_RS_SM
            };

            datos.push(object1);
        }

        if(m2_RS_M != '' /*&& m2_RS_M!=aux6*/){
            var object1 = {              
                origen: origen,
                mescov: mes2,
                grd: "RS-M",
                basis: m2_RS_M
            };

            datos.push(object1);
        }

        if(m2_RS_SLM != '' /*&& m2_RS_SLM!=aux7*/){
            var object1 = {              
                origen: origen,
                mescov: mes2,
                grd: "RS-SLM",
                basis: m2_RS_SLM
            };

            datos.push(object1);
        }

        if(m2_OS != '' /*&& m2_OS!=aux8*/){
            var object1 = {              
                origen: origen,
                mescov: mes2,
                grd: "OE",
                basis: m2_OS
            };

            datos.push(object1);
        }
        // DATOS MES DE COVERTURA 3 

        if(m3_RS_SM != ''/* && m3_RS_SM!=aux9*/){
            var object1 = {              
                origen: origen,
                mescov: mes3,
                grd: "RS-SM",
                basis: m3_RS_SM
            };

            datos.push(object1);
        }

        if(m3_RS_M != ''/* && m3_RS_M!=aux10*/){
            var object1 = {              
                origen: origen,
                mescov: mes3,
                grd: "RS-M",
                basis: m3_RS_M
            };

            datos.push(object1);
        }

        if(m3_RS_SLM != ''/* && m3_RS_SLM!=aux11*/){
            var object1 = {              
                origen: origen,
                mescov: mes3,
                grd: "RS-SLM",
                basis: m3_RS_SLM
            };

            datos.push(object1);
        }

        if(m3_OS != '' /*&& m3_OS!=aux12*/){
            var object1 = {              
                origen: origen,
                mescov: mes3,
                grd: "OE",
                basis: m3_OS
            };

            datos.push(object1);
        }



        // MES DE COVERTURA 4

        
        if(m4_RS_SM != '' /*&& m4_RS_SM!=aux13*/){
            var object1 = {              
                origen: origen,
                mescov: mes4,
                grd: "RS-SM",
                basis: m4_RS_SM
            };

            datos.push(object1);
        }

        if(m4_RS_M != '' /*&& m4_RS_M!=aux14*/){
            var object1 = {              
                origen: origen,
                mescov: mes4,
                grd: "RS-M",
                basis: m4_RS_M
            };

            datos.push(object1);
        }

        if(m4_RS_SLM != '' /*&& m4_RS_SLM!=aux15*/){
            var object1 = {              
                origen: origen,
                mescov: mes4,
                grd: "RS-SLM",
                basis: m4_RS_SLM
            };

            datos.push(object1);
        }

        if(m4_OS != '' /*&& m4_OS!=aux16*/){
            var object1 = {              
                origen: origen,
                mescov: mes4,
                grd: "OE",
                basis: m4_OS
            };

            datos.push(object1);
        }

        
      // console.log(datos.length);

       //mandar los datos a bd

       var arreglo = {
        array: JSON.stringify(datos)
       };

       if(validaM1 !=1 || validaM2 != 1 || validaM3 !=1 || validaM4 != 1){
            if(validaM1 == 0){
                document.getElementById('alerta_mes1').style.display = 'block'; 
            }

            else{
                document.getElementById('alerta_mes1').style.display = 'none'; 
            }


            if(validaM2 == 0){
                document.getElementById('alerta_mes2').style.display = 'block'; 
            }

            else{
                document.getElementById('alerta_mes2').style.display = 'none'; 
            }


            if(validaM3 == 0){
                document.getElementById('alerta_mes3').style.display = 'block'; 
            }
            else{
                document.getElementById('alerta_mes3').style.display = 'none'; 
            }


            if(validaM4 == 0){
                document.getElementById('alerta_mes4').style.display = 'block'; 
            }
            else{
                document.getElementById('alerta_mes4').style.display = 'none'; 
            }


       }

       if(validaM1 ===1 && validaM2 === 1 && validaM3 === 1 && validaM4 === 1){
        document.getElementById('alerta_mes1').style.display = 'none'; 
        document.getElementById('alerta_mes2').style.display = 'none'; 
        document.getElementById('alerta_mes3').style.display = 'none'; 
        document.getElementById('alerta_mes4').style.display = 'none'; 

        if(datos.length > 0){

            $.ajax({
                url: "bd/CrudBasis.php",
                type: "POST",
                datatype:"json",
                data:  {opcion:2,arreglo:arreglo,fecha:fechahoy,User:User},    
                success: function(data){  
                    alert(data)
                    $('#modal_basis').modal('hide');
                     location.reload();

                },error: function(data) {
                    alert('error to load data');
                }
            });

        }
       }


       /* if(datos.length > 0){

            $.ajax({
                url: "bd/CrudBasis.php",
                type: "POST",
                datatype:"json",
                data:  {opcion:2,arreglo:arreglo,fecha:fechahoy,User:User},    
                success: function(data){  
                    alert(data)

                },error: function(data) {
                    alert('error to load data');
                }
            });

        }

        */

    });

    


    //EXPORTAR A EXCEL LA TABLA 

    $("#exportar_basis").click(function(e){ 
        e.preventDefault();
        window.open("./bd/export_basis.php");

    });


           

           
});


$(document).ready(function() {
    //EDITAR LOS MESES DE COVERTURA

    $("#editar_mes").click(function(e){ 
        e.preventDefault();
        $("#covmes1,#covmes2,#covmes3,#covmes4").empty();
        
        $.ajax({
            url: "bd/CrudBasis.php",
            type: "POST",
            datatype:"json",
            data:  {opcion:3},    
            success: function(data){  
                opts = JSON.parse(data);

                for(i=0; i< opts.length; i++){
                    
                    $('#covmes1').append($('<option>').val(opts[i].CovMon).text(opts[i].CovMon)); 
                    $('#covmes2').append($('<option>').val(opts[i].CovMon).text(opts[i].CovMon)); 
                    $('#covmes3').append($('<option>').val(opts[i].CovMon).text(opts[i].CovMon)); 
                    $('#covmes4').append($('<option>').val(opts[i].CovMon).text(opts[i].CovMon)); 
                    
                }

       
                 
                 //console.log(mes1,mes2,mes3)

                $("#modmescover").trigger("reset");
                $(".modal-header").css( "background-color", "#17562c");
                $(".modal-header").css( "color", "white" );
                $(".modal-title").text("Edit Cover Month");
                $('#modalcover').modal('show');
                document.getElementById('alerta_val_mes').style.display = 'none'; 

                $("select#covmes4").val(mes4);  
                $("select#covmes3").val(mes3);  
                $("select#covmes2").val(mes2);  
                $("select#covmes1").val(mes1);  
        


              
            },error: function(data) {
                alert('error to load data');
            }
        });

            
        $("#btnguardarmes").click(function(e){ 
            e.preventDefault();
            nuevomes1=$('#covmes1').val();  
            nuevomes2=$('#covmes2').val(); 
            nuevomes3=$('#covmes3').val(); 
            nuevomes4=$('#covmes4').val(); 

            validacion=valida_meses(nuevomes1,nuevomes2,nuevomes3,nuevomes4);

            console.log(validacion);

            if(validacion==0){
                document.getElementById('alerta_val_mes').style.display = 'none'; 
                $.ajax({
                    url: "bd/CrudBasis.php",
                    type: "POST",
                    datatype:"json",
                    data:  {opcion:4, mes1:nuevomes1, mes2:nuevomes2, mes3:nuevomes3, mes4:nuevomes4},    
                    success: function(data){  
                       alert(data);
                       location.reload();
                    }
                });

            }

            else{

                document.getElementById('alerta_val_mes').style.display = 'block'; 
                
            }
        });


        $("#closecoverx,#closecove").click(function(e){ 
            e.preventDefault();
            $("#modmescover").trigger("reset");
            $('#modalcover').modal('hide');
        });

        
      
    });

});

//ENVIAR CORREO RECORDATORIO

$(document).ready(function() {
    $("#PsendEmail").click(function(){ 
            $("#modal-msj").trigger("reset");
            $(".modal-header").css( "background-color", "#17562c");
            $(".modal-header").css( "color", "white" );
            $(".modal-title").text("Preview Mail");
            $('#modal-msj').modal('show');
            $(".modal-footer").css( "color", "white" );

        $.ajax({
            url: "bd/CrudBasis.php",
            type: "POST",
            datatype:"json",
            data:  {opcion :5},    
            success: function(data){
            var org = data.slice(1, -1);
            $("#maildestino").val(org)
        
            }
        });
       
    });
    
     $("#sendEmail").click(function(){
            User=$("#usuario").val();
            asunto = $("#subject").val();
            correos = $("#maildestino").val();
            $('#modal-msj').modal('hide');
            $.ajax({
                url: "bd/MailRecordatorioBasis.php",
                type: "POST",
                datatype:"json",
                data:  {asunto:asunto, correos:correos,User:User},    
                success: function(data){
                    alert(data)
                }
            });
        });

});



function valida_valores(val1,val2,val3){
    //console.log(val2)
    ban = 0;

    if(val1!== '' && val2 !=='' && val3 !==''){

        if(parseFloat(val1)>=parseFloat(val2) && parseFloat(val1)>=parseFloat(val3) && parseFloat(val2)>= parseFloat(val3)){
            ban = 1;
        }
        else{
            ban= 0;
        }

    }

    else if(val1 !=='' && val2 !== '' && val3==='') {
    
        if(parseFloat(val1) >= parseFloat(val2)){
            ban= 1;
        }

        else{
            ban = 0;
        }

    }

    else if(val1 !==''&& val2 === '' && val3===''){
        ban= 1;
    }
    else if(val1 !=='' && val2 === '' && val3!==''){
        if(parseFloat(val1) >= parseFloat(val3)){
            ban= 1;
        }

        else{
            ban = 0;
        }

    }
    else if(val1 === '' && val2!== ''  && val3!== '' ){
       
        if(parseFloat(val2) >= parseFloat(val3)){
            ban= 1;
        }

        else{
            ban = 0;
        }

    }
    
    else if(val1 === '' && val2 !== '' && val3=== ''){
        ban = 1;
    }
    
    
    else if(val1 === '' && val2 === '' && val3!== ''){
        ban = 1;
    }
    else if (val1 === '' && val2 === '' && val3 ===  '') {
        ban = 1;
    }
    else{
        ban = 0;
    }

    return ban;

}


function valida_meses(m1,m2,m3,m4){
    valor=0;
    
    if(m1==m2 || m1==m3 || m1==m4){
        valor=1;
    }
    if(m2==m3 || m2==m4){
        valor=1;
    }

    if(m3==m4){
        valor=1;
    }

    return valor;

}