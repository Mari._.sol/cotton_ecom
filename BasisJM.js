var opcion,IDReq,editaCom;
$(document).ready(function() {
  $('#table_competition tfoot th').each( function () {
      var title = $(this).text();
      $(this).html( '<input type="text" placeholder="'+title+'" />' );
  } );

  permisoedit=$("#editar").val();
  Usuario = $("#usuario").val()
  Priv = $("#priv").val();

  if(permisoedit==1){
    editar_tabla="<div class='text-center'><div class='btn-group'><button class='btn btn-primary  btn-sm btneditar' title='Edit' ><i class='material-icons'>edit</i></button></div></div>";
  }else{
    editar_tabla=""
  }

  table_competition = $('#table_competition').removeAttr('width').DataTable({
    responsive: "true",
    dom: 'Brtilp',       
    buttons:[ 
      {
        extend:    'excelHtml5',
        text:      '<i class="fas fa-file-excel"></i> ',
        titleAttr: 'Export to Excel',
        className: 'btn btn-success',
        download: 'open',
        exportOptions: {
            columns: ":not(.no-exportar)"
        }
      },
    {
      text: '<i class="bi bi-funnel-fill"></i>',
      titleAttr: 'Filter by',
      orientation: 'landscape',
      className: 'btn btn-info btn-filtrar',
      attr:{
        id:'filtraboton',
        "data-toggle": 'modal tooltip',
        "disabled": false
      }
    },
    ],

    initComplete: function () {
        // Apply the search
        this.api().columns().every( function () {
            var that = this;

            $( 'input', this.footer() ).on( 'keyup change clear', function () {
                if ( that.search() !== this.value ) {
                    that
                    .search( this.value, true, false ).draw();
                        
                }
            } );
        } );
    },

    "order": [ 0, 'desc' ],
    "scrollX": true,
    "scrollY": "50vh",
    "autoWidth": true,
    "scrollCollapse": true,
    "lengthMenu": [100,200,300,500],
    fixedColumns:   {
        left: 0,
        right: 1
    },   
    "ajax":{            
      "url": "bd/CrudBasisJM.php",
      "method": 'POST', //usamos el metodo POST
      "data":{opcion:9,Usuario:Usuario,Priv:Priv},//user
      "dataSrc":""
  },
    "columns":[        
        {"data": "IDInquirie"},
        {"data": "Date"},    
        {"data": "RegionOrigin",width: 150}, 
        {"data": "CountryName",width: 100}, 
        {"data": "Origin2",width: 150},
        {"data": "Quantity"},
        {"data": "CropFrom"},
        {"data": "CropTo"},
        {"data": "Comments"},
        {"data": "ColMax"},
        {"data": "LenMin"},
        {"data": "MicMin"},
        {"data": "MicMax"},
        {"data": "StrMin"},
        {"data": "QualityComment"},
        {"data": "Basis"},          
        {"data": "CoverMonth"},
        {"data": "NameUser"},  
              
        {"defaultContent":editar_tabla,width: '50px'},
    ],
  });

    var oTable = $('#table_competition').DataTable();

    //mostrar modal new 
    $("#btnNuevo").click(function(){
      document.getElementById('btnGuardar').style.display = 'block';
      document.getElementById('alerta_calidad').style.display = 'none';
      document.getElementById('alerta_cantidad').style.display = 'none';
      opcion = 5;
        CoverMonth="";        
        Origin2="";
        var date=new Date();  
        var day=String(date.getDate()).padStart(2, '0');          
        var month=("0" + (date.getMonth() + 1)).slice(-2); 
        var year=date.getFullYear();  
        fechahoy=year+"-"+month+"-"+day

        $("#form_competition").trigger("reset");
        $(".modal-header").css( "background-color", "#17562c");
        $(".modal-header").css( "color", "white" );
        $(".modal-title").text("New");
        $('#modalCompetition').modal('show');
        $("#Origin").empty();
        $("#Destination").empty();
        years_modal();
        years_modal_to();
        origin2();
        zones_origen()  
        covmot();
        color(Origin2);

        $("#Mic_Cat").change(function () {
          var colmax = $(this).val();
          miccat_select(colmax)
        });

        $("#zone2").change(function () {
          var pais2 = $(this).val();
          origin(pais2);
          origin2();
        });    

        $("#Mic_Min").keyup(function(evt) {
          $("select#Mic_Cat").val("");
          var MicMin = $(this).val();      
          var MicMax =$("#Mic_Max").val();
          //validar que micmax sea mayor que micmin
          if(MicMax !=""){
            validamic(MicMin,MicMax)
          }
          //validar que solo sean numeros enteros y decimales
          var self = $(this);
          self.val(self.val().replace(/[^0-9\.]/g, ''));
          if ((evt.which != 46 || self.val().indexOf('.') != -1) && (evt.which < 48 || evt.which > 57)) 
          {
            evt.preventDefault();
          }
          //validamos solo 1 digito despues del punto decimal
          if($(this).val().indexOf('.')!=-1){         
            if($(this).val().split(".")[1].length > 1){                
                if( isNaN( parseFloat( this.value ) ) ) return;
                this.value = parseFloat(this.value).toFixed(1);
            }  
          }    
        });
        
        $("#Mic_Max").keyup(function(evt) {
          $("select#Mic_Cat").val("");
          var MicMax = $(this).val();
          var MicMin =$("#Mic_Min").val();
          if(MicMin !=""){
            validamic(MicMin,MicMax)
          }
          //validar que solo sean numeros enteros y decimales
          var self = $(this);
          self.val(self.val().replace(/[^0-9\.]/g, ''));
          if ((evt.which != 46 || self.val().indexOf('.') != -1) && (evt.which < 48 || evt.which > 57)) 
          {
            evt.preventDefault();
          }
          //validamos solo 1 digito despues del punto decimal
          if($(this).val().indexOf('.')!=-1){         
            if($(this).val().split(".")[1].length > 1){                
              if( isNaN( parseFloat( this.value ) ) ) return;
              this.value = parseFloat(this.value).toFixed(1);
            }  
          }
        });

        $("#Len_Min").keyup(function(evt) {
            //validar que solo sean numeros enteros y decimales
            var self = $(this);
            self.val(self.val().replace(/[^0-9\.]/g, ''));
            if ((evt.which != 46 || self.val().indexOf('.') != -1) && (evt.which < 48 || evt.which > 57)) 
            {
                evt.preventDefault();
            }
            //validamos solo 1 digito despues del punto decimal
            if($(this).val().indexOf('.')!=-1){         
                if($(this).val().split(".")[1].length > 1){                
                    if( isNaN( parseFloat( this.value ) ) ) return;
                    this.value = parseFloat(this.value).toFixed(1);
                }  
            }    
        });

        $("#Str_Min").keyup(function(evt) {
          //validar que solo sean numeros enteros y decimales
          var self = $(this);
          self.val(self.val().replace(/[^0-9\.]/g, ''));
          if ((evt.which != 46 || self.val().indexOf('.') != -1) && (evt.which < 48 || evt.which > 57)) 
          {
            evt.preventDefault();
          }
          //validamos solo 1 digito despues del punto decimal
          if($(this).val().indexOf('.')!=-1){         
            if($(this).val().split(".")[1].length > 1){                
                if( isNaN( parseFloat( this.value ) ) ) return;
                this.value = parseFloat(this.value).toFixed(1);
            }  
          }    
        });
        
        $("#Origin2").change(function () {
          var Origin2 = $(this).val();
          color(Origin2)
        }); 

        $("#Date").val(fechahoy);

        $("#Quantity").keyup(function(evt) {
          //validar que solo sean numeros enteros y decimales
          validaCantidad($(this).val());
          var self = $(this);
          self.val(self.val().replace(/[^0-9\.]/g, ''));
          if ((evt.which != 46 || self.val().indexOf('.') != -1) && (evt.which < 48 || evt.which > 57)) 
          {
            evt.preventDefault();
          }
          //validamos solo 2 digito despues del punto decimal
          if($(this).val().indexOf('.')!=-1){         
            if($(this).val().split(".")[1].length > 2){                
              if( isNaN( parseFloat( this.value ) ) ) return;
              this.value = parseFloat(this.value).toFixed(2);
            }  
          }
        });

      $("#Basis").keyup(function(evt) {
        //validar que solo sean numeros enteros y decimales
        var self = $(this);
        self.val(self.val().replace(/[^0-9\.-]/g, ''));
        if ((evt.which != 46 || self.val().indexOf('.') != -1) && (evt.which < 48 || evt.which > 57 || evt.which == 45)) 
        {
          evt.preventDefault();
        }
        //validamos solo 2 digito despues del punto decimal
        if($(this).val().indexOf('.')!=-1){         
          if($(this).val().split(".")[1].length > 2){                
              if( isNaN( parseFloat( this.value ) ) ) return;
              this.value = parseFloat(this.value).toFixed(2);
          }  
        }  
      });
    });

    // btn editar  
    $(document).on("click", ".btneditar", function(){
      document.getElementById('alerta_calidad').style.display = 'none';
      document.getElementById('alerta_cantidad').style.display = 'none';
      fila = $(this).closest("tr");	     
      opcion = 11;
      IDReq = parseInt(fila.find('td:eq(0)').text());
      Datereq = fila.find('td:eq(1)').text(); 
      RegionOrigin = fila.find('td:eq(2)').text();    
      Origin = fila.find('td:eq(3)').text();
      Origin2 = fila.find('td:eq(4)').text();
      Quantity = fila.find('td:eq(5)').text();
      CropFrom = fila.find('td:eq(6)').text();
      CropTo = fila.find('td:eq(7)').text();
      Comments = fila.find('td:eq(8)').text();
      ColMax = fila.find('td:eq(9)').text();
      LenMin = fila.find('td:eq(10)').text();
      MicMin = fila.find('td:eq(11)').text();
      MicMax = fila.find('td:eq(12)').text();
      StrMin = fila.find('td:eq(13)').text();
      QualityComment = fila.find('td:eq(14)').text();
      Basis = fila.find('td:eq(15)').text();     
      CoverMonth = fila.find('td:eq(16)').text();
      nombre = fila.find('td:eq(17)').text();
      $("#form_competition").trigger("reset");
      $(".modal-header").css( "background-color", "#17562c");
      $(".modal-header").css( "color", "white" );
      $(".modal-title").text("Edit ID " + IDReq);
      $('#modalCompetition').modal('show');    
      $("#Date").val(Datereq);
      $("#Origin").empty();
      $("#Quantity").val(Quantity);
      $("#Col_Max").val(ColMax);
      $("#Len_Min").val(LenMin);
      $("#Mic_Min").val(MicMin);
      $("#Mic_Max").val(MicMax);
      $("#Str_Min").val(StrMin);
      $("#Quality_Comment").val(QualityComment);
      $("#Basis").val(Basis);
      $("#Comments").val(Comments);


      years_modal_to();
      color(Origin2)
      origin2();        
      years_modalEdit(CropFrom);
      zones_origen(RegionOrigin)
      Editcovmot(CoverMonth);
      origin(RegionOrigin,Origin);
      nom = $("#nombre").val();
      regiones = $("#region").val();
      arrayReg = regiones.split(',');
      privOrigin = arrayReg.includes(Origin);
      if((regiones==""|| privOrigin==true || nom==nombre)&&permisoedit==1){
        document.getElementById('btnGuardar').style.display = 'block';
      }
      else{
        document.getElementById('btnGuardar').style.display = 'none';
      }

      $("#zone2").change(function () {
        var pais2 = $(this).val();
        origin(pais2);
        origin2();
      }); 

      $("#Crop_to").append('<option class="form-control selected">' +CropTo+ '</option>');
      $('#Origin2').append('<option class="form-control selected">' +Origin2+ '</option>');
      $("#Col_Max").val(ColMax);

      $("#Mic_Cat").change(function () {
        var colmax = $(this).val();
        miccat_select(colmax)
      }); 

      $("#Origin2").change(function () {
        var Origin2 = $(this).val();   
        color(Origin2)
        
      });

      $("#Basis").keyup(function(evt) {
        //validar que solo sean numeros enteros y decimales
        var self = $(this);
        self.val(self.val().replace(/[^0-9\.-]/g, ''));
        if ((evt.which != 46 || self.val().indexOf('.') != -1) && (evt.which < 48 || evt.which > 57 || evt.which == 45)) 
        {
          evt.preventDefault();
        }
        //validamos solo 2 digito despues del punto decimal
        if($(this).val().indexOf('.')!=-1){         
          if($(this).val().split(".")[1].length > 2){                
              if( isNaN( parseFloat( this.value ) ) ) return;
              this.value = parseFloat(this.value).toFixed(2);
          }  
        }  
      });
    }); 

    //enviar form a bd
    $('#form_competition').submit(function(e){
      e.preventDefault();  
      Dateinq=$("#Date").val();
      zoneorigen=$("#zone2").val();
      Origin=$("#Origin").val();
      Origin2=$("#Origin2").val();
      Quantity=$("#Quantity").val();
      CropFrom=$("#Crop_from").val();
      CropTo=$("#Crop_to").val();
      Comments=$("#Comments").val();
      ColMax=$("#Col_Max").val();
      LenMin=$("#Len_Min").val();
      MicMin=$("#Mic_Min").val();
      MicMax=$("#Mic_Max").val();
      StrMin=$("#Str_Min").val();
      QualityComment=$("#Quality_Comment").val();
      Basis=$("#Basis").val();
      CoverMonth=$("#Cover_Month").val();
      Usuario=$("#usuario").val();
      zonedest=$("#zone1").val();
      if(opcion == 5){
        $.ajax({
          url: "bd/CrudBasisJM.php",
          type: "POST",
          datatype:"json",
          data:  {
            opcion:opcion,
            Date:Dateinq,
            zoneorigen:zoneorigen,
            Origin:Origin,
            Origin2:Origin2,
            Quantity:Quantity,
            CropFrom:CropFrom,
            CropTo:CropTo,
            Comments:Comments,
            ColMax:ColMax,
            LenMin:LenMin,
            MicMin:MicMin,
            MicMax:MicMax,
            StrMin:StrMin,
            QualityComment:QualityComment,
            Basis:Basis,
            CoverMonth:CoverMonth,
            Usuario:Usuario,                      
          },    
          success: function(data){     
            console.log(data);    
            alert("Successful registration");
            table_competition.ajax.reload(null,false);
          },
          error: function(data) {
            alert('error');
          }
        });
      }
      else{
        $.ajax({
          url: "bd/CrudBasisJM.php",
          type: "POST",
          datatype:"json",
          data:  {
            opcion:opcion,
            IDReq:IDReq,
            Date:Dateinq,
            zoneorigen:zoneorigen,
            Origin:Origin,
            Origin2:Origin2,
            Quantity:Quantity,
            CropFrom:CropFrom,
            CropTo:CropTo,
            Comments:Comments,
            ColMax:ColMax,
            LenMin:LenMin,
            MicMin:MicMin,
            MicMax:MicMax,
            StrMin:StrMin,
            QualityComment:QualityComment,
            Basis:Basis,
            CoverMonth:CoverMonth,
            Usuario:Usuario,  
          },    
          success: function(data){         
            alert("Update Successes");
            table_competition.ajax.reload(null,false);
          },
          error: function(data) {
            alert('error');
          }
        });
      }
      $('#modalCompetition').modal('hide');
    }); 
});



//***********************************FILTROS PARA TABLA DE BASIS JM********************************************* 
var fechainicio = "",fechafin="",rango=0;
$(document).ready(function() {
 
    $("#filtraboton").click(function(){
        $(".modal-header").css( "background-color", "#17562c");
        $(".modal-header").css( "color", "white" );
        $('#filtrarmodal').modal('show');
        $('#OriginFiltros').empty();
        if ($('#RegionOrigen option').length === 0) {
        regionesorigen();
        }

        ///validar si hay un cambie en la fecha para capturar los valores
        if(fechainicio =="" && fechafin =="" && rango==0){
        fechadefault();
        }
        else{
          $("#timeSelect").val(rango);
          $("#fromdate").val(fechainicio);
          $("#todate").val(fechafin);
          
        }

        $("#RegionOrigen").change(function () {        
          paisoriginfiltro($(this).val());
        });
        $("#timeSelect").change(function () {        
          selectorfecha();
        });


        if ($('#userlist option').length === 0) {
        userflitro();
        }

        $("#timeSelect").change(function () {        
          rango=$(this).val();
          fechainicio = $("#fromdate").val();
          fechafin= $("#todate").val();
        });
        $("#fromdate").change(function () {        
          fechainicio= $(this).val();
          fechafin= $("#todate").val();
        });
        $("#todate").change(function () { 
          fechainicio = $("#fromdate").val();       
          fechafin= $(this).val();
        });
        
    });

    var botonglobal = 0;

    $("#buscafiltro, #borrarFiltro, #cer").click(function(){
      boton = $(this).val();
      if(boton == 1 || boton == 0)
          botonglobal = boton;
      if(boton != 3){
          FromDate = $("#fromdate").val();  
          ToDate = $("#todate").val();  
          OriginReg = $("#RegionOrigen").val();  
          OriginCount= $("#OriginFiltros").val();
          UserComp= $("#userlist").val();

          var applyFilter =  $.ajax({
            type: 'POST',
            url: 'bd/FiltrosBasisJM.php',
            data: {
                boton:boton,
                FromDate: FromDate,
                ToDate:ToDate,
                OriginReg: OriginReg,
                OriginCount: OriginCount,
                UserComp: UserComp
            }
          })
          
          applyFilter.done(function(data){
            table_competition.ajax.reload(function(){
              if(boton == 1){
                $('#aviso').html('<div id="alert1" class="alert alert-success" style="width: 16rem; height: 3rem;" role="alert"><i class="bi bi-check-circle-fill"></i>Filters Applied</div>')
                setTimeout(function() {
                    $('#alert1').fadeOut(1000);
                    $('#filtrarmodal').modal('hide');
                },1000);
              }
              else{
                // resetear filtros 
                $("#filtros").trigger("reset");
                $('#OriginFiltros').empty();
                regionesorigen();
                fechadefault();
                userflitro();
                rango=0;
                fechainicio = "";
                fechafin= "";
                $('#aviso').html('<div id="alert1" class="alert alert-success" style="width: 16rem; height: 3rem;" role="alert"> <i class="bi bi-trash-fill"></i>Clean Filters</div>')
                setTimeout(function() {
                    $('#alert1').fadeOut(1500);
                },1500);
              }
    
            });
            $('#aviso').html('<div class="spinner-border" style="width: 2rem; height: 2rem;" role="status"><span class="visually-hidden">Loading...</span> </div> <div class="spinner-grow" style="width: 2rem; height: 2rem;" role="status"><span class="visually-hidden">Loading...</span> </div>');  
          })
          applyFilter.error(function(data){
            $('#aviso').html("");
          })
      }else{
        $("#filtros").trigger("reset");
        rango=0;
        fechainicio = "";
        fechafin= "";
      }

    });
}); 

  function years_modal(){
    $("#Crop_from").empty();
    date = new Date();
    year = date.getFullYear();
    diferencia = (year+7) - 2021
   
    $('#Crop_from').append($('<option>').val("").text("Choose..."));  
    for (var i = 0; i<= diferencia; i++){ 
        $('#Crop_from').append($('<option>').val(2021+i).text(2021+i));     
    }
  }

  function years_modalEdit(CropFrom){
    $("#Crop_from").empty();
    date = new Date();
    year = date.getFullYear();
    diferencia = (year+7) - 2021
    if(CropFrom!=""){
      $('#Crop_from').append($('<option>').val(CropFrom).text(CropFrom)); 

    }
    else{
      $('#Crop_from').append($('<option>').val("").text("Choose..."));  

    }
    for (var i = 0; i<= diferencia; i++){ 
        $('#Crop_from').append($('<option>').val(2021+i).text(2021+i));     
    }
  }


function years_modal_to(paisorigen){   
  $('#Crop_to').prop('disabled', true);
  $("#Crop_to").empty();
  date = new Date();
  year = date.getFullYear();
  $("#Crop_from").change(function () { 
      year_from = parseInt($("#Crop_from").val());
      paisorigin = $("#Origin").val();

      if(paisorigin =="AU" || paisorigin =="BR"){
              $("#Crop_to").empty();
            $('#Crop_to').prop('disabled', false);
            for (var i = year_from; i<= year+8; i++){ 
              $('#Crop_to').append($('<option>').val(i).text(i)); 
            }
            $("select#Crop_to").val(year_from);

      } 
      else{     
          if ( year_from !=""){
            year_from = year_from
            $("#Crop_to").empty();
            $('#Crop_to').prop('disabled', false);
            for (var i = year_from; i<= year+8; i++){ 
              $('#Crop_to').append($('<option>').val(i).text(i)); 
            }

            $("select#Crop_to").val(year_from+1);
          }
          else{
            $("#Crop_to").empty();
            $('#Crop_to').prop('disabled', true);
          }
      }  

}); 
}
  function origin(pais,origindelect){    
        idOrigin="";
        
        $('#Origin').prop('disabled', false);
          $.ajax({
            url: "bd/CrudBasisJM.php",
            type: "POST",
            datatype:"json",
            data:  {opcion:1,pais:pais },    
            success: function(data){    
                opts = JSON.parse(data);
                  $('#Origin').empty();
                  $('#Origin').append($('<option>').val("").text("Choose..."));
                  for (var i = 0; i< opts.length; i++){ 
                      $('#Origin').append($('<option>').val(opts[i].CountryCode).text(opts[i].CountryName));  
                      if (origindelect!="" && origindelect==opts[i].CountryName){
                        idOrigin = opts[i].CountryCode
                      }               
                  } 

                  $("select#Origin").val(idOrigin);
                  
                 
            },
            error: function(data) {
                alert('error');
            }
        });

     
  }

  function origin2(valor){
    idgin2="";
    $('#Origin2').prop('disabled', true);
    $("#Origin2").empty();
    $("#Origin").change(function (){ 
      origen = $("#Origin").val();
      if(origen !=""){  
        $("#Origin2").empty();     
          $.ajax({
            url: "bd/CrudBasisJM.php",
            type: "POST",
            datatype:"json",
            data:  {opcion:4,origen:origen },    
            success: function(data){   
                opts = JSON.parse(data);
                if(opts.length > 0){
                  $("#Origin2").empty();   
                  $('#Origin2').prop('disabled', false);
                  $('#Origin2').append($('<option>').val("").text("Choose..."));
                  for (var i = 0; i< opts.length; i++){ 
                      $('#Origin2').append($('<option>').val(opts[i].Zone).text(opts[i].Zone));  
                  }
                }
                else{
                  $("#Origin2").empty();
                  $('#Origin2').prop('disabled', true);
                }   
                
                $("select#Origin2").val(valor);
            },
            error: function(data) {
                alert('error');
            }
          });
      }
      else{
        $('#Origin2').prop('disabled', true);
        $("#Origin2").empty();   
      }
    });

  }

  function miccat_select(colmax){     
      //console.log(colmax);
      switch (colmax) {
        case 'G7':
          $("#Mic_Min").val(5.3)
          $("#Mic_Max").val(10)        
          
        break;
        case 'G6':
          $("#Mic_Min").val(5)
          $("#Mic_Max").val(5.2)        
          
        break;
        case 'G5':
          $("#Mic_Min").val(3.5)
          $("#Mic_Max").val(4.9)        
          
        break;
        case 'G4':
          $("#Mic_Min").val(3.3)
          $("#Mic_Max").val(3.4)        
          
        break;
        case 'G3':
          $("#Mic_Min").val(3)
          $("#Mic_Max").val(3.2)        
          
        break;
        case 'G2':
          $("#Mic_Min").val(2.7)
          $("#Mic_Max").val(2.9)        
          
        break;
        case 'G1':
          $("#Mic_Min").val(2.5)
          $("#Mic_Max").val(2.6)        
          
        break;
        case 'G0':
          $("#Mic_Min").val(0)
          $("#Mic_Max").val(2.4)        
          
        break;
        }
  }

  function zones_origen(valor){
    $("#zone2").empty();

    var group1 = $('<optgroup class="optgroups" label="Africa">');
    var group2 = $('<optgroup class="optgroups" label="America">');
    var group3 = $('<optgroup class="optgroups" label="Asia">');
    var group4 = $('<optgroup class="optgroups" label="Europe">');
    var group5 = $('<optgroup class="optgroups" label="Oceania">');

    $.ajax({
      url: "bd/CrudBasisJM.php",
      type: "POST",
      datatype:"json",
      data:  {opcion:14},    
      success: function(data){
        opts = JSON.parse(data); 
        console.log(opts)     
        $('#zone2').append($('<option>').val("").text("Choose..."));
            for (var i = 0; i< opts.length; i++){
                switch (opts[i].Region) {
                  case 'Africa':
                    $("#zone2").append(group1);
                    group1.append($('<option></option>').val(opts[i].SubRegion).html(opts[i].SubRegion));
                    break;
                  case 'America':
                    $("#zone2").append(group2);
                    group2.append($('<option></option>').val(opts[i].SubRegion).html(opts[i].SubRegion));
                    break;  
                  case 'Asia':
                    $("#zone2").append(group3);
                    group3.append($('<option></option>').val(opts[i].SubRegion).html(opts[i].SubRegion));
                    break;  
                  case 'Europe':
                    $("#zone2").append(group4);
                    group4.append($('<option></option>').val(opts[i].SubRegion).html(opts[i].SubRegion));
                    break;   
                  case 'Oceania':
                    $("#zone2").append(group5);
                    group5.append($('<option></option>').val(opts[i].SubRegion).html(opts[i].SubRegion));
                    break; 
                }   
            }     
            
            if(valor!=""){
              $("select#zone2").val(valor);
            }
     
      },
      error: function(data) {
          alert('error');
      }
    });
  }


  function covmot(){
    val="";
    $.ajax({
      url: "bd/CrudBasisJM.php",
      type: "POST",
      datatype:"json",
      data:  {opcion:13},    
      success: function(data){  
          $("#Cover_Month").empty();       
          $('#Cover_Month').append($('<option>').val("").text("Choose..."));
          opts = JSON.parse(data);
          for (var i = 0; i< opts.length; i++){
            $('#Cover_Month').append($('<option>').val(opts[i].CovMon).text(opts[i].CovMon)); 
            if (opts[i].DefaultSelect == 1){  
                val= opts[i].CovMon;
             }
          }

          $("select#Cover_Month").val(val);
                  
      },
      error: function(data) {
          alert('error');
      }
  });


  }
  
  
function Editcovmot(valor){
    $.ajax({
      url: "bd/CrudBasisJM.php",
      type: "POST",
      datatype:"json",
      data:  {opcion:13},    
      success: function(data){  
          $("#Cover_Month").empty();       
          $('#Cover_Month').append($('<option>').val("").text("Choose..."));
          opts = JSON.parse(data);
          for (var i = 0; i< opts.length; i++){
            $('#Cover_Month').append($('<option>').val(opts[i].CovMon).text(opts[i].CovMon)); 
          }
          $("select#Cover_Month").val(valor);
      },
      error: function(data) {
          alert('error');
      }
    });


}

function color(origen2){
  if (origen2==4 || origen2=="PIMA" ){
    $("#Col_Max").empty();  
    $('#Col_Max').append($('<option>').val("0").text("Choose..."));
    $('#Col_Max').append($('<option>').val(1).text(1)); 
    $('#Col_Max').append($('<option>').val(2).text(2)); 
    $('#Col_Max').append($('<option>').val(3).text(3)); 
    $('#Col_Max').append($('<option>').val(4).text(4)); 
    $('#Col_Max').append($('<option>').val(5).text(5)); 
    $('#Col_Max').append($('<option>').val(6).text(6)); 
  }
  else{
    $("#Col_Max").empty(); 
    $('#Col_Max').append($('<option>').val("0").text("Choose..."));
    var $group1 = $('<optgroup class="optgroups" label="White">');
    var $group2 = $('<optgroup class="optgroups" label="Light Spotted">');
    var $group3 = $('<optgroup class="optgroups" label="Spotted">');

    $("#Col_Max").append($group1);
    $group1.append($('<option></option>').val(11).html(11));
    $group1.append($('<option></option>').val(21).html(21));
    $group1.append($('<option></option>').val(31).html(31));
    $group1.append($('<option></option>').val(41).html(41));
    $group1.append($('<option></option>').val(51).html(51));
    $group1.append($('<option></option>').val(61).html(61));
    $group1.append($('<option></option>').val(71).html(71));
    $group1.append($('<option></option>').val(81).html(81));
    $("#Col_Max").append($group2);
    $group2.append($('<option></option>').val(12).html(12));
    $group2.append($('<option></option>').val(22).html(22));
    $group2.append($('<option></option>').val(32).html(32));
    $group2.append($('<option></option>').val(42).html(42));
    $group2.append($('<option></option>').val(52).html(52));
    $group2.append($('<option></option>').val(62).html(62));
    $group2.append($('<option></option>').val(82).html(82));  
    $("#Col_Max").append($group3);
    $group3.append($('<option></option>').val(13).html(13));
    $group3.append($('<option></option>').val(23).html(23));
    $group3.append($('<option></option>').val(24).html(24));
    $group3.append($('<option></option>').val(33).html(33));
    $group3.append($('<option></option>').val(34).html(34));
    $group3.append($('<option></option>').val(35).html(35));
    $group3.append($('<option></option>').val(43).html(43));
    $group3.append($('<option></option>').val(44).html(44));
    $group3.append($('<option></option>').val(53).html(53));
    $group3.append($('<option></option>').val(54).html(54));
    $group3.append($('<option></option>').val(63).html(63));
    $group3.append($('<option></option>').val(83).html(83));
    $group3.append($('<option></option>').val(84).html(84));
    $group3.append($('<option></option>').val(85).html(85));
  }
}

function valdate(dat1,dat2){
  var f1 = new Date(dat1);
  var f2 = new Date(dat2);
  if(f1>f2){
    document.getElementById('alerta_fecha').style.display = 'block';
  }
  else{
    document.getElementById('alerta_fecha').style.display = 'none';
  }
}

function validamic(micmin,micmax){
  if(parseFloat(micmin) > parseFloat(micmax)){
    document.getElementById('alerta_calidad').style.display = 'block';
  }
  else{
  document.getElementById('alerta_calidad').style.display = 'none';
  }
}

function validaCantidad(cantidad){
  if (cantidad <=0){
    document.getElementById('alerta_cantidad').style.display = 'block';    
  }
  else{
    document.getElementById('alerta_cantidad').style.display = 'none';
  }
}

//funciones para el modal de filtro
function regiondestfiltro(){
  $("#RegionDest").empty();
  var group1 = $('<optgroup class="optgroups" label="Africa">');
  var group2 = $('<optgroup class="optgroups" label="America">');
  var group3 = $('<optgroup class="optgroups" label="Asia">');
  var group4 = $('<optgroup class="optgroups" label="Europe">');
  var group5 = $('<optgroup class="optgroups" label="Oceania">');
  $.ajax({
    url: "bd/CrudCompetition.php",
    type: "POST",
    datatype:"json",
    data:  {opcion:12},    
    success: function(data){
      opts = JSON.parse(data);
      $('#RegionDest').append($('<option>').val("").text("Choose..."));    
      
      for (var i = 0; i< opts.length; i++){ 

          switch (opts[i].Region) {
            case 'Africa':
              $("#RegionDest").append(group1);
              group1.append($('<option></option>').val(opts[i].SubRegion).html(opts[i].SubRegion));
              break;
            case 'America':
              $("#RegionDest").append(group2);
              group2.append($('<option></option>').val(opts[i].SubRegion).html(opts[i].SubRegion));
              break;  
            case 'Asia':
              $("#RegionDest").append(group3);
              group3.append($('<option></option>').val(opts[i].SubRegion).html(opts[i].SubRegion));
              break;  
            case 'Europe':
              $("#RegionDest").append(group4);
              group4.append($('<option></option>').val(opts[i].SubRegion).html(opts[i].SubRegion));
              break;   
            case 'Oceania':
              $("#RegionDest").append(group5);
              group5.append($('<option></option>').val(opts[i].SubRegion).html(opts[i].SubRegion));
              break; 
          }   
        
      }
    },
    error: function(data) {
        alert('error');
    }
  });
}

function regionesorigen(){
  $("#RegionOrigen").empty();

  var group1 = $('<optgroup class="optgroups" label="Africa">');
  var group2 = $('<optgroup class="optgroups" label="America">');
  var group3 = $('<optgroup class="optgroups" label="Asia">');
  var group4 = $('<optgroup class="optgroups" label="Europe">');
  var group5 = $('<optgroup class="optgroups" label="Oceania">');

  $.ajax({
    url: "bd/CrudBasisJM.php",
    type: "POST",
    datatype:"json",
    data:  {opcion:14},    
    success: function(data){
      opts = JSON.parse(data);       
      $('#RegionOrigen').append($('<option>').val("").text("Choose..."));
          for (var i = 0; i< opts.length; i++){
              switch (opts[i].Region) {
                case 'Africa':
                  $("#RegionOrigen").append(group1);
                  group1.append($('<option></option>').val(opts[i].SubRegion).html(opts[i].SubRegion));
                  break;
                case 'America':
                  $("#RegionOrigen").append(group2);
                  group2.append($('<option></option>').val(opts[i].SubRegion).html(opts[i].SubRegion));
                  break;  
                case 'Asia':
                  $("#RegionOrigen").append(group3);
                  group3.append($('<option></option>').val(opts[i].SubRegion).html(opts[i].SubRegion));
                  break;  
                case 'Europe':
                  $("#RegionOrigen").append(group4);
                  group4.append($('<option></option>').val(opts[i].SubRegion).html(opts[i].SubRegion));
                  break;   
                case 'Oceania':
                  $("#RegionOrigen").append(group5);
                  group5.append($('<option></option>').val(opts[i].SubRegion).html(opts[i].SubRegion));
                  break; 
              }   
          }
    },
    error: function(data) {
        alert('error');
    }
  });
}

function paisoriginfiltro(pais){    
    $.ajax({
      url: "bd/CrudBasisJM.php",
      type: "POST",
      datatype:"json",
      data:  {opcion:1,pais:pais },    
      success: function(data){    
          opts = JSON.parse(data);
            $('#OriginFiltros').empty();
            $('#OriginFiltros').append($('<option>').val("").text("Choose..."));
            for (var i = 0; i< opts.length; i++){ 
                $('#OriginFiltros').append($('<option>').val(opts[i].CountryCode).text(opts[i].CountryName));    
            } 
      },
      error: function(data) {
          alert('error');
      }
  });
}

function fechadefault(){
  var date=new Date();  
  date.setDate(date.getDate());
  var day=String(date.getDate()).padStart(2, '0');          
  var month=("0" + (date.getMonth() + 1)).slice(-2); 
  var year=date.getFullYear();  
  fechadef=year+"-"+month+"-"+day 

  var date2=new Date();  
  date2.setDate(date2.getDate() - 30);
  var day2=String(date2.getDate()).padStart(2, '0');          
  var month2=("0" + (date2.getMonth() + 1)).slice(-2); 
  var year2=date2.getFullYear();  
  fechadef2=year2+"-"+month2+"-"+day2 ;

  $('#fromdate').val( fechadef2);
  $('#todate').val(fechadef);
  $('#timeSelect').val(30);
}

function selectorfecha(){
    timeSelect = $('select[name=timeSelect] option').filter(':selected').val();
    if(timeSelect){
    timeSelect = parseInt(timeSelect);
    var today = new Date();

    let dateEnMilisegundos = 1000 * 60* 60* 24* timeSelect;
    let suma = today.getTime() - dateEnMilisegundos;
    let fechainicial = new Date(suma);
    datetoday =  formatDate(today);
    datetosearch = formatDate(fechainicial);
    $('#fromdate').val(datetosearch);
    $('#todate').val(datetoday);
    }else{
      fechadefault();
    }
}

function formatDate(date) {
  var d = new Date(date),
      month = '' + (d.getMonth() + 1),
      day = '' + d.getDate(),
      year = d.getFullYear();

  if (month.length < 2) 
      month = '0' + month;
  if (day.length < 2) 
      day = '0' + day;

  return [year, month, day].join('-');
}

function userflitro(){
  $('#userlist').empty();
  $.ajax({
    url: "bd/CrudBasisJM.php",
    type: "POST",
    datatype:"json",
    data:  {opcion:15},    
    success: function(data){    
        opts = JSON.parse(data);
          $('#userlist').empty();
          $('#userlist').append($('<option>').val("").text("Choose..."));
          for (var i = 0; i< opts.length; i++){ 
              $('#userlist').append($('<option>').val(opts[i].User).text(opts[i].Name));  
                    
          } 
    },
    error: function(data) {
        alert('error');
    }
  });
}




