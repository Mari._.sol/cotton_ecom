<?php
 session_start();
if (!isset($_SESSION['user_inquiries'])) :
    include_once('index.php');
else :
   
    $Gin = $_SESSION['gin_user'];
    $usuario=$_SESSION['user_inquiries']
?>
    <!doctype html>
    <html lang="es en">

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="shortcut icon" href="img/ecom.png" />
        <title>PURCHASES</title>

        <!-- CSS bootstrap -->
        <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
        <!--datables CSS básico-->
        <link rel="stylesheet" type="text/css" href="assets/datatables/datatables.min.css" />
        <!--datables estilo bootstrap 4 CSS-->
        <link rel="stylesheet" type="text/css" href="assets/datatables/DataTables-1.10.18/css/dataTables.bootstrap4.min.css">

        <!-- CSS personalizado -->
        <link rel="stylesheet" href="main.css">
        
        
        
        <!--Google fonts -->
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Inter:wght@300&family=Padauk&display=swap" rel="stylesheet">

        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      

        <!--font awesome con CDN  -->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.1/font/bootstrap-icons.css">
    </head>

    <body>

        <!-- Scripts -->

        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script>
   

        <!-- jQuery, Popper.js, Bootstrap JS -->
        <script src="assets/jquery/jquery-3.3.1.min.js"></script>
        <script src="assets/popper/popper.min.js"></script>
        <script src="assets/bootstrap/js/bootstrap.min.js"></script>

         <!-- librerias necesarias para finalizar sesion por inactividad -->
         <script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script>
        <!-- Scrip para finalizar sesion por inactividad -->
        <script type="text/javascript" src="timer.js"></script>



        <!-- datatables JS -->
        <script type="text/javascript" src="assets/datatables/datatables.min.js"></script>
        

        <!-- para usar botones en datatables JS -->
        <script src="datatables/Buttons-1.5.6/js/dataTables.buttons.min.js"></script>
        <script src="datatables/JSZip-2.5.0/jszip.min.js"></script>
        <script src="datatables/pdfmake-0.1.36/pdfmake.min.js"></script>
        <script src="datatables/pdfmake-0.1.36/vfs_fonts.js"></script>
        <script src="datatables/Buttons-1.5.6/js/buttons.html5.min.js"></script>
        
        <!-- Ficed columns -->
        <script src="https://cdn.datatables.net/fixedcolumns/4.0.0/js/dataTables.fixedColumns.min.js"></script>

        <!-- Scrip para formularios y vistas 
        <script type="text/javascript" src="purchases.js"></script>
        -->
        
      

        <!-- Terminan Scripts -->
            
        <!-- Esto es el Nav bar, todo contenido en un container-fluid -->
        <nav class="navbar navbar-expand-lg bg-transparent navbar-custom">
            <div class="container-fluid" style="padding-right: 1.5rem; padding-left: 1.5rem;">
                <div class="dropdown  nav-item col-xl-1 col-lg-1 col-md-2 col-sm-2 col-xs-1">
                    <a class="btn dropdown-toggle " href="#" role="button" id="dropdownMenuLink" data-bs-toggle="dropdown" aria-expanded="false">
                        <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" fill="white" class="bi bi-list" viewBox="0 0 16 16">
                            <path fill-rule="evenodd" d="M2.5 12a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5z" />
                        </svg>
                    </a>
              
                    <ul class="dropdown-menu " aria-labelledby="dropdownMenuLink">
                        <li><a class="dropdown-item" style="background-color: #5a926d;" href="purchases.php">Purchases</a></li>
                        <li><a class="dropdown-item" href="doproveed.php">Delivery Order</a></li>
                    
                     
                    </ul>


             <div id="gin" style="display:none;">
                     <input id="GinID" value="<?php echo $Gin; ?>"/>
                     <input id="usuario" value="<?php echo $usuario; ?>"/>

               </div> 


                </div>
                <a class="navbar-brand nav-item col-xl-1 col-lg-1 col-md-1 col-sm-2 col-xs-2" href="doproveed.php">
                    <div class="logos"><img class="log" src="img/logo1.png"> AMSA</div>
                </a>
                <div class="col-xl-1 col-lg-1 col-md-1 col-sm-1 col-xs-1 tex">
                    <p> PURCHASES </p>
                </div>
                <div class="container-fluid  col-xl-7 col-lg-6 col-md-3 col-sm-2 col-xs-1">
                    <!-- Botones -->
                    

                </div>
                <div class="dropdown nav-item col-xl-2 col-lg-3 col-md-3 col-sm-3 col-xs-1 d-flex justify-content-end" style="padding-right: 1.5rem;">
                <button style="color:white;" class="btn dropdown-toggle btn-outline-success" data-bs-toggle="dropdown" aria-expanded="false" id="dropdownMenuLink2"  value="<?php echo $_SESSION['proveed']; ?>"><?php echo $_SESSION['proveed']; ?></button>
                    <ul class="dropdown-menu" aria-labelledby="dropdownMenuLink2">
                        <li><a class="dropdown-item" href="logout.php">Cerrar sesión</a></li>
                    </ul>
                </div>
            </div>
        </nav>
            <!-- Estilo para inmovilizar titulo de tabla secundaria -->
        
            <!-- Estilo para cambiar el color del search de Datatabla -->
            <style>
            .dataTables_wrapper 
            .dataTables_length, div.dataTables_wrapper 
            div.dataTables_filter label, 
            div.dataTables_wrapper div.dataTables_info {
            color: white;
                
            }
            </style>
                      
        <!-- Esto es el Nav bar, todo contenido en un container-fluid -->

        <!-- Aquí inicia todo código de tablas etc -->
        <!-- Inicio de la tabla -->
      
        <div class="card card-body " style="opacity:100%;" >      
        
        <div class="col-xs-3">
        <div class="input-group mb-1">
        <label class="input-group-text" >LiqID</label>
        <label class="input-group-text"  id="totaLiqID" >0</label>
        <label class="input-group-text" >Total Bales</label>
        <label class="input-group-text"  id="bales" >0</label>
   
        </div>
        </div>    
        <div class="table-responsive" style="opacity:100%;">            
            <table id="tablaPurchases"  class="table bg-white table-striped row-border order-column table-hover " style="opacity:100%;">
                     
                    <thead style="background-color: #65ac7c;" style="opacity:100%;">
                   
                            <tr>                       
                                <th ></th>
                                <th class="th-sm">LiqID</th>
                                <th class="th-sm">Bales</th>       
                                 
                            
                        
                            </tr>
                    </thead>
                          
                      
                    <tbody>
                       

                        
                    </tbody>
                    
                      

                </table>
                   
                
            </div>

    
        </div>
          


     
       
       


    </body>

    </html>
<?php
endif;
?>
