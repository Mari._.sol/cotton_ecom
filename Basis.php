<?php
 session_start();
 header("Cache-Control: no-cache, must-revalidate"); //para borrar cache
if (!isset($_SESSION['user_inquiries'])) :
    include_once('index.php');
else :
    include_once('bd/permisos.php');
   
    $region = $_SESSION['Region'];
    $usuario=$_SESSION['user_inquiries'];
    $priv = $_SESSION['Priv'];
    $vistaComp=$_SESSION['ViewCompetition'];
    $vistaInq= $_SESSION['ViewInquirie'];
    $vistabuyer= $_SESSION['ViewBuyer'];
    $CostToLand = $_SESSION['CostToLand'];
    $basis = $_SESSION['basis'];
    $MailBasis =$_SESSION['MailBasis']; 
    $permisos_basis= getEditBasis($usuario);
    $editbasis=$permisos_basis[0]['EditBasis'];
    $editcov=$permisos_basis[0]['EditCovBasis'];
    $mes_cov=mes_covertura();

    $m1=$mes_cov[0]['CovMon'];
    $m2=$mes_cov[1]['CovMon'];
    $m3=$mes_cov[2]['CovMon'];
    $m4=$mes_cov[3]['CovMon'];
    
    if ($basis == 0){
        header("Location:index.php"); 
    }
?>
    <!doctype html>
    <html lang="en-US">

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="shortcut icon" href="img/ecom.png" />
        <title>Origin Basis</title>

        <!-- CSS bootstrap -->
        <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
        <!--datables CSS básico-->
        <link rel="stylesheet" type="text/css" href="assets/datatables/datatables.min.css" />
        <!--datables estilo bootstrap 4 CSS-->
        <link rel="stylesheet" type="text/css" href="assets/datatables/DataTables-1.10.18/css/dataTables.bootstrap4.min.css">

        <!-- CSS personalizado -->
        <link rel="stylesheet" href="main.css">
        
        
        
        <!--Google fonts -->
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Inter:wght@300&family=Padauk&display=swap" rel="stylesheet">

        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      

        <!--font awesome con CDN  -->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.1/font/bootstrap-icons.css">
    </head>

    <body>

        <!-- Scripts -->

        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script>
   

        <!-- jQuery, Popper.js, Bootstrap JS -->
        <script src="assets/jquery/jquery-3.3.1.min.js"></script>
        <script src="assets/popper/popper.min.js"></script>
        <script src="assets/bootstrap/js/bootstrap.min.js"></script>

         <!-- librerias necesarias para finalizar sesion por inactividad -->
         <script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script>
        <!-- Scrip para finalizar sesion por inactividad -->
        <script type="text/javascript" src="timer.js"></script>



        <!-- datatables JS -->
        <script type="text/javascript" src="assets/datatables/datatables.min.js"></script>
        

        <!-- para usar botones en datatables JS -->
        <script src="datatables/Buttons-1.5.6/js/dataTables.buttons.min.js"></script>
        <script src="datatables/JSZip-2.5.0/jszip.min.js"></script>
        <script src="datatables/pdfmake-0.1.36/pdfmake.min.js"></script>
        <script src="datatables/pdfmake-0.1.36/vfs_fonts.js"></script>
        <script src="datatables/Buttons-1.5.6/js/buttons.html5.min.js"></script>
        
        <!-- Ficed columns -->
        <script src="https://cdn.datatables.net/fixedcolumns/4.0.0/js/dataTables.fixedColumns.min.js"></script>

        
         <script type="text/javascript" src="Basis.js?v=<?php echo time(); ?>"></script>   
      
        
      

        <!-- Terminan Scripts -->
            
        <!-- Esto es el Nav bar, todo contenido en un container-fluid -->
        <nav class="navbar navbar-expand-lg bg-transparent navbar-custom">
            <div class="container-fluid" style="padding-right: 1.5rem; padding-left: 1.5rem;">
                <div class="dropdown  nav-item col-xl-1 col-lg-1 col-md-2 col-sm-2 col-xs-1">
                    <a class="btn dropdown-toggle " href="#" role="button" id="dropdownMenuLink" data-bs-toggle="dropdown" aria-expanded="false">
                        <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" fill="white" class="bi bi-list" viewBox="0 0 16 16">
                            <path fill-rule="evenodd" d="M2.5 12a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5z" />
                        </svg>
                    </a>
                    
                    <ul class="dropdown-menu " aria-labelledby="dropdownMenuLink">
                        <?php if ($vistaInq == 1){?>      
                            <li><a class="dropdown-item" href="inquirie.php">Inquiries</a></li>
                        <?php } ?> 
                        <?php if ($vistaComp == 1){?>           
                        <li><a class="dropdown-item" href="competition.php">Competition Offers</a></li>    
                        <?php } ?>
                        <?php if ($CostToLand == 1){?>                            
                        <li><a class="dropdown-item" href="CostToLand.php">Cost to Land</a></li> 
                        <?php } ?>
                        <?php if ($vistabuyer == 1){?>           
                            <li><a class="dropdown-item" href="buyers.php">Buyers</a></li>    
                        <?php } ?>                         
                       <li><a class="dropdown-item" style="background-color: #5a926d;" href="Basis.php">Origin Basis</a></li>
                    </ul>
                   


                <div id="gin" style="display:none;">
                     <input id="editar" value="<?php echo $editbasis; ?>"/>
                     <input id="usuario" value="<?php echo $usuario; ?>"/>
                     <input id="mes1" value="<?php echo $m1; ?>"/>
                     <input id="mes2" value="<?php echo $m2; ?>"/>
                     <input id="mes3" value="<?php echo $m3; ?>"/>
                     <input id="mes4" value="<?php echo $m4; ?>"/>
                </div> 


                </div>
                <a class="navbar-brand nav-item col-xl-1 col-lg-1 col-md-1 col-sm-2 col-xs-2" href="main.php">
                    <div class="logos"><img class="log" src="img/logo1.png">  ECOM/ Origin Basis</div>
                </a>
                <div class="col-xl-1 col-lg-1 col-md-1 col-sm-1 col-xs-1 tex">
                    <!--<p> Origin Basis </p> -->
                </div>
                <div class="container-fluid  col-xl-7 col-lg-6 col-md-3 col-sm-2 col-xs-1">
                <!-- Botones -->
                

                
                <?php if ($editcov == 1){?>      
                <button id="editar_mes" type="button" class="btn btn-dark" data-toggle="modal tooltip" data-placement="bottom" title="Edit Cover Month"><i class="bi bi-calendar2-month"></i></button>  
                <?php } ?>                
               
               
                <button id="exportar_basis" type="button" class="btn btn-success" data-toggle="modal tooltip"  data-placement="bottom" title="Export to Excel"><i class="bi bi-file-earmark-excel"></i></button>   
                
                 <?php if ($MailBasis == 1){?>      
                  <button id="PsendEmail" type="button" class="btn btn-primary" data-toggle="modal tooltip" data-placement="bottom" title="Send Mail"><i class="bi bi-envelope"></i>
                <?php } ?>     
                
        

                </div>
                <div class="dropdown nav-item col-xl-2 col-lg-3 col-md-3 col-sm-3 col-xs-1 d-flex justify-content-end" style="padding-right: 1.5rem;">
                <button style="color:white;" class="btn dropdown-toggle btn-outline-success" data-bs-toggle="dropdown" aria-expanded="false" id="dropdownMenuLink2"  value="<?php echo $_SESSION['user_inquiries']; ?>"><?php echo $_SESSION['user_inquiries']; ?></button>
                    <ul class="dropdown-menu" aria-labelledby="dropdownMenuLink2">
                        <li><a class="dropdown-item" href="logout.php">Close sesion</a></li>
                    </ul>
                </div>
            </div>
        </nav>

        <style>
            #table_basis thead th {
                border: 1px solid black;
                text-align: center;
            }

            #table_basis tbody td {
              
                text-align: center;
                padding: 5px;
            }

            .sinborde {
             background-color:0;
             border: 0;
            }

        </style> 
                    
        
    <div class="card card-body " style="opacity:100%;" >   
        <div class="table-responsive" style="opacity:100%;">
        
            <table id="table_basis" class="table bg-white table-striped row-border order-column table-hover table table-bordered tablas"  style="opacity:100%;">                     
                    <thead style="background-color: #65ac7c;" style="opacity:100%;"> 
                        <tr>
                            <th  rowspan="2">Origin</th>
                            <th  colspan="4"><?php echo $m1; ?></th>
                            <th  colspan="4"><?php echo $m2; ?></th>
                            <th  colspan="4"><?php echo $m3; ?></th>
                            <th  colspan="4"><?php echo $m4; ?></th>
                            <th  rowspan="2" class="no-exportar">EDIT</th>
                        </tr>                  
                        <tr>      
                            
                            <th>RS-SM</th>
                            <th>RS-M</th>       
                            <th>RS-SLM</th> 
                            <th>OE</th>

                            <th>RS-SM</th>
                            <th>RS-M</th>       
                            <th>RS-SLM</th> 
                            <th>OE</th>

                            <th>RS-SM</th>
                            <th>RS-M</th>       
                            <th>RS-SLM</th> 
                            <th>OE</th>

                            <th>RS-SM</th>
                            <th>RS-M</th>       
                            <th>RS-SLM</th> 
                            <th>OE</th>
                                           
                                                      
                        </tr>
                    </thead>       
                    <tbody>
                        
                        
                    </tbody>               

                    
            </table>
                   
                
            </div>

    
        </div>


         <!--Modal-->

         <div class="modal hide fade in" data-bs-backdrop="static"  data-bs-keyboard="false" class="modal" id="modal_basis" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
         <?php
         //Mostrar los meses de covertura en modal para capturar
            $mes_cov_modal=mes_covertura();

            $mes1=$mes_cov_modal[0]['CovMon'];
            $mes2=$mes_cov_modal[1]['CovMon'];
            $mes3=$mes_cov_modal[2]['CovMon'];
            $mes4=$mes_cov_modal[3]['CovMon'];
        ?>

            <div class="modal-dialog modal-lg">
                <!--g" role="document">-->
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title"></h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" style="background-color: white;" aria-label="Close">
                        </button>
                    </div>
                    <form id="form_basis">
                        <div class="modal-body" id="InData">
                            <div class="row" style="display:none">
                                <div class="col-md-3">                                 
                                    <div class="input-group mb-3">
                                        <label class="input-group-text" >Origin</label>
                                        <input type="text" class="form-control form-control-sm" id="origin" readonly>                                        
                                       
                                    </div> 
                                </div>    
                            </div>
                            <div class="strike">
                                <span>Cover Month: <?php echo $mes1; ?></span>
                            </div>

                            <div class="row shadow p-3 mb-5 bg-body rounded">
                                <div class="col-md-3" style="display:none;">                                 
                                    <div class="input-g roup mb-3">
                                        <label class="input-group-text" >Cover Month</label>
                                        <input type="text" class="form-control form-control-sm" value="<?php echo $mes1; ?>" id="covmont_m1" readonly >                                        
                                       
                                    </div> 
                                </div>    

                                <div class="col-md-3">                                 
                                    <div class="input-group mb-3">
                                        <label class="input-group-text" >RS-SM</label>
                                        <input type="text" class="form-control form-control-sm" id="m1_RS_SM" >                                        
                                       
                                    </div> 
                                </div>    

                                <div class="col-md-3">                                 
                                    <div class="input-group mb-3">
                                        <label class="input-group-text" >RS-M</label>
                                        <input type="text" class="form-control form-control-sm" id="m1_RS_M" >                                        
                                       
                                    </div> 
                                </div>    

                                <div class="col-md-3">                                 
                                    <div class="input-group mb-3">
                                        <label class="input-group-text" >RS-SLM</label>
                                        <input type="text" class="form-control form-control-sm" id="m1_RS_SLM" >                                        
                                       
                                    </div> 
                                </div>    
                                <div class="col-md-3">                                 
                                    <div class="input-group mb-3">
                                        <label class="input-group-text" >OE</label>
                                        <input type="text" class="form-control form-control-sm" id="m1_OS" >                                        
                                       
                                    </div> 
                                </div>  
                                    
                                    <!-- ALERTA -->
                                <p> </p>
                                <div id="alerta_mes1" class="row" style="display:none"> 

                                    <div class="alert alert-warning d-flex align-items-center" role="alert">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="currentColor" class="bi bi-exclamation-triangle-fill flex-shrink-0 me-2" viewBox="0 0 16 16" role="img" aria-label="Warning:">
                                        <path d="M8.982 1.566a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566zM8 5c.535 0 .954.462.9.995l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995A.905.905 0 0 1 8 5zm.002 6a1 1 0 1 1 0 2 1 1 0 0 1 0-2z"/>
                                    </svg>
                                    <div id="texto_alerta1">
                                        Validate the values 
                                    </div>
                                    </div>                            

                                </div>
                            
                            </div>

               
                            <div class="strike">
                                <span>Cover Month: <?php echo $mes2; ?></span>
                            </div>
                            <div class="row shadow p-3 mb-5 bg-body rounded">
                                <div class="col-md-3" style="display:none;">                                 
                                    <div class="input-group mb-3">
                                        <label class="input-group-text" >Cover Month</label>
                                        <input type="text" class="form-control form-control-sm" value="<?php echo $mes2; ?>" id="covmont_m2" readonly>                                        
                                       
                                    </div> 
                                </div>    

                                <div class="col-md-3">                                 
                                    <div class="input-group mb-3">
                                        <label class="input-group-text" >RS-SM</label>
                                        <input type="text" class="form-control form-control-sm" id="m2_RS_SM">                                        
                                       
                                    </div> 
                                </div>    

                                <div class="col-md-3">                                 
                                    <div class="input-group mb-3">
                                        <label class="input-group-text" >RS-M</label>
                                        <input type="text" class="form-control form-control-sm" id="m2_RS_M">                                        
                                       
                                    </div> 
                                </div>    

                                <div class="col-md-3">                                 
                                    <div class="input-group mb-3">
                                        <label class="input-group-text" >RS-SLM</label>
                                        <input type="text" class="form-control form-control-sm" id="m2_RS_SLM">                                        
                                       
                                    </div> 
                                </div>    
                                <div class="col-md-3">                                 
                                    <div class="input-group mb-3">
                                        <label class="input-group-text" >OE</label>
                                        <input type="text" class="form-control form-control-sm" id="m2_OS">                                        
                                       
                                    </div> 
                                </div>   
                                
                                    <!-- ALERTA -->
                                    <p> </p>
                                <div id="alerta_mes2" class="row" style="display:none"> 

                                    <div class="alert alert-warning d-flex align-items-center" role="alert">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="currentColor" class="bi bi-exclamation-triangle-fill flex-shrink-0 me-2" viewBox="0 0 16 16" role="img" aria-label="Warning:">
                                        <path d="M8.982 1.566a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566zM8 5c.535 0 .954.462.9.995l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995A.905.905 0 0 1 8 5zm.002 6a1 1 0 1 1 0 2 1 1 0 0 1 0-2z"/>
                                    </svg>
                                    <div id="texto_alerta2">
                                        Validate the values 
                                    </div>
                                    </div>                            

                                </div>
                            </div>



                            <div class="strike">
                                <span>Cover Month: <?php echo $mes3; ?></span>
                            </div>

                            <div class="row shadow p-3 mb-5 bg-body rounded">
                                <div class="col-md-3" style="display:none;">                                 
                                    <div class="input-group mb-3">
                                        <label class="input-group-text" >Cover Month</label>
                                        <input type="text" class="form-control form-control-sm" value="<?php echo $mes3; ?>" id="covmont_m3" readonly>                                        
                                       
                                    </div> 
                                </div>    

                                <div class="col-md-3">                                 
                                    <div class="input-group mb-3">
                                        <label class="input-group-text" >RS-SM</label>
                                        <input type="text" class="form-control form-control-sm" id="m3_RS_SM" >                                        
                                       
                                    </div> 
                                </div>    

                                <div class="col-md-3">                                 
                                    <div class="input-group mb-3">
                                        <label class="input-group-text" >RS-M</label>
                                        <input type="text" class="form-control form-control-sm" id="m3_RS_M" >                                        
                                       
                                    </div> 
                                </div>    

                                <div class="col-md-3">                                 
                                    <div class="input-group mb-3">
                                        <label class="input-group-text" >RS-SLM</label>
                                        <input type="text" class="form-control form-control-sm" id="m3_RS_SLM" >                                        
                                       
                                    </div> 
                                </div>    
                                <div class="col-md-3">                                 
                                    <div class="input-group mb-3">
                                        <label class="input-group-text" >OE</label>
                                        <input type="text" class="form-control form-control-sm" id="m3_OS" >                                        
                                       
                                    </div> 
                                </div> 
                                
                                    <!-- ALERTA -->
                                    <p> </p>
                                <div id="alerta_mes3" class="row" style="display:none"> 

                                    <div class="alert alert-warning d-flex align-items-center" role="alert">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="currentColor" class="bi bi-exclamation-triangle-fill flex-shrink-0 me-2" viewBox="0 0 16 16" role="img" aria-label="Warning:">
                                        <path d="M8.982 1.566a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566zM8 5c.535 0 .954.462.9.995l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995A.905.905 0 0 1 8 5zm.002 6a1 1 0 1 1 0 2 1 1 0 0 1 0-2z"/>
                                    </svg>
                                    <div id="texto_alerta3">
                                        Validate the values 
                                    </div>
                                    </div>                            

                                </div>
                            </div>

                            <div class="strike">
                                <span>Cover Month: <?php echo $mes4; ?></span>
                            </div>
                            <div class="row shadow p-3 mb-5 bg-body rounded">
                                <div class="col-md-3" style="display:none;">                                 
                                    <div class="input-group mb-3">
                                        <label class="input-group-text" >Cover Month</label>
                                        <input type="text" class="form-control form-control-sm" value="<?php echo $mes4; ?>" id="covmont_m4" readonly>                                        
                                       
                                    </div> 
                                </div>    

                                <div class="col-md-3">                                 
                                    <div class="input-group mb-3">
                                        <label class="input-group-text" >RS-SM</label>
                                        <input type="text" class="form-control form-control-sm" id="m4_RS_SM" >                                        
                                       
                                    </div> 
                                </div>    

                                <div class="col-md-3">                                 
                                    <div class="input-group mb-3">
                                        <label class="input-group-text" >RS-M</label>
                                        <input type="text" class="form-control form-control-sm" id="m4_RS_M" >                                        
                                       
                                    </div> 
                                </div>    

                                <div class="col-md-3">                                 
                                    <div class="input-group mb-3">
                                        <label class="input-group-text" >RS-SLM</label>
                                        <input type="text" class="form-control form-control-sm" id="m4_RS_SLM" >                                        
                                       
                                    </div> 
                                </div>    
                                <div class="col-md-3">                                 
                                    <div class="input-group mb-3">
                                        <label class="input-group-text" >OE</label>
                                        <input type="text" class="form-control form-control-sm" id="m4_OS" >                                        
                                       
                                    </div> 
                                </div>    

                                    <!-- ALERTA -->
                                    <p> </p>
                                <div id="alerta_mes4" class="row" style="display:none"> 

                                    <div class="alert alert-warning d-flex align-items-center" role="alert">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="currentColor" class="bi bi-exclamation-triangle-fill flex-shrink-0 me-2" viewBox="0 0 16 16" role="img" aria-label="Warning:">
                                        <path d="M8.982 1.566a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566zM8 5c.535 0 .954.462.9.995l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995A.905.905 0 0 1 8 5zm.002 6a1 1 0 1 1 0 2 1 1 0 0 1 0-2z"/>
                                    </svg>
                                    <div id="texto_alerta4">
                                        Validate the values 
                                    </div>
                                    </div>                            

                                </div>
                            </div>

                            
                            
                            <div id="divCreate">
                                <div class="modal-footer">
                                    <!--<button class='btn btn-success btn-sm btnBuscar'><i class='material-icons'>manage_search</i></button>-->
                                    <div id="avisoCrud"></div>                                    
                                    <button type="button" class="btn btn-light" id="btnCancel" data-bs-dismiss="modal">Cancel</button>
                                    <button type="submit" id="btnGuardar" class="btn btn-dark">Save</button>
                                </div>
                            </div>
                    
                        </div>
                    </form>
                </div>
            </div>
        </div>  
        
        
                    
        <div class="modal fade" data-bs-backdrop="static" id="modalcover" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Info Lots</h5>
                        <button type="button" class="btn-close" aria-label="Close" id="closecoverx"></button>
                    </div>
                    <form id="modmescover">
                        <div class="modal-body" >
                            <div class="row">
                                <div class="col-lg-8">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Cover Month 1 </label>
                                        <select class="form-control form-control-sm" id="covmes1" name="covmes1">                              
                                            
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-8">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Cover Month 2</label>
                                        <select class="form-control form-control-sm" id="covmes2" name="covmes2">                              
                                            
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-8">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Cover Month 3</label>
                                        <select class="form-control form-control-sm" id="covmes3" name="covmes3">                              
                                            
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-8">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Cover Month 4</label>
                                        <select class="form-control form-control-sm" id="covmes4" name="covmes4">                              
                                            
                                        </select>
                                    </div>
                                </div>
                            </div>

                             <!-- ALERTA -->
                             <p> </p>
                            <div id="alerta_val_mes" class="row" style="display:none"> 

                                <div class="alert alert-warning d-flex align-items-center" role="alert">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="currentColor" class="bi bi-exclamation-triangle-fill flex-shrink-0 me-2" viewBox="0 0 16 16" role="img" aria-label="Warning:">
                                    <path d="M8.982 1.566a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566zM8 5c.535 0 .954.462.9.995l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995A.905.905 0 0 1 8 5zm.002 6a1 1 0 1 1 0 2 1 1 0 0 1 0-2z"/>
                                </svg>
                                <div id="textalerta_val_mes">
                                    Validate Cover Month
                                </div>
                                </div>                            

                            </div>
                        </div>                        
                    </form>
                    <div class="modal-footer">
                        <!--<button type="submit" id="btnClose" class="btn btn-light">Close</button>-->
                        <button type="button" id="closecove" class="btn btn-dark" data-dismiss="modal">Close</button>
                       <button type="button" id="btnguardarmes" class="btn btn-primary">Save changes</button>
                    </div>
                </div>
            </div>
        </div>
        
        <!-- CORREO RECORDATORIO -->
        
        <div class="modal hide fade" id="modal-msj" data-bs-backdrop="static" tabindex="-1" role="dialog" data-bs-keyboard="false" aria-hidden="true">
            <div class="modal-dialog modal-xl">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-tittle" style="">Preview email</h4>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <form id="Msj" class="Msj">
                            <div class="input-group mt-3">
                                <label class="input-group-text" for="subject">Subject</label>
                                <input type="text" class="form-control subject" id="subject" value="Origin Basis - Update Reminder" readonly>
                            </div>
                            <br>
                            <div class="body-mail">
                                <h5>Mails:</h5>
                            </div>
                            <div class="input-group mt-3">
                                <textarea type="text" rows="4" placeholder="Separados por coma" class="form-control extraEmails" id="maildestino"></textarea>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        <button id="sendEmail" type="button" class="btn btn-primary sendEmail">Send Email</button>
                    </div>
                </div>
            </div>
        </div>



    </body>

    </html>
<?php
endif;
?>
