$(document).ready(function() {

    //var tabla = $('#table_costtoland');
    var val1,val2,val3,val4,val5,val6,val7,val8,val9,val10,val12,val13;

    ///asiganr fecha en calendario

    var date=new Date();  
    var day=String(date.getDate()).padStart(2, '0');          
    var month=("0" + (date.getMonth() + 1)).slice(-2); 
    var year=date.getFullYear();  
    fechahoy=year+"-"+month+"-"+day

    $("#fecha").val(fechahoy);
    permiso=$("#editCost").val();



    $.ajax({
        url: "bd/CrudCostToLand.php",
        type: "POST",
        datatype:"json",
        data:  {opcion:1,fecha:fechahoy},    
        success: function(data){    
            opts = JSON.parse(data);

            for (var i = 0; i< opts.length; i++){ 
                //console.log(opts[i][0].length);
                
                var fila = $("<tr>  class='sinborde'");
                fila.append("<td><h8><b><input type='text'  style='transform: scale(0.8);' size='8' class='origen' value='"+opts[i][0][0]+"' disabled></b></h8></td>");
                fila.append("<td><input type='text' size='2' style='transform: scale(0.8);' class='bgd' value='"+opts[i][0][1]+"' disabled></td>");
                fila.append("<td><input type='text' size='2' style='transform: scale(0.8);' class='bra' value='"+opts[i][0][2]+"' disabled></td>");
                fila.append("<td><input type='text' size='2' style='transform: scale(0.8);' class='chn' value='"+opts[i][0][3]+"' disabled></td>");
                fila.append("<td><input type='text' size='2' style='transform: scale(0.8);' class='idn' value='"+opts[i][0][4]+"' disabled></td>");
                fila.append("<td><input type='text' size='2' style='transform: scale(0.8);' class='ind' value='"+opts[i][0][5]+"' disabled></td>");
                fila.append("<td><input type='text' size='2' style='transform: scale(0.8);' class='ita' value='"+opts[i][0][6]+"' disabled></td>");
                fila.append("<td><input type='text' size='2' style='transform: scale(0.8);' class='mex' value='"+opts[i][0][7]+"' disabled></td>");
                fila.append("<td><input type='text' size='2' style='transform: scale(0.8);' class='pak' value='"+opts[i][0][8]+"' disabled></td>");
                fila.append("<td><input type='text' size='2' style='transform: scale(0.8);' class='prt' value='"+opts[i][0][9]+"' disabled></td>");
                fila.append("<td><input type='text' size='2' style='transform: scale(0.8);' class='tur' value='"+opts[i][0][10]+"' disabled></td>");
                fila.append("<td><input type='text' size='2' style='transform: scale(0.8);' class='twn' value='"+opts[i][0][11]+"' disabled></td>");
                fila.append("<td><input type='text' size='2' style='transform: scale(0.8);' class='usa' value='"+opts[i][0][12]+"' disabled></td>");
                fila.append("<td><input type='text' size='2' style='transform: scale(0.8);' class='vnm' value='"+opts[i][0][13]+"' disabled></td>");  
                if(permiso == 1){       
                fila.append("<td> <button class='btn btn-primary  btn-sm editar' title='Edit' ><i class='material-icons'>edit</i></button><button class='btn btn-success  btn-sm guardar' title='Save' style='display:none;'><i class='material-icons'>save</i></button><button class='btn btn-danger  btn-sm cancelar' title='Cancel' style='display:none;'><i class='material-icons'>close</i></button></td>");
                }
               
                $('#table_costtoland').append(fila);
            }

            $('.origen').removeClass("origen").addClass("origen sinborde colortexto");
            $('.bgd').removeClass("bgd").addClass("bgd sinborde");
            $('.bra').removeClass("bra").addClass("bra sinborde");
            $('.chn').removeClass("chn").addClass("chn sinborde");
            $('.idn').removeClass("idn").addClass("idn sinborde");
            $('.ind').removeClass("ind").addClass("ind sinborde");
            $('.ita').removeClass("ita").addClass("ita sinborde");
            $('.mex').removeClass("mex").addClass("mex sinborde");
            $('.pak').removeClass("pak").addClass("pak sinborde");
            $('.prt').removeClass("prt").addClass("prt sinborde");
            $('.tur').removeClass("tur").addClass("tur sinborde");
            $('.twn').removeClass("twn").addClass("twn sinborde");
            $('.usa').removeClass("usa").addClass("usa sinborde");
            $('.vnm').removeClass("vnm").addClass("vnm sinborde");
    
            
            

        },
        error: function(data) {
            alert('error');
        }
    });



    $("#table_costtoland").on("click", ".editar", function() {
        var fila = $(this).closest("tr");
        fecha=$("#fecha").val();
        fila.find(".bgd").prop("disabled", false);
        fila.find(".bra").prop("disabled", false);
        fila.find(".chn").prop("disabled", false);
        fila.find(".idn").prop("disabled", false);
        fila.find(".ind").prop("disabled", false);
        fila.find(".ita").prop("disabled", false);
        fila.find(".mex").prop("disabled", false);
        fila.find(".pak").prop("disabled", false);
        fila.find(".prt").prop("disabled", false);
        fila.find(".tur").prop("disabled", false);
        fila.find(".twn").prop("disabled", false);
        fila.find(".usa").prop("disabled", false);
        fila.find(".vnm").prop("disabled", false);

        //obtener valores antes de editar 
        val1=fila.find(".bgd").val();
        val2=fila.find(".bra").val();
        val3=fila.find(".chn").val();
        val4=fila.find(".idn").val();
        val5=fila.find(".ind").val();
        val6=fila.find(".ita").val();
        val7=fila.find(".mex").val();
        val8=fila.find(".pak").val();
        val9=fila.find(".prt").val();
        val10=fila.find(".tur").val();
        val11=fila.find(".twn").val();
        val12=fila.find(".usa").val();
        val13=fila.find(".vnm").val();

        fila.find(".editar").hide();
        fila.find(".guardar").show();
        fila.find(".cancelar").show();
        $(".editar").prop("disabled", true);

      });

      $("#table_costtoland").on("click", ".guardar", function() {
        var fila = $(this).closest("tr");
        origen=fila.find(".origen").val();
        var datos = [];
        $(".editar").prop("disabled", false);
        

        //obtener los valores de los campos 
        newval1=fila.find(".bgd").val();
        newval2=fila.find(".bra").val();
        newval3=fila.find(".chn").val();
        newval4=fila.find(".idn").val();
        newval5=fila.find(".ind").val();
        newval6=fila.find(".ita").val();
        newval7=fila.find(".mex").val();
        newval8=fila.find(".pak").val();
        newval9=fila.find(".prt").val();
        newval10=fila.find(".tur").val();
        newval11=fila.find(".twn").val();
        newval12=fila.find(".usa").val();
        newval13=fila.find(".vnm").val();


        //comparar si hubo alguna cambio en alguno de los campos 
        //si el cambio se hace el sobre la fecha actual se guarda todo el vector 
        if(fecha == fechahoy){

            if(newval1 == val1){
                fila.find(".bgd").val(val1);
                if(val1!="" && val1!=null){
                    var object1 = {
                        id: 1,
                        origin: origen,
                        destino: 'BGD',
                        basis: val1
                    };
                    datos.push(object1);
                }
           
            }
            else{
                fila.find(".bgd").val(newval1);
                
                var object1 = {
                    id: 1,
                    origin: origen,
                    destino: 'BGD',
                    basis: newval1
                };
                datos.push(object1);
    
            }

            if(newval2 == val2){
                fila.find(".bra").val(val2);
                if(val2!="" && val2!=null){
                    var object2 = {
                        id: 2,
                        origin: origen,
                        destino: 'BRA',
                        basis: val2
                    };
                    datos.push(object2);
                }
               
                
           
            }
            else{
                fila.find(".bra").val(newval2);
                var object2 = {
                    id: 2,
                    origin: origen,
                    destino: 'BRA',
                    basis: newval2
                };
                datos.push(object2);
            }

            if(newval3 == val3){
                fila.find(".chn").val(val3);
                if(val3!="" && val3!=null){
                    var object3 = {
                        id: 3,
                        origin: origen,
                        destino: 'CHN',
                        basis: val3
                    };
                    datos.push(object3);
                }

                
           
            }
            else{
                fila.find(".chn").val(newval3);
                var object3 = {
                    id: 3,
                    origin: origen,
                    destino: 'CHN',
                    basis: newval3
                };
                datos.push(object3);
            }

            if(newval4 == val4){
                fila.find(".idn").val(val4);
                if(val4!="" && val4!=null){
                    var object4 = {
                        id: 4,
                        origin: origen,
                        destino: 'IDN',
                        basis: val4
                    };
                    datos.push(object4);
                }

               
            }
            else{
                fila.find(".idn").val(newval4);
                
                    var object4 = {
                        id: 4,
                        origin: origen,
                        destino: 'IDN',
                        basis: newval4
                    };
               
                datos.push(object4);
            }

            if(newval5 == val5){
                fila.find(".ind").val(val5);
                if(val5!="" && val5!=null){
                    var object5 = {
                        id: 5,
                        origin: origen,
                        destino: 'IND',
                        basis: val5
                    };
                    datos.push(object5);
                }

                
            }
            else{
                fila.find(".ind").val(newval5);
                var object5 = {
                    id: 5,
                    origin: origen,
                    destino: 'IND',
                    basis: newval5
                };
                datos.push(object5);
            }

            if(newval6 == val6){
                fila.find(".ita").val(val6);
                if(val6!="" && val6!=null){
                    var object6 = {
                        id: 6,
                        origin: origen,
                        destino: 'ITA',
                        basis: val6
                    };
                    datos.push(object6);
                }

                
            }
            else{
                fila.find(".ita").val(newval6);
                var object6 = {
                    id: 6,
                    origin: origen,
                    destino: 'ITA',
                    basis: newval6
                };
                datos.push(object6);

            }

            if(newval7 == val7){
                fila.find(".mex").val(val7);
                if(val7!="" && val7!=null){
                    var object7 = {
                        id: 7,
                        origin: origen,
                        destino: 'MEX',
                        basis: val7
                    };
                    datos.push(object7);
                }

                
            }
            else{
                fila.find(".mex").val(newval7);
                var object7 = {
                    id: 7,
                    origin: origen,
                    destino: 'MEX',
                    basis: newval7
                };
                datos.push(object7);
            }

            if(newval8 == val8){
                fila.find(".pak").val(val8);
                if(val8!="" && val8!=null){
                    var object8 = {
                        id: 8,
                        origin: origen,
                        destino: 'PAK',
                        basis: val8
                    };

                    datos.push(object8);
                }

                
            }
            else{
                fila.find(".pak").val(newval8);
                var object8 = {
                    id: 8,
                    origin: origen,
                    destino: 'PAK',
                    basis: newval8
                };
                datos.push(object8);
            }

            if(newval9 == val9){
                fila.find(".prt").val(val9);
                if(val9!="" && val9!=null){
                    var object9 = {
                        id: 9,
                        origin: origen,
                        destino: 'PRT',
                        basis: val9
                    };

                    datos.push(object9);
                }

                
            }
            else{
                fila.find(".prt").val(newval9);
                var object9 = {
                    id: 9,
                    origin: origen,
                    destino: 'PRT',
                    basis: newval9
                };
                datos.push(object9);
            }
            if(newval10 == val10){
                fila.find(".tur").val(val10);
                if(val10!="" && val10!=null){
                    var object10 = {
                        id: 10,
                        origin: origen,
                        destino: 'PRT',
                        basis: val10
                    };
                    datos.push(object10);

                }

            }
            else{
                fila.find(".tur").val(newval10);
                var object10 = {
                    id: 10,
                    origin: origen,
                    destino: 'TUR',
                    basis: newval10
                };
                datos.push(object10);
                
            }
            if(newval11 == val11){
                fila.find(".twn").val(val11);
                if(val11!="" && val11!=null){
                    var object11 = {
                        id: 11,
                        origin: origen,
                        destino: 'TWN',
                        basis: val11
                    };

                    datos.push(object11);
                }

                
            }
            else{
                fila.find(".twn").val(newval11);
                var object11 = {
                    id: 11,
                    origin: origen,
                    destino: 'TWN',
                    basis: newval11
                };
                datos.push(object11);
            }

            if(newval12 == val12){
                fila.find(".usa").val(val12);
                if(val12!="" && val12!=null){
                    var object12 = {
                        id: 12,
                        origin: origen,
                        destino: 'USA',
                        basis: val12
                    };
                    datos.push(object12);
                }

               
            }
            else{
                fila.find(".usa").val(newval12);
                var object12 = {
                    id: 12,
                    origin: origen,
                    destino: 'USA',
                    basis: newval12
                };
                datos.push(object12);
            }
            if(newval13 == val13){
                fila.find(".vnm").val(val13);
                if(val13!="" && val13!=null){
                    var object13 = {
                        id: 13,
                        origin: origen,
                        destino: 'VNM',
                        basis: val13
                    };
                    datos.push(object13);
                }
               
            }
            else{
                fila.find(".vnm").val(newval13);
                var object13 = {
                    id: 13,
                    origin: origen,
                    destino: 'VNM',
                    basis: newval13
                };
                datos.push(object13);
            }

        }

        //si el cambio se hace sobre una fecha diferente a la de hoy se guarda solo el registro(s) que se modificos 
        else{

                if(newval1 == val1){
                    fila.find(".bgd").val(val1);
                //var object1={};
                }
                else{
                    fila.find(".bgd").val(newval1);
                    var object1 = {
                        id: 1,
                        origin: origen,
                        destino: 'BGD',
                        basis: newval1
                    };


                    datos.push(object1);
                }
            
                if(newval2 == val2){
                    fila.find(".bra").val(val2);
                //var object2={};
                }
                else{
                    fila.find(".bra").val(newval2);
                    var object2 = {
                        id: 2,
                        origin: origen,
                        destino: 'BRA',
                        basis: newval2
                    };
                    datos.push(object2);
                }


                if(newval3 == val3){
                    fila.find(".chn").val(val3);
                    //var object3={};
                }
                else{
                    fila.find(".chn").val(newval3);
                    var object3 = {
                        id: 3,
                        origin: origen,
                        destino: 'CHN',
                        basis: newval3
                    };


                    datos.push(object3);
                }

                if(newval4 == val4){
                    fila.find(".idn").val(val4);
                    //var object4={};
                }
                else{
                    fila.find(".idn").val(newval4);
                    var object4 = {
                        id: 4,
                        origin: origen,
                        destino: 'IDN',
                        basis: newval4
                    };

                    datos.push(object4);
                }

                if(newval5 == val5){
                    fila.find(".ind").val(val5);
                // var object5={};
                }
                else{
                    fila.find(".idn").val(newval5);
                    var object5 = {
                        id: 5,
                        origin: origen,
                        destino: 'IND',
                        basis: newval5
                    };
                    datos.push(object5);
                }

                if(newval6 == val6){
                    fila.find(".ita").val(val6);
                // var object6={};
                }
                else{
                    fila.find(".ita").val(newval6);
                    var object6 = {
                        id: 6,
                        origin: origen,
                        destino: 'ITA',
                        basis: newval6
                    };

                    datos.push(object6);
                }

                if(newval7 == val7){
                    fila.find(".mex").val(val7);
                // var object7={};
                }
                else{
                    fila.find(".mex").val(newval7);
                    var object7 = {
                        id: 7,
                        origin: origen,
                        destino: 'MEX',
                        basis: newval7
                    };

                    datos.push(object7);
                }


                if(newval8 == val8){
                    fila.find(".pak").val(val8);
                // var object8={};
                }
                else{
                    fila.find(".pak").val(newval8);
                    var object8 = {
                        id: 8,
                        origin: origen,
                        destino: 'PAK',
                        basis: newval8
                    };

                    datos.push(object8);
                }

                if(newval9 == val9){
                    fila.find(".prt").val(val9);
                // var object9={};
                }
                else{
                    fila.find(".prt").val(newval9);
                    var object9 = {
                        id: 9,
                        origin: origen,
                        destino: 'PRT',
                        basis: newval9
                    };

                    datos.push(object9);
                }


                if(newval10 == val10){
                    fila.find(".tur").val(val10);
                    //var object10={};
                }
                else{
                    fila.find(".tur").val(newval10);
                    var object10 = {
                        id: 10,
                        origin: origen,
                        destino: 'TUR',
                        basis: newval10
                    };

                    datos.push(object10);
                }


                if(newval11 == val11){
                    fila.find(".twn").val(val11);
                    //var object11={};
                }
                else{
                    fila.find(".twn").val(newval11);
                    var object11 = {
                        id: 11,
                        origin: origen,
                        destino: 'TWN',
                        basis: newval11
                    };

                    datos.push(object11);
                }


                if(newval12 == val12){
                    fila.find(".usa").val(val12);
                    //var object12={};
                }
                else{
                    fila.find(".usa").val(newval12);
                    var object12 = {
                        id: 12,
                        origin: origen,
                        destino: 'USA',
                        basis: newval12
                    };

                    datos.push(object12);
                }





                if(newval13 == val13){
                    fila.find(".vnm").val(val13);
                // var object13={};
                }
                else{
                    fila.find(".vnm").val(newval13);
                    var object13 = {
                        id: 13,
                        origin: origen,
                        destino: 'VNM',
                        basis: newval13
                    };

                    datos.push(object13);
                }
            }

        if(datos.length==0){   
        console.log("Sin cambio")
        }
        else{
            var arreglo = {
                array: JSON.stringify(datos)
            };
            usuario=$("#usuario").val();
          
            //validaos si hubo un cambio en un registro con fecha diferente a la de hoy 
            if(fechahoy != fecha){
                var opcion = confirm("you want to save the change with the date of " + fecha);
                if (opcion == true) {
                    $.ajax({
                        url: "bd/CrudCostToLand.php",
                        type: "POST",
                        datatype:"json",
                        data:  {opcion:2,arreglo:arreglo,fecha:fecha,usuario:usuario},    
                        success: function(data){  
                            alert(data)
        
                        },error: function(data) {
                            alert('error to load data');
                        }
                    });
                }
                else{
                    alert("change discarded");
                }

            }

            else{

                $.ajax({
                    url: "bd/CrudCostToLand.php",
                    type: "POST",
                    datatype:"json",
                    data:  {opcion:2,arreglo:arreglo,fecha:fecha,usuario:usuario},    
                    success: function(data){  
                        alert(data)

                    },error: function(data) {
                        alert('error to load data');
                    }
                });
            }
         }

        
        //volver a bloquear los campos 
        fila.find(".bgd").prop("disabled", true);
        fila.find(".bra").prop("disabled", true);
        fila.find(".chn").prop("disabled", true);
        fila.find(".idn").prop("disabled", true);
        fila.find(".ind").prop("disabled", true);
        fila.find(".ita").prop("disabled", true);
        fila.find(".mex").prop("disabled", true);
        fila.find(".pak").prop("disabled", true);
        fila.find(".prt").prop("disabled", true);
        fila.find(".tur").prop("disabled", true);
        fila.find(".twn").prop("disabled", true);
        fila.find(".usa").prop("disabled", true);
        fila.find(".vnm").prop("disabled", true);
        //ocultar los boton de editar y mostrar botones de guardar y cancelar
        fila.find(".editar").show();
        fila.find(".guardar").hide();
        fila.find(".cancelar").hide();

      });



      $("#table_costtoland").on("click", ".cancelar", function() {
        var fila = $(this).closest("tr");
       
        //volver a bloquear los campos 
        fila.find(".bgd").prop("disabled", true);
        fila.find(".bra").prop("disabled", true);
        fila.find(".chn").prop("disabled", true);
        fila.find(".idn").prop("disabled", true);
        fila.find(".ind").prop("disabled", true);
        fila.find(".ita").prop("disabled", true);
        fila.find(".mex").prop("disabled", true);
        fila.find(".pak").prop("disabled", true);
        fila.find(".prt").prop("disabled", true);
        fila.find(".tur").prop("disabled", true);
        fila.find(".twn").prop("disabled", true);
        fila.find(".usa").prop("disabled", true);
        fila.find(".vnm").prop("disabled", true);



        //asignar valor antes de cambio
        // fila.find(".bgd").val("HOLIS");
        fila.find(".bgd").val(val1);
        fila.find(".bra").val(val2);
        fila.find(".chn").val(val3);
        fila.find(".idn").val(val4);
        fila.find(".ind").val(val5);
        fila.find(".ita").val(val6);
        fila.find(".mex").val(val7);
        fila.find(".pak").val(val8);
        fila.find(".prt").val(val9);
        fila.find(".tur").val(val10);
        fila.find(".twn").val(val11);
        fila.find(".usa").val(val12);
        fila.find(".vnm").val(val13);

        //ocultar los botones de guardar y cancelar y mistrar solo editar 
        fila.find(".editar").show();
        fila.find(".guardar").hide();
        fila.find(".cancelar").hide();
        $(".editar").prop("disabled", false);
      });


//buscar por una determinada fecha
    $("#btnbuscar").click(function(e){
        e.preventDefault();  
        limpiarTabla()
        fecha=$("#fecha").val();
        buscar_fecha(fecha);
    })

    $("#exportar").click(function(e){
        e.preventDefault();
        exportarTabla();
    })



    
})


//ENVIAR CORREO DE RECORDATORIO

$(document).ready(function() {
    $("#PsendEmail").click(function(){ 
            $("#modal-msj").trigger("reset");
            $(".modal-header").css( "background-color", "#17562c");
            $(".modal-header").css( "color", "white" );
            $(".modal-title").text("Preview Mail");
            $('#modal-msj').modal('show');
            $(".modal-footer").css( "color", "white" );

        $.ajax({
            url: "bd/CrudCostToLand.php",
            type: "POST",
            datatype:"json",
            data:  {opcion :4},    
            success: function(data){

            var org = data.slice(1, -1);
            $("#maildestino").val(org)
        
            }
        });
        
    });
    
    $("#sendEmail").click(function(){
            asunto = $("#subject").val();
            correos = $("#maildestino").val();
            User=$("#usuario").val();
            $('#modal-msj').modal('hide');
            $.ajax({
                url: "bd/MailRecordatorioCosttoland.php",
                type: "POST",
                datatype:"json",
                data:  {asunto:asunto, correos:correos,User:User},    
                success: function(data){
                    alert(data)
                }
            });
        });
});


function buscar_fecha(date){
permiso=$("#editCost").val();
    $.ajax({
        url: "bd/CrudCostToLand.php",
        type: "POST",
        datatype:"json",
        data:  {opcion:3,fecha:date},    
        success: function(data){    
            opts = JSON.parse(data);

            for (var i = 0; i< opts.length; i++){ 
                //console.log(opts[i][0].length);
                
                var fila = $("<tr>  class='sinborde'");
                fila.append("<td><b><input type='text' size='5' class='origen' value='"+opts[i][0][0]+"' disabled></b></td>");
                fila.append("<td><input type='text' size='2' class='bgd' value='"+opts[i][0][1]+"' disabled></td>");
                fila.append("<td><input type='text' size='2' class='bra' value='"+opts[i][0][2]+"' disabled></td>");
                fila.append("<td><input type='text' size='2' class='chn' value='"+opts[i][0][3]+"' disabled></td>");
                fila.append("<td><input type='text' size='2' class='idn' value='"+opts[i][0][4]+"' disabled></td>");
                fila.append("<td><input type='text' size='2' class='ind' value='"+opts[i][0][5]+"' disabled></td>");
                fila.append("<td><input type='text' size='2' class='ita' value='"+opts[i][0][6]+"' disabled></td>");
                fila.append("<td><input type='text' size='2' class='mex' value='"+opts[i][0][7]+"' disabled></td>");
                fila.append("<td><input type='text' size='2' class='pak' value='"+opts[i][0][8]+"' disabled></td>");
                fila.append("<td><input type='text' size='2' class='prt' value='"+opts[i][0][9]+"' disabled></td>");
                fila.append("<td><input type='text' size='2' class='tur' value='"+opts[i][0][10]+"' disabled></td>");
                fila.append("<td><input type='text' size='2' class='twn' value='"+opts[i][0][11]+"' disabled></td>");
                fila.append("<td><input type='text' size='2' class='usa' value='"+opts[i][0][12]+"' disabled></td>");
                fila.append("<td><input type='text' size='2' class='vnm' value='"+opts[i][0][13]+"' disabled></td>"); 
                if (permiso == 1){        
                fila.append("<td> <button class='btn btn-primary  btn-sm editar' title='Edit' ><i class='material-icons'>edit</i></button><button class='btn btn-success  btn-sm guardar' title='Save' style='display:none;'><i class='material-icons'>save</i></button><button class='btn btn-danger  btn-sm cancelar' title='Cancel' style='display:none;'><i class='material-icons'>close</i></button></td>");
                }
               
                $('#table_costtoland').append(fila);
            }

            $('.origen').removeClass("origen").addClass("origen sinborde colortexto");
            $('.bgd').removeClass("bgd").addClass("bgd sinborde");
            $('.bra').removeClass("bra").addClass("bra sinborde");
            $('.chn').removeClass("chn").addClass("chn sinborde");
            $('.idn').removeClass("idn").addClass("idn sinborde");
            $('.ind').removeClass("ind").addClass("ind sinborde");
            $('.ita').removeClass("ita").addClass("ita sinborde");
            $('.mex').removeClass("mex").addClass("mex sinborde");
            $('.pak').removeClass("pak").addClass("pak sinborde");
            $('.prt').removeClass("prt").addClass("prt sinborde");
            $('.tur').removeClass("tur").addClass("tur sinborde");
            $('.twn').removeClass("twn").addClass("twn sinborde");
            $('.usa').removeClass("usa").addClass("usa sinborde");
            $('.vnm').removeClass("vnm").addClass("vnm sinborde");
    
            
            

        },
        error: function(data) {
            alert('error');
        }
    });

}

//funcion para lipiar la tabla 
function limpiarTabla() {
    var tabla = document.getElementById("table_costtoland");
    var filas = tabla.rows.length;

    for (var i = filas - 1; i > 0; i--) {
        tabla.deleteRow(i);
    }
   
}


//Funcion para exportar la tabla a un excell 

function exportarTabla() {
    // Obtener la tabla
    var tabla = document.getElementById("table_costtoland");
  
    // Crear una matriz para almacenar los datos de la tabla
    var datos = [];
  
    // Recorrer las filas de la tabla
    for (var i = 0; i < tabla.rows.length; i++) {
      var fila = tabla.rows[i];
      var rowData = [];
  
      // Recorrer las celdas de cada fila
      for (var j = 0; j < (fila.cells.length-1); j++) {
        var celda = fila.cells[j];
  
        // Obtener el valor del elemento input o textarea dentro de la celda
        var valor = "";
        var input = celda.querySelector("input");
        var textarea = celda.querySelector("textarea");
        var cel = celda.innerText
  
        if (input) {
          valor = input.value;
        } else if (textarea) {
          valor = textarea.value;
        }
        else if (cel) {
            valor = celda.innerText;
        }
  
        // Agregar el valor a la matriz
        rowData.push(valor);
      }
  
      // Agregar la fila a los datos
      datos.push(rowData);
    }
  
    // Crear una hoja de cálculo de Excel
    var libro = XLSX.utils.book_new();
    var hoja = XLSX.utils.aoa_to_sheet(datos);
    XLSX.utils.book_append_sheet(libro, hoja, "Cost to Land " + fechahoy);
  
    // Guardar el archivo de Excel
    XLSX.writeFile(libro, "CostToLand-"+fechahoy+".xlsx");
  }
 