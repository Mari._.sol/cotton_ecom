var opcion,IdBuy;
$(document).ready(function() {

    //funcion para sumar

    
jQuery.fn.dataTable.Api.register( 'sum()', function ( ) {
    return this.flatten().reduce( function ( a, b ) {
      if ( typeof a === 'string' ) {
        a = a.replace(/[^\d.-]/g, '') * 1;
      }
      if ( typeof b === 'string' ) {
        b = b.replace(/[^\d.-]/g, '') * 1;
      }
      return a + b;
    }, 0);
});

$('#table_buyers tfoot th').each( function () {
    var title = $(this).text();
    $(this).html( '<input type="text" placeholder="'+title+'" />' );
} );

Usuario = $("#usuario").val()
Priv = $("#priv").val();
table_buyers = $('#table_buyers').removeAttr('width').DataTable({


       /* drawCallback: function () {
            var api = this.api();
    
            var bales = api.column( 2, {"filter":"applied"}).data().sum();
            var total = api.rows({"filter":"applied"}).count();
           // var pesototal = api.column( 11, {"filter":"applied"}).data().sum();
            $('#bales').html(bales);
            $('#totaLiqID').html(total);
           // $("#totallotes").val(numlotes);
         //   $('#totalpeso').html(pesototal);
    
          },

          */
     
      
        responsive: "true",
        dom: 'Brtilp',       
        buttons:[ 
			{
			    extend:    'excelHtml5',
			    text:      '<i class="fas fa-file-excel"></i> ',
			    titleAttr: 'Export to Excel',
			    className: 'btn btn-success',
                download: 'open',
			    exportOptions: {
			        columns: ":not(.no-exportar)"
			    }
			},
        ],

        initComplete: function () {
            // Apply the search
            this.api().columns().every( function () {
                var that = this;
 
                $( 'input', this.footer() ).on( 'keyup change clear', function () {
                    if ( that.search() !== this.value ) {
                        that
                        .search( this.value, true, false ).draw();
                            
                    }
                } );
            } );
        },
           
             
        "order": [ 0, 'desc' ],
        "scrollX": true,
        "scrollY": "45vh",
        "scrollCollapse": true,
        "lengthMenu": [100,200,300,500],
        fixedColumns:   {
            left: 0,
            right: 1
        },
   
        "ajax":{            
          "url": "bd/CrudBuyers.php",
          "method": 'POST', //usamos el metodo POST
          "data":{opcion:1}, //enviamos opcion 1 para que haga un SELECT
          "dataSrc":""
      },
        "columns":[        
            {"data": "IDBuyer",width: 90},
            {"data": "Buyer"},    
            {"data": "Country",width: 150}, 
            {"data": "CorporateNumber",width: 200}, 
            {"data": "ClientStatus",width: 150},
            {"data": "Rating"},
            {"data": "Status"},
            {"defaultContent":"<div class='text-center'><div class='btn-group'><button class='btn btn-primary  btn-sm btneditar' title='Edit' ><i class='material-icons'>edit</i></button></div></div>",width: '50px'},
           
          
        ],

        columnDefs : [
       
            //COST
            { targets : [6],
                render : function (data, type, row) {
                  return data == '1' ? 'Active' : 'Inactive'
                }
              },
            ]
            
    });    
    
    var oTable = $('#table_buyers').DataTable();

    $("#btnNuevo").click(function(){
        $("#form_buyer").trigger("reset");
        $(".modal-header").css( "background-color", "#17562c");
        $(".modal-header").css( "color", "white" );
        $(".modal-title").text("New Buyer");
        $('#modal_buyer').modal('show');
        $("#Country").empty(); 
        zones();
        opcion = 4;
        $("#zone").change(function () {
            country($(this).val(),"");
        }); 
    }); 


    $(document).on("click", ".btneditar", function(){
        opcion = 6;
        fila = $(this).closest("tr");	   
        IdBuy = parseInt(fila.find('td:eq(0)').text());
        Country = fila.find('td:eq(2)').text(); 
        Buyer = fila.find('td:eq(1)').text(); 
        CorporateNumber = fila.find('td:eq(3)').text(); 
        ClientStatus= fila.find('td:eq(4)').text(); 
        Rating= fila.find('td:eq(5)').text(); 
        Status= fila.find('td:eq(6)').text(); 
        $("#zone").empty();
        $("#Country").empty(); 
        
        $("#form_buyer").trigger("reset");
        $(".modal-header").css( "background-color", "#17562c");
        $(".modal-header").css( "color", "white" );
        $(".modal-title").text("Edit Buyer");
        $('#modal_buyer').modal('show');
        

        //$("#Country").val();
        $("#Buyer").val(Buyer);
        $("#CorporateNumber").val(CorporateNumber);
        $("#ClientStatus").val(ClientStatus);
        $("#Rating").val(Rating);
        if(Status=="Active"){
            $("#Status").val(1); 
        }
        else{
            $("#Status").val(0); 
        }

        zones();
        subregion(Country);
        

        
       

    }); 





    $('#form_buyer').submit(function(e){
        e.preventDefault();  
        //zone=$("#zone").val();
        Country=$("#Country").val();
        Buyer=$("#Buyer").val();
        CorporateNumber=$("#CorporateNumber").val();
        ClientStatus=$("#ClientStatus").val();
        Rating=$("#Rating").val();
        Status=$("#Status").val();        
  
        if(opcion == 4){
  
              $.ajax({
                url: "bd/CrudBuyers.php",
                type: "POST",
                datatype:"json",
                data:  {opcion:opcion,
                        Country:Country,
                        Buyer:Buyer,
                        CorporateNumber:CorporateNumber,
                        ClientStatus:ClientStatus,
                        Rating:Rating,
                        Status:Status
                                                
                      },    
                success: function(data){         
                  alert("Successful registration");
                  table_buyers.ajax.reload(null,false);
  
                },
                error: function(data) {
                    alert('error');
                }
            });
        }
        else{
  
          $.ajax({
            url: "bd/CrudBuyers.php",
            type: "POST",
            datatype:"json",
            data:  {opcion:opcion,
                    IdBuy:IdBuy,
                    Country:Country,
                    Buyer:Buyer,
                    CorporateNumber:CorporateNumber,
                    ClientStatus:ClientStatus,
                    Rating:Rating,
                    Status:Status
                  },    
            success: function(data){         
              alert("Update Successes");
              table_buyers.ajax.reload(null,false);
  
            },
            error: function(data) {
                alert('error');
            }
          });
  
        }
  
  
        $('#modal_buyer').modal('hide');
  
  
      }); 
  



  }); 


  

  function zones(valor){
    $("#zone").empty();
    var group1 = $('<optgroup class="optgroups" label="Africa">');
    var group2 = $('<optgroup class="optgroups" label="America">');
    var group3 = $('<optgroup class="optgroups" label="Asia">');
    var group4 = $('<optgroup class="optgroups" label="Europe">');
    var group5 = $('<optgroup class="optgroups" label="Oceania">');

 

    $.ajax({
      url: "bd/CrudBuyers.php",
      type: "POST",
      datatype:"json",
      data:  {opcion:2},    
      success: function(data){
        opts = JSON.parse(data);
        $('#zone').append($('<option>').val("").text("Choose..."));
        
        for (var i = 0; i< opts.length; i++){ 

            switch (opts[i].Region) {
              case 'Africa':
                $("#zone").append(group1);
                group1.append($('<option></option>').val(opts[i].SubRegion).html(opts[i].SubRegion));
                break;
              case 'America':
                $("#zone").append(group2);
                group2.append($('<option></option>').val(opts[i].SubRegion).html(opts[i].SubRegion));
                break;  
              case 'Asia':
                $("#zone").append(group3);
                group3.append($('<option></option>').val(opts[i].SubRegion).html(opts[i].SubRegion));
                break;  
              case 'Europe':
                $("#zone").append(group4);
                group4.append($('<option></option>').val(opts[i].SubRegion).html(opts[i].SubRegion));
                break;   
              case 'Oceania':
                $("#zone").append(group5);
                group5.append($('<option></option>').val(opts[i].SubRegion).html(opts[i].SubRegion));
                break; 
            }   
          
        }     
        
        if(valor!=""){
          $("select#zone").val(valor);
        }
     
      },
      error: function(data) {
          alert('error');
      }
    });

}


function country(zona,paisselect){

    ///$('#Origin').empty();
    $.ajax({
      url: "bd/CrudBuyers.php",
      type: "POST",
      datatype:"json",
      data:  {opcion:3,zona:zona},    
      success: function(data){        
           $("#Country").empty(); 
          $('#Country').append($('<option>').val("").text("Choose..."));
          opts = JSON.parse(data);
          for (var i = 0; i< opts.length; i++){ 
              $('#Country').append($('<option>').val(opts[i].CountryName).text(opts[i].CountryName));                        
            }
            if(paisselect!=""){
            $("select#Country").val(paisselect);
            }

          
          },          
          error: function(data) {
              alert('error');
          }
      });


}


function subregion(Country){
   
    ///$('#Origin').empty();
    $.ajax({
      url: "bd/CrudBuyers.php",
      type: "POST",
      datatype:"json",
      data:  {opcion:5,Country:Country},    
      success: function(data){ 
            opts = JSON.parse(data);
            //console.log(opts[0]["SubRegion"]);
            subr=opts[0]["SubRegion"]
            $("select#zone").val(subr);
            country(subr,Country);
          },          
          error: function(data) {
              alert('error');
          }
      });


}



