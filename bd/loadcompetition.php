<?php

include_once '../bd/conexion.php';
$objeto = new Conexion();
$conexion = $objeto->Conectar();
$file_name = $_FILES['file']['name'];
$filename = $_FILES['file']['tmp_name'];
if(isset($_FILES['file']) && $_FILES['file']['error'] === UPLOAD_ERR_OK){
    $handle = fopen($filename, 'r');
    //leer primer registro para valdar numero de columnas
    $cont = 0;
    $data = fgetcsv($handle, 1000, ",");
    
    /*$consulta = "INSERT INTO Inquiries_Inquirie (Date,File,Shipper,RegionOrigin,Origin,Origin2,CropFrom,CropTo,ColMax,LenMin,MicMin,MicMax,
    QualityComment,Basis,CoverMonth,Price,StartShip,EndShip,ShipComment,Type,User,OfferComments) VALUES ('$Date','$File','$Shipper','$RegionOrigin','$Origin','$Origin2',
    '$CropFrom','$CropTo','$ColMax','$LenMin','$MicMin','$MicMax',:QualityComment,'$Basis','$CoverMonth','$Price','$StartShip','$EndShip',:ShipComment,2,'$User',:OfferComments);";      
    $resultado = $conexion->prepare($consulta);*/
    
    $consulta = "INSERT INTO Inquiries_Inquirie (Date,File,Shipper,RegionOrigin,Origin,Origin2,RegionDest,Destination,CropFrom,CropTo,ColMax,LenMin,MicMin,MicMax,
    QualityComment,Basis,Unit,CoverMonth,Price,StartShip,EndShip,ShipComment,Type,User,StrMin,Quantity,PaymentTerms) VALUES (:Date,:File,:Shipper,:RegionOrigin,:Origin,:Origin2,:RegionDest,:Destination,
    :CropFrom,:CropTo,:ColMax,:LenMin,:MicMin,:MicMax,:QualityComment,:Basis,:Unit,:CoverMonth,:Price,:StartShip,:EndShip,:ShipComment,:Type,:User,:StrMin,:Quantity,:PaymentTerms);";      
    $resultado = $conexion->prepare($consulta);
    
    while($data = fgetcsv($handle, 1000, ",")){
    
        $Date=$data[0];
        $File=$data[1];
        $Shipper=$data[2];
        $RegionOrigin=$data[3];
        $Origin=$data[4];
        $Origin2=$data[5];
        $RegionDest=$data[6];
        $Destination=$data[7];
        $CropFrom=$data[8];
        $CropTo=$data[9];
        $ColMax=$data[10];
        $LenMin=$data[11];
        $MicMin=$data[12];
        $MicMax=$data[13];
        $StrMin=$data[14];
        $QualityComment=$data[15];
        $Basis=$data[16];
        $CoverMonth=$data[17];
        $Price=$data[18];
        $Unit=$data[19];
        $Quantity=$data[20];
        $PaymentTerms=$data[21]; 
        $StartShip=$data[22];
        $EndShip=$data[23];
        $ShipComment=$data[24];
        $User=$data[25];
        //$OfferComments=$data[23];
    
        $resultado->bindValue(':Date', $Date);
        $resultado->bindValue(':File', $File);
        $resultado->bindValue(':Shipper', $Shipper);
        $resultado->bindValue(':RegionOrigin', $RegionOrigin);
        $resultado->bindValue(':Origin', $Origin);
        $resultado->bindValue(':Origin2', $Origin2);
        $resultado->bindValue(':RegionDest', $RegionDest);
        $resultado->bindValue(':Destination', $Destination);
        $resultado->bindValue(':CropFrom', $CropFrom);
        $resultado->bindValue(':CropTo', $CropTo);
        $resultado->bindValue(':ColMax', $ColMax );
        $resultado->bindValue(':LenMin', $LenMin);
        $resultado->bindValue(':MicMin', $MicMin);
        $resultado->bindValue(':MicMax', $MicMax);
        $resultado->bindValue(':QualityComment', $QualityComment, PDO::PARAM_STR);
        $resultado->bindValue(':Basis', $Basis);
        $resultado->bindValue(':Unit', $Unit);
        $resultado->bindValue(':CoverMonth', $CoverMonth);
        $resultado->bindValue(':Price', $Price);
        $resultado->bindValue(':StartShip', $StartShip);
        $resultado->bindValue(':EndShip', $EndShip);
        $resultado->bindValue(':ShipComment', $ShipComment, PDO::PARAM_STR);
        $resultado->bindValue(':Type',2);
        $resultado->bindValue(':User', $User);
        $resultado->bindValue(':StrMin',$StrMin);
        $resultado->bindValue(':Quantity',$Quantity);
        $resultado->bindValue(':PaymentTerms',$PaymentTerms);
       // $resultado->bindValue(':OfferComments', $OfferComments, PDO::PARAM_STR);
        $resultado->execute();        
    
        //$resultado->bindValue(':', );
        //$data=$resultado->fetchAll(PDO::FETCH_ASSOC);
        
    
        $cont++;
    
    
     
       
    }
    
    $dat= $cont;
    
    print json_encode($dat, JSON_UNESCAPED_UNICODE);
}


else{
    $dat="";

    print json_encode($dat, JSON_UNESCAPED_UNICODE);

}



?>