<?php
include_once '../bd/conexion.php';
$objeto = new Conexion();
$conexion = $objeto->Conectar();
$opcion = (isset($_POST['opcion'])) ? $_POST['opcion'] : '';
$Date= (isset($_POST['Date'])) ? $_POST['Date'] : '';
$origen = (isset($_POST['origen'])) ? $_POST['origen'] : '';
$zoneorigen=(isset($_POST['zoneorigen'])) ? $_POST['zoneorigen'] : '';
$Origin= (isset($_POST['Origin'])) ? $_POST['Origin'] : '';
$Origin2=(isset($_POST['Origin2'])) ? $_POST['Origin2'] : '';
$Quantity=(isset($_POST['Quantity'])) ? $_POST['Quantity'] : '';
$CropFrom=(isset($_POST['CropFrom'])) ? $_POST['CropFrom'] : '';
$CropTo=(isset($_POST['CropTo'])) ? $_POST['CropTo'] : '';
$Comments=(isset($_POST['Comments'])) ? $_POST['Comments'] : '';
$ColMax=(isset($_POST['ColMax'])) ? $_POST['ColMax'] : '';
$LenMin=(isset($_POST['LenMin'])) ? $_POST['LenMin'] : '';
$MicMin=(isset($_POST['MicMin'])) ? $_POST['MicMin'] : '';
$MicMax=(isset($_POST['MicMax'])) ? $_POST['MicMax'] : '';
$StrMin=(isset($_POST['StrMin'])) ? $_POST['StrMin'] : '';
$QualityComment=(isset($_POST['QualityComment'])) ? $_POST['QualityComment'] : '';
$Basis=(isset($_POST['Basis'])) ? $_POST['Basis'] : '';
$CoverMonth=(isset($_POST['CoverMonth'])) ? $_POST['CoverMonth'] : '';
$Usuario=(isset($_POST['Usuario'])) ? $_POST['Usuario'] : '';

$IDReq=(isset($_POST['IDReq'])) ? $_POST['IDReq'] : '';
$Zone=(isset($_POST['pais'])) ? $_POST['pais'] : '';
$priv=(isset($_POST['Priv'])) ? $_POST['Priv'] : '';

//// Variables para filtros
session_start();

$filterSes = $_SESSION['Filters'];
$FromDate=$filterSes['FromDate'];
$ToDate=$filterSes['ToDate'];
$OriginReg=$filterSes['OriginReg'];
$OriginCount=$filterSes['OriginCount'];
$UserComp=$filterSes['UserComp'];      


switch($opcion){
    case 1: // origin Country
        $consulta = "SELECT CountryCode,CountryName FROM amsadb1.Inquiries_Countries WHERE IsOrigin = '1' AND SubRegion = '$Zone' ORDER BY CountryName ASC";      
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();        
        $data=$resultado->fetchAll(PDO::FETCH_ASSOC);    
    break;
    case 4:
        $consulta = "SELECT IDZone,Zone  FROM amsadb1.Inquiries_Zones WHERE CountryCode ='$origen'  ORDER BY Zone ASC";      
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();        
        $data=$resultado->fetchAll(PDO::FETCH_ASSOC);       
    break;
    case 5:
        $consulta = "INSERT INTO amsadb1.Inquiries_basisjm (Date,RegionOrigin,Origin,Origin2,Quantity,CropFrom,CropTo,
        Comments,ColMax,LenMin,MicMin,MicMax,StrMin,QualityComment,Basis,CoverMonth,User) 
        VALUES ('$Date','$zoneorigen','$Origin','$Origin2','$Quantity','$CropFrom','$CropTo','$Comments','$ColMax',
        '$LenMin','$MicMin','$MicMax','$StrMin','$QualityComment','$Basis','$CoverMonth','$Usuario')";      
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();        
        $data=$resultado->fetchAll(PDO::FETCH_ASSOC);     
    break;
    case 9: //CARGAR TABLA
        $complemeto="";
        if($priv==1){
            $complemeto=" ORDER BY IDInquirie  DESC LIMIT 200;";
        }
        else{
            $complemeto=" WHERE Inquiries_basisjm.User = '$Usuario' ORDER BY IDInquirie  DESC ;";
        }       
    
        //VALIDAR LOS FILTROS 
        if($FromDate=="" && $ToDate=="" && $OriginReg =="" && $OriginCount =="" && $UserComp==""){
            $consulta = "SELECT 
            IDInquirie,
            Date,
            RegionOrigin,
            Origin,
            Origin2,
            Quantity,
            CropFrom,
            CropTo,
            Comments,
            ColMax,
            LenMin,
            MicMin,
            MicMax,
            StrMin,
            QualityComment,
            Basis,
            CoverMonth, 
            U.Name as NameUser,
            Countries.CountryName
            FROM amsadb1.Inquiries_basisjm
            LEFT JOIN amsadb1.Inquiries_Countries as Countries 
            ON Countries.CountryCode = Inquiries_basisjm.Origin
            LEFT JOIN amsadb1.Users_Inquiries as U
            ON U.User = Inquiries_basisjm.User ".$complemeto;
            $resultado = $conexion->prepare($consulta);
            $resultado->execute();
        }else{
            $finconsulta=" ORDER BY IDInquirie  DESC";
            $filtro1="";
            if($OriginReg != ""){
                $filtro1 .= " AND RegionOrigin = '$OriginReg'";
            }
            if($OriginCount != ""){
                $filtro1 .= " AND Origin = '$OriginCount' ";
            }

            if($UserComp != ""){       
                $filtro1 .= " AND Inquiries_basisjm.User = '$UserComp' ";
            }
            $consulta ="SELECT
            IDInquirie,
            Date,
            RegionOrigin,
            Origin,
            Origin2,
            Quantity,
            CropFrom,
            CropTo,
            Comments,
            ColMax,
            LenMin,
            MicMin,
            MicMax,
            StrMin,
            QualityComment,
            Basis,
            CoverMonth,
            U.Name as NameUser,
            Countries.CountryName
        FROM
            amsadb1.Inquiries_basisjm
        LEFT JOIN
            amsadb1.Inquiries_Countries as Countries ON Countries.CountryCode = Inquiries_basisjm.Origin
        LEFT JOIN
            amsadb1.Users_Inquiries as U ON U.User = Inquiries_basisjm.User
        WHERE
            (Date BETWEEN  '$FromDate' AND '$ToDate')".$filtro1.$finconsulta;
            $resultado = $conexion->prepare($consulta);
            $resultado->execute();     
        }
        $data=$resultado->fetchAll(PDO::FETCH_ASSOC);  
    break;
    case 11: // editar
        $consulta = " UPDATE Inquiries_basisjm  SET 
        Date = '$Date', 
        RegionOrigin ='$zoneorigen',
        Origin='$Origin',
        Origin2='$Origin2',
        Quantity = '$Quantity',
        CropFrom='$CropFrom', 
        CropTo = '$CropTo',
        Comments = '$Comments',
        ColMax = '$ColMax', 
        LenMin='$LenMin', 
        MicMin = '$MicMin', 
        MicMax = $MicMax, 
        StrMin = '$StrMin', 
        QualityComment = '$QualityComment',
        Basis='$Basis', 
        CoverMonth = '$CoverMonth',
        User='$Usuario'
        where Inquiries_basisjm.IDInquirie='$IDReq'";      
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();        
        $data=$resultado->fetchAll(PDO::FETCH_ASSOC);   
        print_r($IDReq);
    break;
    case 13: //coverMonth
        $consulta = "SELECT * FROM amsadb1.Inquiries_Covmon WHERE  Inquiries_Covmon.Available = '1' ";      
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();        
        $data=$resultado->fetchAll(PDO::FETCH_ASSOC);    
    break;
    case 14://origin region
        $consulta = "SELECT  DISTINCT SubRegion,Region FROM amsadb1.Inquiries_Countries WHERE IsOrigin = '1' ORDER BY Region ASC";      
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();        
        $data=$resultado->fetchAll(PDO::FETCH_ASSOC);    
    break;
    case 15: //User filtro
        $consulta = "SELECT  DISTINCT Users_Inquiries.Name,Users_Inquiries.User 
        FROM Inquiries_basisjm, Users_Inquiries 
        WHERE Inquiries_basisjm.User = Users_Inquiries.User
        ORDER BY Name ASC";
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();        
        $data=$resultado->fetchAll(PDO::FETCH_ASSOC);    
    break;
}
print json_encode($data, JSON_UNESCAPED_UNICODE);

$conexion=null;