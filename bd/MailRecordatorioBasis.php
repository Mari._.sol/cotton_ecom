<?php
  require '../vendor/autoload.php';
  use PHPMailer\PHPMailer\PHPMailer;
  use PHPMailer\PHPMailer\SMTP;

  include_once '../bd/conexion.php';
  $objeto = new Conexion();
  $conexion = $objeto->Conectar();

  require '../bd/vendor/phpmailer/phpmailer/src/PHPMailer.php';
  require '../bd/vendor/phpmailer/phpmailer/src/SMTP.php'; 

  $asunto= (isset($_POST['asunto'])) ? $_POST['asunto'] : '';
  $correos= (isset($_POST['correos'])) ? $_POST['correos'] : '';
  $User=(isset($_POST['User'])) ? $_POST['User'] : '';

  date_default_timezone_set('America/Los_Angeles');

  $query = 'SELECT Name,Email,PassApp FROM amsadb1.Users_Inquiries WHERE User = "'.$User.'"';
  $result =$conexion->prepare($query);
  $result->execute();
  $datosuser=$result->fetch(PDO::FETCH_ASSOC);

  $email=$datosuser['Email'];
  $pass =$datosuser['PassApp'];
  $usuario =$datosuser['Name'];
  // -------------------- fechas de ORIGIN BASIS-----
  $origenes = ['AUS', 'BRA', 'GRC', 'IND', 'MEX', 'PRY', 'SPA', 'TUR', 'USA-MS', 'USA-PM', 'USA-SE', 'USA-STX', 'USA-WTX', 'WAF'];
  $origenesData = array_fill_keys($origenes, ['OrgBasis' => 0, 'InquiriesBasisjm' => 0]); // Inicializa el array con ambas columnas
  $placeholders = implode(', ', array_fill(0, count($origenes), '?'));
  
  // Consulta para OrgBasis
  $consultaOrgBasis = "SELECT Org, COALESCE(MAX(Dat), 0) as fecha FROM amsadb1.OrgBasis WHERE Org IN ($placeholders) GROUP BY Org ORDER BY Org";
  $resultadoOrgBasis = $conexion->prepare($consultaOrgBasis);
  $resultadoOrgBasis->execute($origenes);
  $tablaOrgBasis = $resultadoOrgBasis->fetchAll(PDO::FETCH_ASSOC);
  
  // Llenar el array con los resultados de la consulta para OrgBasis
  foreach ($tablaOrgBasis as $row) {
      $origenesData[$row['Org']]['OrgBasis'] = $row['fecha'];
  }
  
  // ------------------Consulta para InquiriesBasisjm-----------
  $origenes2 = ['AU', 'BR', 'GR', 'IN', 'MX', 'PY', 'ES', 'TR'];
  $placeholders = implode(', ', array_fill(0, count($origenes2), '?'));
  $consultaInquiriesBasisjm = "SELECT Origin, COALESCE(MAX(Date), 0) as fecha FROM amsadb1.Inquiries_basisjm WHERE Origin IN ($placeholders) GROUP BY Origin ORDER BY Origin";
  $resultadosBasisJM = $conexion->prepare($consultaInquiriesBasisjm);
  $resultadosBasisJM->execute($origenes2);
  $tablaInquiriesBasisjm = $resultadosBasisJM->fetchAll(PDO::FETCH_ASSOC);
  
  // cambiar nombre y traer ultimas fechas 
  $mapaPaises = [
    'AU' => 'AUS',
    'BR' => 'BRA',
    'GR' => 'GRC',
    'IN' => 'IND',
    'MX' => 'MEX',
    'PY' => 'PRY',
    'ES' => 'SPA',
    'TR' => 'TUR'
  ];
  
  foreach ($tablaInquiriesBasisjm as $row) {
    $origen = isset($mapaPaises[$row['Origin']]) ? $mapaPaises[$row['Origin']] : $row['Origin'];
    $origenesData[$origen]['InquiriesBasisjm'] = $row['fecha'];
  }

  // -----------------------USA------------
  $origenes3 = ['Memphis', 'PIMA', 'Southeast', 'South Texas', 'West Texas'];
  $placeholders3 = implode(', ', array_fill(0, count($origenes3), '?'));

  $consultaInquiriesBasisjm = "SELECT Origin2, COALESCE(MAX(Date), 0) as fecha FROM amsadb1.Inquiries_basisjm WHERE Origin2 IN ($placeholders3) GROUP BY Origin2 ORDER BY Origin2";
  $resultadosBasisJM = $conexion->prepare($consultaInquiriesBasisjm);
  $resultadosBasisJM->execute($origenes3);
  $tablaInquiriesBasisjm2 = $resultadosBasisJM->fetchAll(PDO::FETCH_ASSOC);
  
  // cambiar nombre y traeer ult fechazona US
  $mapaPaises2 = [
    'Memphis' => 'USA-MS',
    'PIMA' => 'USA-PM',
    'Southeast' => 'USA-SE',
    'South Texas' => 'USA-STX',
    'West Texas' => 'USA-WTX'
  ];
  foreach ($tablaInquiriesBasisjm2 as $row) {
    $origen = isset($mapaPaises2[$row['Origin2']]) ? $mapaPaises2[$row['Origin2']] : $row['Origin2'];
    $origenesData[$origen]['InquiriesBasisjm'] = $row['fecha'];
  }

  //------------------  Western Africa ------------------------
  $consultaInquiriesBasisjm = "SELECT RegionOrigin, COALESCE(MAX(Date), 0) as fecha FROM amsadb1.Inquiries_basisjm WHERE RegionOrigin ='Western Africa' GROUP BY RegionOrigin ORDER BY RegionOrigin";
  $resultadosBasisJM = $conexion->prepare($consultaInquiriesBasisjm);
  $resultadosBasisJM->execute();
  $tablaInquiriesBasisjm2 = $resultadosBasisJM->fetchAll(PDO::FETCH_ASSOC);
  
  // cambiar nombre y traer ult fecha zona africa
  $mapaPaises2 = [
    'Western Africa' => 'WAF'
  ];
  foreach ($tablaInquiriesBasisjm2 as $row) {
    $origen = isset($mapaPaises2[$row['RegionOrigin']]) ? $mapaPaises2[$row['RegionOrigin']] : $row['RegionOrigin'];
    $origenesData[$origen]['InquiriesBasisjm'] = $row['fecha'];
  }

  // Hoy menos 15 días --- General----
  $hoy = date('Y-m-d', strtotime('-15 days'));
  $formatoFecha = 'Y-m-d';
  
  $complementoTabla = "
  <table style='width: 360px;' border='1'>
  <tr>
    <th style='background-color:#CDCDCD'>Origin</th>
    <th style='background-color:#CDCDCD'>Last Update</th>
    <th style='background-color:#CDCDCD'>Last Update <br> Low Grades</th>
  </tr>";

  foreach ($origenesData as $origen => $fechas) {
    $fechaOrgBasis = $fechas['OrgBasis'];
    $fechaInquiriesBasisjm = $fechas['InquiriesBasisjm'];

    // Verificar si ambas fechas son mayores a hoy menos 15 días
    $ambasFechasMayor = ($fechaOrgBasis >= $hoy && $fechaInquiriesBasisjm >= $hoy);

    // Verificar si al menos una de las fechas es menor a hoy menos 15 días
    $alMenosUnaFechaMenor = ($fechaOrgBasis < $hoy || $fechaInquiriesBasisjm < $hoy);

    // Aplicar estilos basados en las condiciones
    if ($ambasFechasMayor) {
        $estiloOrgBasis = '';
    } elseif ($alMenosUnaFechaMenor) {
        $estiloOrgBasis = 'style="color: red;"';
    } else {
        $estiloOrgBasis = 'style="color: black;"';
    }

    // Aplicar estilos a las fechas
    $estiloFechaOrgBasis = $fechaOrgBasis < $hoy ? 'style="color: red;"' : '';
    $estiloFechaInquiriesBasisjm = $fechaInquiriesBasisjm < $hoy ? 'style="color: red;"' : '';

    // Si alguna de las fechas no tiene estilo rojo, poner el origen en negro
    if ($estiloFechaOrgBasis === '' || $estiloFechaInquiriesBasisjm === '') {
      $estiloOrgBasis = '';
    }

    // Construir la fila de la tabla
    $complementoTabla .= "
<tr>
  <td {$estiloOrgBasis}>{$origen}</td>
  <td {$estiloFechaOrgBasis}>" . ($fechaOrgBasis !== 0 ? date($formatoFecha, strtotime($fechaOrgBasis)) : '') . "</td>
  <td {$estiloFechaInquiriesBasisjm}>" . ($fechaInquiriesBasisjm !== 0 ? date($formatoFecha, strtotime($fechaInquiriesBasisjm)) : '') . "</td>
</tr>";
}
$complementoTabla .= "</table>";

  // Intancia de PHPMailer
  $mail = new PHPMailer();
  // Es necesario para poder usar un servidor SMTP como gmail
  $mail->isSMTP();
  // Si estamos en desarrollo podemos utilizar esta propiedad para ver mensajes de error
  //SMTP::DEBUG_OFF    = off (for production use) 0
  $mail->SMTPDebug     = 0; //SMTP::DEBUG_SERVER;
  //Set the hostname of the mail server
  $mail->Host = 'smtp.gmail.com';
  $mail->Port = 465; // 465 o 587
  // Propiedad para establecer la seguridad de encripción de la comunicación
  $mail->SMTPSecure    = PHPMailer::ENCRYPTION_SMTPS; // tls o ssl para gmail obligado
  // Para activar la autenticación smtp del servidor
  $mail->SMTPAuth      = true;
  //$mail->SMTPAuthTLS      = false;
  // Credenciales de la cuenta
  $mail->Username     = $email;
  $mail->Password     = $pass;
  // Quien envía este mensaje
  $mail->setFrom($email, $usuario);
  // Si queremos una dirección de respuesta
  //$mail->addReplyTo('replyto@panchos.com', 'Pancho Doe');
  // Destinatario
  $correos = "marisol.hernandez@ecomtrading.com";
  $mail->addAddress($correos);
  /* $arraycorreo = explode(",",$correos);
  $tam = sizeof($arraycorreo);
  for($i=0; $i<$tam; $i++){
  $mail->addAddress($arraycorreo[$i]);
  } */

  // Asunto del correo
  $mail->Subject = $asunto;
  // Contenido
  $mail->IsHTML(true);
  $mail->CharSet = 'UTF-8';

  $bodyy ='
  Hi all, <br><br>

  In order to have as much information as possible to analyze, could you please log in to the portal and update the <b>Origin Basis and Origin Basis - JM Cotton</b> data.<br><br>
  
  <b>Origin Basis Link:</b> www.ecom-tracking.com/_Cotton_ECOM/Basis.php<br>
  <b>Origin Basis JM Cotton Link:</b> www.ecom-tracking.com/_Cotton_ECOM/BasisJM.php <br><br>
    
  The following table shows the last capture from each of the origins: <br><br>'.$complementoTabla.'<br><br>

  <b>Note:</b> If you do not have your username or password, you can send me an email with a copy to: 
  cottonportal@ecomtrading.com<br><br><br>

  Your support is greatly appreciated.<br>
  Regards.';


  $mail->Body = $bodyy;
  
  if ($mail->send()) {
    echo 'Correo enviado correctamente';
  } else {
    echo 'Error al enviar el correo: ' . $mail->ErrorInfo;
  }
?>