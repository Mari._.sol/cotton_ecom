
<?php
header('Content-Type: text/html; charset=utf-8');
include_once '../bd/conexion.php';
$objeto = new Conexion();
$conexion = $objeto->Conectar();
$opcion = (isset($_POST['opcion'])) ? $_POST['opcion'] : '';
$origen = (isset($_POST['origen'])) ? $_POST['origen'] : '';
$Date= (isset($_POST['Date'])) ? $_POST['Date'] : '';
$Destination= (isset($_POST['Destination'])) ? $_POST['Destination'] : '';
$Origin= (isset($_POST['Origin'])) ? $_POST['Origin'] : '';
$Origin2=(isset($_POST['Origin2'])) ? $_POST['Origin2'] : '';
$Buyer=(isset($_POST['Buyer'])) ? $_POST['Buyer'] : '';
$Agent=(isset($_POST['Agentinq'])) ? $_POST['Agentinq'] : '';
$Quantity=(isset($_POST['Quantity'])) ? $_POST['Quantity'] : '';
$CropFrom=(isset($_POST['CropFrom'])) ? $_POST['CropFrom'] : '';
$CropTo=(isset($_POST['CropTo'])) ? $_POST['CropTo'] : '';
$Certification=(isset($_POST['Certification'])) ? $_POST['Certification'] : '';
$ColMax=(isset($_POST['ColMax'])) ? $_POST['ColMax'] : '';
$LenMin=(isset($_POST['LenMin'])) ? $_POST['LenMin'] : '';
$MicMin=(isset($_POST['MicMin'])) ? $_POST['MicMin'] : '';
$MicMax=(isset($_POST['MicMax'])) ? $_POST['MicMax'] : '';
$StrMin=(isset($_POST['StrMin'])) ? $_POST['StrMin'] : '';
$QualityComment=(isset($_POST['QualityComment'])) ? $_POST['QualityComment'] : '';
$StartShip=(isset($_POST['StartShip'])) ? $_POST['StartShip'] : '';
$EndShip=(isset($_POST['EndShip'])) ? $_POST['EndShip'] : '';
$ShipComment=(isset($_POST['ShipComment'])) ? $_POST['ShipComment'] : '';
$Basis=(isset($_POST['Basis'])) ? $_POST['Basis'] : '';
$Unit=(isset($_POST['Unit'])) ? $_POST['Unit'] : '';
$CoverMonth=(isset($_POST['CoverMonth'])) ? $_POST['CoverMonth'] : '';
$PaymentTerms=(isset($_POST['PaymentTerms'])) ? $_POST['PaymentTerms'] : '';
$OfferComments=(isset($_POST['OfferComments'])) ? $_POST['OfferComments'] : '';
$Raiting=(isset($_POST['Raiting'])) ? $_POST['Raiting'] : '';
$Status=(isset($_POST['Status'])) ? $_POST['Status'] : '';
$Comments=(isset($_POST['Comments'])) ? $_POST['Comments'] : '';
$Usuario=(isset($_POST['Usuario'])) ? $_POST['Usuario'] : '';
$IDReq=(isset($_POST['IDReq'])) ? $_POST['IDReq'] : '';
$Zone=(isset($_POST['pais'])) ? $_POST['pais'] : '';
$zoneorigen=(isset($_POST['zoneorigen'])) ? $_POST['zoneorigen'] : '';
$zonedest=(isset($_POST['zonedest'])) ? $_POST['zonedest'] : '';
$priv=(isset($_POST['Priv'])) ? $_POST['Priv'] : '';
$country=(isset($_POST['country'])) ? $_POST['country'] : '';
$Price=(isset($_POST['Price'])) ? $_POST['Price'] : '';
$CountryDest=(isset($_POST['CountryDest'])) ? $_POST['CountryDest'] : '';

//// Variables para filtros
session_start();

$filterSes = $_SESSION['FiltersInq'];

$FromDate=$filterSes['FromDateInq'];
$ToDate=$filterSes['ToDateInq'];
$OriginReg=$filterSes['OriginRegInq'];
$OriginCount=$filterSes['OriginCountInq'];
$DestReg=$filterSes['DestRegInq'];
$DestCount=$filterSes['DestCountInq'];
$BuyerInq=$filterSes['BuyerInq'];
$UserInq=$filterSes['UserInq'];      



switch($opcion){
    case 1:
        $consulta = "SELECT * FROM  Inquiries_Status";      
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();        
        $data=$resultado->fetchAll(PDO::FETCH_ASSOC);
    break;     
    case 2: 
        $consulta = "SELECT * FROM  Inquiries_Raiting";      
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();        
        $data=$resultado->fetchAll(PDO::FETCH_ASSOC);
    break;
    case 3:   
        $consulta = "SELECT DISTINCT CountryCode,CountryName FROM Inquiries_Countries WHERE IsDestination = '1' AND SubRegion = '$Zone' ORDER BY CountryName ASC";      
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();        
        $data=$resultado->fetchAll(PDO::FETCH_ASSOC);    
    break; 
    case 4:  
        $consulta = "SELECT CountryCode,CountryName FROM Inquiries_Countries WHERE IsOrigin = '1' AND SubRegion = '$Zone' ORDER BY CountryName ASC";      
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();        
        $data=$resultado->fetchAll(PDO::FETCH_ASSOC);    
    break;    
    case 5:
        $consulta = "SELECT IDZone,Zone  FROM Inquiries_Zones WHERE CountryCode ='$origen'  ORDER BY Zone ASC";      
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();        
        $data=$resultado->fetchAll(PDO::FETCH_ASSOC);       
    break;
    case 6:
        $tam= strlen($country);
        $consulta="";
        if ($tam <= 2){
            $consulta = "SELECT Inquiries_Buyer.IDBuyer,Inquiries_Buyer.Buyer,Inquiries_Buyer.Status  FROM Inquiries_Countries 
            LEFT JOIN amsadb1.Inquiries_Buyer
            ON Inquiries_Countries.CountryName=Inquiries_Buyer.Country
            WHERE CountryCode='$country' AND Inquiries_Buyer.Status = 1 ORDER BY Buyer ASC";      
        }
        else{
            $consulta = "SELECT Inquiries_Buyer.IDBuyer,Inquiries_Buyer.Buyer,Inquiries_Buyer.Status  FROM Inquiries_Buyer 
            WHERE 	Inquiries_Buyer.Country='$country' AND Inquiries_Buyer.Status = 1 ORDER BY Buyer ASC";  
        }
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();        
        $data=$resultado->fetchAll(PDO::FETCH_ASSOC);       
    break;
    case 7:
        $consulta = "SELECT *  FROM Inquiries_Agents ORDER BY Agent ASC";      
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();        
        $data=$resultado->fetchAll(PDO::FETCH_ASSOC);       
    break;
    case 8:
        $consulta = "INSERT INTO Inquiries_Inquirie (Date,Destination,Origin,Origin2,Buyer,Agent,Quantity,CropFrom,CropTo,Certification,ColMax,LenMin,MicMin,MicMax,StrMin,QualityComment,StartShip,EndShip,ShipComment,Basis,Unit,
        CoverMonth,PaymentTerms,OfferComments,Raiting,Status,Comments,User,RegionOrigin,RegionDest,Type,Price) VALUES ('$Date','$Destination','$Origin','$Origin2','$Buyer','$Agent','$Quantity','$CropFrom','$CropTo','$Certification','$ColMax','$LenMin','$MicMin',
        '$MicMax','$StrMin',:QualityComment,'$StartShip','$EndShip',:ShipComment,'$Basis','$Unit','$CoverMonth','$PaymentTerms',:OfferComments,'$Raiting','$Status',:Comments,'$Usuario','$zoneorigen','$zonedest',1,'$Price')";      
        $resultado = $conexion->prepare($consulta);
         $resultado->bindValue(':QualityComment', $QualityComment, PDO::PARAM_STR);
         $resultado->bindValue(':OfferComments', $OfferComments, PDO::PARAM_STR);
         $resultado->bindValue(':Comments', $Comments, PDO::PARAM_STR);
          $resultado->bindValue(':ShipComment', $ShipComment, PDO::PARAM_STR);
        $resultado->execute();        
        $data=$resultado->fetchAll(PDO::FETCH_ASSOC);
    break;
    case 9:
        //CARGAR TABLA 
        $complemeto="";
        if($priv==1){
            $complemeto=" WHERE Inquiries_Inquirie.Type = '1' ORDER BY IDInquirie  DESC LIMIT 200;";
        }
        else{
            $complemeto=" WHERE Inquiries_Inquirie.User = '$Usuario' AND Inquiries_Inquirie.Type='1' ORDER BY IDInquirie  DESC ;";
        }
        //VALIDAR LOS FILTROS 
        if($FromDate=="" && $ToDate=="" && $OriginReg =="" && $OriginCount =="" &&  $DestReg =="" &&  $DestCount =="" &&  $BuyerInq =="" &&  $UserInq==""){
            $consulta = "SELECT Agent, IDInquirie,Date,CoverMonth,Quantity,Origin2,CropFrom,CropTo,Certification,ColMax,LenMin,MicMin,MicMax,StrMin,QualityComment,
            StartShip,EndShip,ShipComment,Basis,Unit,PaymentTerms,OfferComments,Comments,RegionOrigin,RegionDest,Inquiries_Countries.CountryName as Destination,
            Countries.CountryName as Origin,Inquiries_Buyer.Buyer as Buyer,Inquiries_Inquirie.Price,
            Inquiries_Raiting.Raiting,Inquiries_Status.Status as Status,U.Name as NameUser
            FROM Inquiries_Inquirie 
            LEFT JOIN amsadb1.Inquiries_Countries
            ON Inquiries_Countries.CountryCode = Inquiries_Inquirie.Destination
            LEFT JOIN amsadb1.Inquiries_Countries as Countries 
            ON Countries.CountryCode = Inquiries_Inquirie.Origin
            
            LEFT JOIN amsadb1.Inquiries_Buyer
            ON Inquiries_Buyer.IDBuyer = Inquiries_Inquirie.Buyer 
        
            LEFT JOIN amsadb1.Inquiries_Raiting
            ON Inquiries_Raiting.IDRaiting = Inquiries_Inquirie.Raiting
            LEFT JOIN Inquiries_Status
            ON Inquiries_Status.IDStatus = Inquiries_Inquirie.Status  
            
            LEFT JOIN Users_Inquiries as U
            ON U.User = Inquiries_Inquirie.User ".$complemeto;

            $resultado = $conexion->prepare($consulta);
            $resultado->execute();       
        }
        else{
            $finconsulta=" ORDER BY IDInquirie  DESC";
            $filtro1="";
            if($OriginReg != ""){
                $filtro1 .= " AND RegionOrigin = '$OriginReg'";
            }
            if($OriginCount != ""){
                $filtro1 .= " AND Origin = '$OriginCount' ";
            }
            if($DestReg != ""){
                $filtro1 .= " AND RegionDest = '$DestReg' ";
            }
            if($DestCount != ""){
                $filtro1 .= " AND Destination = '$DestCount' ";
            }
            if($BuyerInq != ""){
                $filtro1 .= " AND Inquiries_Inquirie.Buyer = '$BuyerInq' ";
            }
            if($UserInq != ""){
                $filtro1 .= " AND Inquiries_Inquirie.User = '$UserInq' ";
            }

            $consulta = "SELECT Agent, IDInquirie,Date,CoverMonth,Quantity,Origin2,CropFrom,CropTo,Certification,ColMax,LenMin,MicMin,MicMax,StrMin,QualityComment,
            StartShip,EndShip,ShipComment,Basis,Unit,PaymentTerms,OfferComments,Comments,RegionOrigin,RegionDest,Inquiries_Countries.CountryName as Destination,
            Countries.CountryName as Origin,Inquiries_Buyer.Buyer as Buyer,Inquiries_Inquirie.Price,
            Inquiries_Raiting.Raiting,Inquiries_Status.Status as Status,U.Name as NameUser
            FROM Inquiries_Inquirie 
            LEFT JOIN amsadb1.Inquiries_Countries
            ON Inquiries_Countries.CountryCode = Inquiries_Inquirie.Destination
            LEFT JOIN amsadb1.Inquiries_Countries as Countries 
            ON Countries.CountryCode = Inquiries_Inquirie.Origin
            
            LEFT JOIN amsadb1.Inquiries_Buyer
            ON Inquiries_Buyer.IDBuyer = Inquiries_Inquirie.Buyer 
        
            LEFT JOIN amsadb1.Inquiries_Raiting
            ON Inquiries_Raiting.IDRaiting = Inquiries_Inquirie.Raiting
            LEFT JOIN Inquiries_Status
            ON Inquiries_Status.IDStatus = Inquiries_Inquirie.Status  
            
            LEFT JOIN Users_Inquiries as U
            ON U.User = Inquiries_Inquirie.User 
            
            WHERE Inquiries_Inquirie.Type = '1' AND (Date BETWEEN  '$FromDate' AND '$ToDate')".$filtro1.$finconsulta;

            $resultado = $conexion->prepare($consulta);
            $resultado->execute();       
        }
        $data=$resultado->fetchAll(PDO::FETCH_ASSOC);    
    break; 
    case 10:
        $consulta = " SELECT Inquiries_Countries.CountryCode as IDdestino,
        (SELECT origen.CountryCode FROM Inquiries_Countries as origen where origen.CountryName='$Origin') as IDOrigen        
        FROM Inquiries_Countries
        where Inquiries_Countries.CountryName='$Destination'";      
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();        
        $data=$resultado->fetchAll(PDO::FETCH_ASSOC);   
    break;
    case 11:
        $consulta = " UPDATE Inquiries_Inquirie  SET Date = '$Date', Destination = '$Destination', Origin='$Origin', Origin2='$Origin2',
        Buyer = '$Buyer', Agent = '$Agent',Quantity = '$Quantity',CropFrom='$CropFrom', CropTo = '$CropTo', Certification = '$Certification',
        ColMax = '$ColMax', LenMin='$LenMin', MicMin = '$MicMin', MicMax = $MicMax, StrMin = '$StrMin', QualityComment = :QualityComment,
        StartShip = '$StartShip', EndShip = '$EndShip', ShipComment = :ShipComment,Basis='$Basis', Unit = '$Unit', CoverMonth = '$CoverMonth',
        PaymentTerms = '$PaymentTerms', OfferComments = :OfferComments, Raiting = '$Raiting', Status = '$Status', Comments = :Comments,
        RegionOrigin ='$zoneorigen',RegionDest='$zonedest',Price='$Price'
        where Inquiries_Inquirie.IDInquirie='$IDReq'";      
        
        $resultado = $conexion->prepare($consulta);  
        $resultado->bindValue(':QualityComment', $QualityComment, PDO::PARAM_STR);
        $resultado->bindValue(':OfferComments', $OfferComments, PDO::PARAM_STR);
        $resultado->bindValue(':Comments', $Comments, PDO::PARAM_STR);
        $resultado->bindValue(':ShipComment', $ShipComment, PDO::PARAM_STR);
      
        $resultado->execute();        
        $data=$resultado->fetchAll(PDO::FETCH_ASSOC);   
    break;
    case 12:
        $consulta = "SELECT  DISTINCT DISTINCT SubRegion,Region FROM Inquiries_Countries WHERE IsDestination = '1' ORDER BY Region ASC";      
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();        
        $data=$resultado->fetchAll(PDO::FETCH_ASSOC);    
    break;
    case 13:
        $consulta = "SELECT * FROM Inquiries_Covmon WHERE  Inquiries_Covmon.Available = '1' ";      
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();        
        $data=$resultado->fetchAll(PDO::FETCH_ASSOC);    
    break;
    case 14:
        $consulta = "SELECT  DISTINCT SubRegion,Region FROM Inquiries_Countries WHERE IsOrigin = '1' ORDER BY Region ASC";      
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();        
        $data=$resultado->fetchAll(PDO::FETCH_ASSOC);    
    break;
     case 15:
        $consulta = "SELECT  DISTINCT Users_Inquiries.Name,Users_Inquiries.User 
        FROM Inquiries_Inquirie,Users_Inquiries 
        WHERE Inquiries_Inquirie.User = Users_Inquiries.User AND Inquiries_Inquirie.Type = '1'
        ORDER BY Name ASC";      
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();        
        $data=$resultado->fetchAll(PDO::FETCH_ASSOC);    
    break;
    case 16:
        $consulta = "INSERT INTO Inquiries_Inquirie (Date, Destination, Origin, Origin2, Buyer, Agent, Quantity, CropFrom, CropTo, Certification, ColMax, LenMin, MicMin, MicMax, StrMin, QualityComment, StartShip, EndShip, ShipComment, Basis, Unit, CoverMonth, PaymentTerms, OfferComments, Raiting, Status, Comments, RegionOrigin, RegionDest, Price, User, Type) 
                     VALUES ('$Date', '$Destination', '$Origin', '$Origin2', '$Buyer', '$Agent', '$Quantity', '$CropFrom', '$CropTo', '$Certification', '$ColMax', '$LenMin', '$MicMin', '$MicMax', '$StrMin', :QualityComment, '$StartShip', '$EndShip', :ShipComment, '$Basis', '$Unit', '$CoverMonth', '$PaymentTerms', :OfferComments, '$Raiting', '$Status', :Comments, '$zoneorigen', '$zonedest', '$Price','$Usuario', 1)";
        $resultado = $conexion->prepare($consulta);  
        $resultado->bindValue(':QualityComment', $QualityComment, PDO::PARAM_STR);
        $resultado->bindValue(':OfferComments', $OfferComments, PDO::PARAM_STR);
        $resultado->bindValue(':Comments', $Comments, PDO::PARAM_STR);
        $resultado->bindValue(':ShipComment', $ShipComment, PDO::PARAM_STR);
        $resultado->execute();        
        $data = $resultado->fetchAll(PDO::FETCH_ASSOC);   
    break;
    case 17:
        $consulta = "SELECT Max(IDInquirie) as IDInq,  RegionOrigin, Origin, Origin2, RegionDest, Destination, Buyer, Agent, Quantity,
            CropFrom, CropTo, Certification, ColMax, LenMin, MicMin, MicMax, StrMin, QualityComment, StartShip, EndShip,
            ShipComment, Basis, CoverMonth, PaymentTerms, Price, Unit, OfferComments, Raiting, Status, Comments
            FROM inquiries_inquirie
            WHERE
                Date = (SELECT MAX(Date) FROM inquiries_inquirie WHERE Origin = '$origen' AND Destination = '$CountryDest')
                AND Origin ='$origen'
                AND Destination = '$CountryDest'";
        $resultado = $conexion->prepare($consulta);
        $resultado->execute();        
        $data = $resultado->fetchAll(PDO::FETCH_ASSOC);   
        //print_r($data);
    break; 
}
print json_encode($data, JSON_UNESCAPED_UNICODE);

 $conexion=null;
//$conexion=null;
